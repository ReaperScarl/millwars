# README #

This is a university project for the subject "online game design" and "Artificial Intelligence for videogames".
This project was made by 3 persons:
 - Francesco Scarlata
 - Davide Quadrelli
 - Daniele Piergigli

The contacts will be shown below.
 


### What is this repository for? ###

* Quick summary

This is a moba 2 vs 2 in 3rd person, 3D game project made with Unity. 
The following is the link to the Game Design Document for the game.
https://docs.google.com/document/d/1GSanJItFpMMijldGExfafl40vt3hquJycGJDd3sxKnc/edit?usp=sharing

Note: the meshes of the characters are not done due to a problem with the guys that should have done them. 
The only thing we can do now it to apologize for the cubes and say to imagine the characters with the references from the GDD.

### How do I get set up? ###

* Summary of set up

Download this project and open it with Unity (version 5.5+)
After it is opened, you can build it going in "Settings" and choose the "Standalone mode". Then you can save the build and play it.

If you want just the build without open Unity, contact me (See below Francesco's contact) and i'll send it to you.


### Who do I talk to? ###

* Repo owner

Francesco Scarlata

 e-mail: francescoscarl93@gmail.com
 
Linkedin : https://www.linkedin.com/in/francescoscarlata/

* Other team contacts

Davide Quadrelli

e-mail: ildado94@gmail.com

Daniele Piergigli

e-mail: daniele.piergigli@gmail.com
﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Class responsible to give the info to the game player prefab after the scene is loaded
/// </summary>
public class NetworkLobbyHook : MonoBehaviour {

    //for spawn points: 0 red, 1 blue
    private int[] spawns = { 0, 0 };

    //player here is going to be instantiated in the scene. before to do so, we need to put all the info we need on the player.
    public void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
        PlayerStats ps = gamePlayer.GetComponent<PlayerStats>();

        ps.playerName = lobby.playerName;
        int team = -1;
        switch (lobby.imRed)
        {
            case 1:
                ps.align = "red";
                team = 0;
                break;
            case 2:
                ps.align = "blue";
                team = 1;
                break;
        }
        //Debug.Log("Spawn point del team " + team + "vale " + spawns[team]);
        gamePlayer.GetComponent<PlayerControllerWithNet>().spawnIndex = spawns[team]++;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class responsible to picke a champ and give a backstory about that champ
/// </summary>
public class ChampSelectionController : MonoBehaviour {

    public RectTransform playersList;
    public string[] Backstory;

    // the player chooses the champ
    public void PickAChampion(int champ)
    {
        LobbyPlayer[] lobbyPlayers = playersList.gameObject.GetComponentsInChildren<LobbyPlayer>();
        foreach (LobbyPlayer player in lobbyPlayers) // i don't know how to find yourself in the list, so i just do it in all
        {
            if (player.isLocalPlayer)
            {
                if (player.isClient && !player.isServer)
                    player.CmdChampChanged(champ);
                else
                    player.champion = champ;
                GameObject.Find("Backstory").GetComponent<Text>().text ="Backstory: \n"+ Backstory[champ - 1];
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

/// <summary>
/// It's the class used for the count down in the champion Select.
/// </summary>
public class SimpleTimerManager : NetworkBehaviour {

    //Time limit of play. Can change in the custom match
    public int timeLimit = 2; //in minutes

    [SyncVar(hook = "OnChangedCountdown")]
    private float countdown;
    private bool timerActive = true;
    GameObject netManager;

    void Start () {
        netManager = GameObject.Find("LobbyManagerCanvas");
        countdown = timeLimit * 60;
        this.GetComponent<Text>().text = "" + (int)countdown / 60 + " : " + countdown % 60;

        if (isServer)
            StartCoroutine(ChangeText());
    }

    private void OnEnable()
    {
        netManager = GameObject.Find("LobbyManagerCanvas");
        countdown = timeLimit * 60;
        this.GetComponent<Text>().text = "" + (int)countdown / 60 + " : " + countdown % 60;
        if (isServer)
            StartCoroutine(ChangeText());
    }

    IEnumerator ChangeText()
    {    
        while (timerActive){
            countdown -= 1;
            int minutes = (int)countdown / 60;
            yield return new WaitForSeconds(1);
            //End of the game for time out
            if (minutes == 0 && (int)countdown % 60 == 1)
                TimesUp();
        }
    }

    // The game can start even if there are not all 4 players after 2 mins
    void TimesUp()
    {
        netManager.GetComponent<LobbyManager>().StartMatch();
    }

    void OnChangedCountdown(float newcountdown)
    {
        this.GetComponent<Text>().text = "" + (int)newcountdown / 60 + " : " + (int)newcountdown % 60;
    }

    public void StartLastCountDown() // To put the countdown off for the matchmaking countdown before the spawn
    {
        timerActive = false;
    }

    public void StartCountDown()
    {
        timerActive = true;
        OnEnable();
    }
}
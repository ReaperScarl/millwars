﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
/// Class adapted almost from the project Meteoroid by Unity
/// </summary>
public class LobbyManager : NetworkLobbyManager
{

    static public LobbyManager s_Singleton;

    [Header("Unity UI Lobby")]
    [Tooltip("Time in second between all players ready & match start")]
    public float prematchCountdown = 5;

    //to know if the instance is on a server or on a client -> needed in button controller
    public bool isHost=false;

    public Text IPText;
    public RectTransform background;
    public RectTransform MatchMenu;
    public RectTransform ChampSelectMenu;
    public RectTransform Champs;
    public RectTransform playerList;
    public Button backButton;
    public Text countdownPanel;
    public Text infoPanel;

    [Tooltip ("Check yes if you want to see your ip adress instead of local host")]
    public bool isIpAddress; 
    public GameObject[] champions;                     //the prefabs of the player champions prefabs
    public GameObject[] championsAI=new GameObject[2]; //the prefabs of the champions AI
    protected NetworkLobbyHook _lobbyHooks;

    public delegate void BackButtonDelegate();
    public BackButtonDelegate backDelegate; //actually not used, but nvm

    private int numOfAI=0; // needed to know how many slots left there are.

    void Start () {
        s_Singleton = this;
        _lobbyHooks = GetComponent<NetworkLobbyHook>();
        if (isIpAddress)
            this.networkAddress = Network.player.ipAddress;
        DontDestroyOnLoad(gameObject);
        dontDestroyOnLoad = true;
    }

    public void GoBackButton()
    {
        backDelegate();
    }

    // ----------------- Server management

    public void AddLocalPlayer()
    {
        TryToAddPlayer();
    }

    public void RemovePlayer(LobbyPlayer player)
    {
        player.RemovePlayer();
    }

    public void StopHostClbk()
    {
        StopHost();
    }

    public void StopClientClbk()
    {
        StopClient();
    }

    //===================


    public override void OnStartHost()
    {
        base.OnStartHost();
        StartMatchMenu(true);
        isHost = true;
        countdownPanel.GetComponent<SimpleTimerManager>().StartCountDown();
        backDelegate = StopHostClbk;
    }

    new public void StartClient()
    {
        base.StartClient();
        backDelegate = StopClientClbk;
        isHost = false;
        StartMatchMenu(true);
    }

    public override void OnStopClient()
    {
        base.OnStopClient();       
        background.gameObject.SetActive(true);
        countdownPanel.enabled = true;
        countdownPanel.gameObject.SetActive(false);
        StartMatchMenu(false);
    }

    // ----------------- Server callbacks ------------------

    //we want to disable the button JOIN if we don't have enough player -> not in the prototype, because the min is 1
    //But OnLobbyClientConnect isn't called on hosting player. So we override the lobbyPlayer creation
    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;

        LobbyPlayer newPlayer = obj.GetComponent<LobbyPlayer>();
        newPlayer.ToggleJoinButton(numPlayers + 1 >= minPlayers);
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

            if (p != null)
                p.ToggleJoinButton(numPlayers + 1 >= minPlayers);
        }
        return obj;
    }

    public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
    {
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

            if (p != null)
                p.ToggleJoinButton(numPlayers + 1 >= minPlayers);
        }
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

            if (p != null)
                p.ToggleJoinButton(numPlayers >= minPlayers);
        }
    }

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        //This hook allows you to apply state data from the lobby-player to the game-player
        if (_lobbyHooks)
            _lobbyHooks.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);
        if (countdownPanel != null)
        {
            countdownPanel.text = ""; // just to be safe
            countdownPanel.enabled = false; 
        }   
        return true;
    }
    
    //To create the GamePlayer
    public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
    {
        LobbyPlayer[] players = playerList.GetComponentsInChildren<LobbyPlayer>();
        for (int i = 0; i < players.Length; i++)
        { 
            if (players[i].connectionToClient==conn)
            {
                GameObject obj = Instantiate(champions[players[i].champion - 1].gameObject) as GameObject;
                return obj;
            }
        }
        Debug.Log("You shouldn't be here");
        return base.OnLobbyServerCreateGamePlayer(conn, playerControllerId);
    }

    // It just doesn't allow to more players to connect. No idea how to know if the disconnect is from the host (no reason given to the client for the disconnection)
    public override void OnLobbyServerConnect(NetworkConnection conn)
    {
        if (numPlayers >= maxPlayers)
        {
            conn.Disconnect();
            return;
        }
        base.OnLobbyServerConnect(conn);
    }

    // --- Countdown management

    // All the players inside are ready, no matter how many they are (<4 anyway)
    public override void OnLobbyServerPlayersReady() 
    {
        bool allready = true;
        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            if (lobbySlots[i] != null)
                allready &= lobbySlots[i].readyToBegin;
        }
        // check how may players there are and give the remaining number for the AI
        numOfAI = maxPlayers - numPlayers;
        if (allready)
            StartCoroutine(ServerCountdownCoroutine());
    }

    //The server scene is changed. it's the time to spawn the AI
    public override void OnLobbyServerSceneChanged(string sceneName)
    {
        for(int i=numOfAI; i>0; i--) // I'mm putting all bun bun, if you don't like it, modify it yourself
        {
            if (i == 1 || i == 3) // should be the blue member (1 and 3 are the blue, usually) 
            {
                if (lobbySlots[0].GetComponent<LobbyPlayer>().blueMembers < 2)
                {
                    InstantiateAI(false);
                    lobbySlots[0].GetComponent<LobbyPlayer>().blueMembers++;
                }
                else
                {

                    InstantiateAI(true);
                    lobbySlots[0].GetComponent<LobbyPlayer>().redMembers++;
                }
            }
            if(i == 2) //should be the red member (0 and 2 are the red, usually)
            {
                if (lobbySlots[0].GetComponent<LobbyPlayer>().redMembers < 2)
                {
                    InstantiateAI(true);
                    lobbySlots[0].GetComponent<LobbyPlayer>().redMembers++;
                }
                else
                {
                    InstantiateAI(false);
                    lobbySlots[0].GetComponent<LobbyPlayer>().blueMembers++;
                }
            }
        }
        base.OnLobbyServerSceneChanged(sceneName);
    }

    // Function responsible for the countdown after the player are all ready (or are forced to ready)
    public IEnumerator ServerCountdownCoroutine()
    {
        int remainingTime = (int)prematchCountdown;
        countdownPanel.GetComponent<SimpleTimerManager>().StartLastCountDown();

        while (remainingTime > 0)
        {
            remainingTime -= 1;
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                if (lobbySlots[i] != null) //there is maxPlayer slots, so some could be == null, need to test it before accessing!
                    (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(remainingTime);
            }
            yield return new WaitForSeconds(1);
        }

        for (int i = 0; i < lobbySlots.Length; ++i)
        {
            if (lobbySlots[i] != null)
            {
                (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(0);
            }
        }
        ServerChangeScene(playScene);
    }

    // ----------------- Client callbacks ------------------
    //Needed to close/open the background and the player list
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        if (SceneManager.GetActiveScene().name == "GameWithNet")
        {
            background.gameObject.SetActive(false);
            playerList.gameObject.SetActive(false);
            Champs.gameObject.SetActive(false);
            countdownPanel.enabled = false;
        }
        else
        {
            background.gameObject.SetActive(true);
            playerList.gameObject.SetActive(true);
        }
    }
    
    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (SceneManager.GetActiveScene().name == offlineScene)
        {
            MatchMenu.gameObject.SetActive(true);
            ChampSelectMenu.gameObject.SetActive(false);
            Champs.gameObject.SetActive(false);
        }
    }

    //just adapted but not used
    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        infoPanel.enabled = true;
        infoPanel.text= "Cient error : " + (errorCode == 6 ? "timeout" : errorCode.ToString());
    }

    //Function used to change from match menu and champselectionmenu in MatchMenu scene (offline)
    private void StartMatchMenu( bool start)
    {  
        MatchMenu.gameObject.SetActive(!start);
        ChampSelectMenu.gameObject.SetActive(start);
        
        Champs.gameObject.SetActive(start);
        IPText.text = "Your networkAddress: " + networkAddress;
        playerList.gameObject.SetActive(start);
    }

    //  Just for prototype -> changes if wanted are welcomed
    public void StartMatch() 
    {
        LobbyPlayer[] players = playerList.GetComponentsInChildren<LobbyPlayer>();
        for (int i=0; i<players.Length; i++)
        {
            if (players[i].champion == 0) // if the player doesn't have chosen the champ and the times up
                players[i].CmdChampChanged(1); // choose of default boxer    
            players[i].RpcForceReady();
        }
    }

    //Function used to create the instantiate the AI champions
    void InstantiateAI(bool isRed)
    {
		int index = Random.Range (0, 2);
        GameObject obj = Instantiate(championsAI[index].gameObject) as GameObject;
        obj.GetComponent<PlayerStats>().playerName = "Bot " + Random.Range(1, 50);
        obj.GetComponent<PlayerStats>().align = (isRed) ? "red" : "blue";
        NetworkServer.Spawn(obj);
    }
}

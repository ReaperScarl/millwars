﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


/// <summary>
/// List of players in the lobby
/// Class Adapted from the Class LobbyPlayerList in Meteoroid by Unity
/// </summary>
public class LobbyPlayerList : MonoBehaviour {

    public static LobbyPlayerList _instance = null;
    public RectTransform playerListContentTransform;

    protected VerticalLayoutGroup _layout;
    protected List<LobbyPlayer> _players = new List<LobbyPlayer>();

    public void OnEnable() // Maybe to move in start? TO look
    {
        _instance = this;
        _layout = playerListContentTransform.GetComponent<VerticalLayoutGroup>();
    }

    public void AddPlayer(LobbyPlayer player) // Add the lobby player to the list
    {
        if (_players.Contains(player))
            return;
        _players.Add(player);

        player.transform.SetParent(playerListContentTransform, false);
    }


    public void RemovePlayer(LobbyPlayer player)
    {
        if(playerListContentTransform.childCount>1) //There is at least the host
            transform.GetComponentInChildren<LobbyPlayer>().CmdRemoveTeamMember(player.imRed);
        _players.Remove(player);
    }
}

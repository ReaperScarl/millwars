﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Class responsible for the Network GUI of MatchMenu (quickmatch, join etc)
/// </summary>
public class LobbyManagerMenu : MonoBehaviour {

    public LobbyManager lobbyManager;
    public Text errorText;
    public InputField ipInput;

    //Called When clicked "QuickMatch"
    public void OnClickHost()
    {
        lobbyManager.StartHost();
    }

    //Called when clicked Join. In a future it could be also put a check, but not now.
    public void OnClickJoin()
    {
        lobbyManager.networkAddress = ipInput.text;
        lobbyManager.StartClient();
    }

    public void onEndEditIP(string text)
    {
        if (Input.GetKeyDown(KeyCode.Return))
            OnClickJoin();
    }

    public void TooManyConnections() //Doesn't show because it's not server. so bad
    {
        errorText.text = "Sorry but the Match doesn't have slots available";
        errorText.GetComponent<DelayedDisappearScript>().Disappear();
    }
}

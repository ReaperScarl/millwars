﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;

/// <summary>
/// This class is adapted from the Class LobbyPlayer in Meteoroid by Unity
/// </summary>
public class LobbyPlayer : NetworkLobbyPlayer {

    public Image colorTeam;
    public Text nameText;
    public Button readyButton;
    public Button waitingPlayerButton;
    public Button notReadyButton;
    public GameObject localIcone;
    public GameObject remoteIcone;
    //GameObject ChampionSelected; // To decide how to do it (text? img? etc)

    [SyncVar (hook ="OnMyName")]
    public string playerName="";

    [SyncVar (hook = "ChoseChamp")]
    public int champion = 0;

    [SyncVar ]
    public int blueMembers = 0;
    [SyncVar]
    public int redMembers = 0;
    [SyncVar]
    public bool isReady=false;

    [SyncVar(hook = "TeamPicked")]
    public int imRed=0;

    static Color JoinColor = new Color(255.0f / 255.0f, 0.0f, 101.0f / 255.0f, 1.0f);
    static Color NotReadyColor = new Color(34.0f / 255.0f, 44 / 255.0f, 55.0f / 255.0f, 1.0f);
    static Color ReadyColor = new Color(0.0f, 204.0f / 255.0f, 204.0f / 255.0f, 1.0f);
    static Color RedTeam = Color.red;
    static Color BlueTeam = Color.blue;
    static Color TransparentColor = new Color(0, 0, 0, 0);


    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();
        LobbyPlayerList._instance.AddPlayer(this);
        if (isLocalPlayer)
            SetupLocalPlayer();
        else
            SetupOtherPlayer();

        //setup the player data on UI. The value are SyncVar so the player
        //will be created with the right value currently on server
        OnMyName(playerName);
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        //if we return from a game, color of text can still be the one for "Ready"
        readyButton.transform.GetChild(0).GetComponent<Text>().color = Color.white;
        SetupLocalPlayer();
    }

    //Change the color of the button with the color c
    void ChangeReadyButtonColor(Color c)
    {
        ColorBlock b = readyButton.colors;
        b.normalColor = c;
        b.pressedColor = c;
        b.highlightedColor = c;
        b.disabledColor = c;
        readyButton.colors = b;
    }

    //Setup when it is not the LocalPlayer
    void SetupOtherPlayer()
    {
        ChangeReadyButtonColor(NotReadyColor);
        if (imRed == 1)
            colorTeam.color = RedTeam;
        else
            colorTeam.color = BlueTeam;
        ChoseChamp(champion); //let you see the old champions picked by other players

        readyButton.transform.GetChild(0).GetComponent<Text>().text = "...";
        
        readyButton.interactable = false;
        OnClientReady(false);
        if (isReady) //If they are ready, but you didn't see it, now you know
        {
            ChangeReadyButtonColor(TransparentColor);
            Text textComponent = readyButton.transform.GetChild(0).GetComponent<Text>();
            textComponent.text = "READY";
            textComponent.color = ReadyColor;
            readyButton.interactable = false;
        }
    }

    //Setup the localPlayerInfo
    void SetupLocalPlayer()
    {
        if (LobbyPlayerList._instance.playerListContentTransform.childCount > 4) // dependent (and not too important) - could be more independent if would look to the maxplayers inside LobbyManager, but it's ok because the number is costant
        {
            Debug.Log("You couldn't stay here");
            GetComponent<NetworkIdentity>().connectionToClient.Disconnect();
        }
        remoteIcone.gameObject.SetActive(false);
        localIcone.gameObject.SetActive(true);

        UpdateGUI();
        ChangeReadyButtonColor(JoinColor);

        readyButton.transform.GetChild(0).GetComponent<Text>().text = "Pick a champ";
        readyButton.interactable = false;

        playerName = PlayerPrefs.GetString("CurrentName");
        CmdNameChanged(playerName);
        transform.root.GetComponent<ScoreInfo>().myName = playerName;

        if ((LobbyPlayerList._instance.playerListContentTransform.childCount - 1) % 2 == 0) // pattern: 0 & 2 = red; 1 & 3 = blue
        { 
            // It should take the first child: the host
            if (LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().redMembers < 2)
                CmdPickedSide(1);
            else
                CmdPickedSide(2);
        }
        else
        {
            if (LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().blueMembers < 2)
                CmdPickedSide(2);
            else
                CmdPickedSide(1);   
        }
        readyButton.onClick.RemoveAllListeners();
        readyButton.onClick.AddListener(OnReadyClicked);
    }

    public override void OnClientReady(bool readyState)
    {
        if (readyState)
        {
            ChangeReadyButtonColor(TransparentColor);
            Text textComponent = readyButton.transform.GetChild(0).GetComponent<Text>();
            textComponent.text = "READY";
            textComponent.color = ReadyColor;
            readyButton.interactable = false;
            isReady = true;
        }
        else
        {
            ChangeReadyButtonColor(isLocalPlayer ? JoinColor : NotReadyColor);
            Text textComponent = readyButton.transform.GetChild(0).GetComponent<Text>();
            textComponent.text = isLocalPlayer ? "JOIN" : "...";
            textComponent.color = Color.white;
            readyButton.interactable = isLocalPlayer;
        }
    }

    //===== CALLBACK FROM SYNCVAR

    public void OnMyName(string newName)
    {
        playerName = newName;
        nameText.text = playerName;
    }

    public void ChoseChamp(int newChamp)
    {    
        switch (newChamp) //1: boxer, 2: bun bun
        {
            case 0:
                return;
            case 1:
                colorTeam.transform.Find("BoxerIcon").gameObject.SetActive(true);
                if(champion==2)
                    colorTeam.transform.Find("BunIcon").gameObject.SetActive(false);
                break;
            case 2:
                colorTeam.transform.Find("BunIcon").gameObject.SetActive(true);
                if (champion == 1)
                    colorTeam.transform.Find("BoxerIcon").gameObject.SetActive(false);
                break;
        }
        champion = newChamp;
        if(isLocalPlayer)
        {
            LobbyManager lobbymanager = GameObject.Find("LobbyManagerCanvas").GetComponent<LobbyManager>();
            lobbymanager.gamePlayerPrefab = lobbymanager.champions[newChamp - 1];
            readyButton.interactable = true;
            readyButton.transform.GetChild(0).GetComponent<Text>().text = "Ready?";
        }
    }

    public void TeamPicked(int newSide)
    {
        imRed = newSide;
        if (newSide != 0)
        {
            if (newSide == 1) // ->is red
                colorTeam.color = RedTeam;
            else
                colorTeam.color = BlueTeam;
        }
    }

    //===== UI Handler

    //Note that those handler use Command function, as we need to change the value on the server not locally, so that all client get the new value throught syncvar
    // Function used for the ready button of LobbyPlayer prefab
    public void OnReadyClicked()
    {
        SendReadyToBeginMessage();
    }

    public void ToggleJoinButton(bool enabled)
    {
        readyButton.gameObject.SetActive(enabled);
        waitingPlayerButton.gameObject.SetActive(!enabled);
    }

    [ClientRpc]
    public void RpcUpdateCountdown(int countdown)
    {
        LobbyManager.s_Singleton.countdownPanel.text = "Match Starting in \n" + countdown;
        LobbyManager.s_Singleton.countdownPanel.gameObject.SetActive(countdown != 0);
    }
   
    [ClientRpc]
    public void RpcForceReady()
    {
        SendReadyToBeginMessage();
    }

    //====== Server Command

    [Command]
    public void CmdNameChanged(string name)
    {
        playerName = name;
    }

    [Command]
    public void CmdChampChanged(int champ)
    {
        champion = champ;
    }

    [Command]
    public void CmdPickedSide(int redSide)
    {
        imRed = redSide;
        if (imRed == 1) // Changes the numbers in the host directly
        {
            LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().redMembers++;
        }
        else
            LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().blueMembers++;
    }

    [Command]
    public void CmdRemoveTeamMember(int side) // can give exception for the server
    {
        if (side==1)
            LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().redMembers--;
        else
            LobbyPlayerList._instance.playerListContentTransform.GetComponentInChildren<LobbyPlayer>().blueMembers--;
    }

    //Cleanup thing when get destroy (which happens when player disconnects)
    public void OnDestroy()
    {
        LobbyPlayerList._instance.RemovePlayer(this);
    }

    private void UpdateGUI()
    {
        if (isClient&& !isServer)
            LobbyManager.s_Singleton.countdownPanel.rectTransform.localPosition = new Vector3(0, 450, 0);
    }
}

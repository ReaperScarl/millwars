﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BTStructure;

public class BTStartFightBoxer {

	private BehaviorTree behaviourTree;

	// string with the derived DTree
	private string derivedTree;
	public BTStartFightBoxer(PlayerIAController controller) {
		this.controller = controller;
	}
	private PlayerIAController controller;
	// current target enemy
	private KeyValuePair<GameObject, float> currentEnemyTarget = new KeyValuePair<GameObject, float>();
	private GameObject target;
	// control immortality
	private float immortalityAbilityTimer;
	
	public void StartFightBoxerBTree () {

		BTAction useImmortalityAbility = new BTAction (UseImmortalityAbility);
		BTAction hookTarget = new BTAction (HookTarget);
		BTAction escapeFromTower = new BTAction (EscapeFromTower);
		BTAction moveNear = new BTAction (MoveNear);
		BTAction useFireAbility = new BTAction (UseFireAbility);
		BTAction useRotationAttackAbility = new BTAction (UseRotationAttackAbility);
		BTAction useEnGuardAbility = new BTAction (UseEnGuardAbility);
		BTAction useBaseAttack = new BTAction (UseBaseAttack);

		BTCondition checkPlayerLife = new BTCondition (CheckPlayerLife);
		BTCondition checkTarget = new BTCondition (CheckTarget);
		BTCondition checkPlayerLifeInUntilFail = new BTCondition (CheckPlayerLifeInUntilFail);
		BTCondition checkTargetIsAlive = new BTCondition (CheckTargetIsAlive);
		BTCondition checkAroundTower = new BTCondition (CheckAroundTower);
		BTCondition checkDistanceFromTarget = new BTCondition (CheckDistanceFromTarget);
		BTCondition checkImmortalityAbility = new BTCondition (CheckImmortalityAbility);
		BTCondition checkFireWeaponAbility = new BTCondition (CheckFireWeaponAbility);
		BTCondition checkRotationAttackAbility = new BTCondition (CheckRotationAttackAbility);
		BTCondition checkEnGuardeAbility = new BTCondition (CheckEnGuardeAbility);
		BTCondition checkBaseAttack = new BTCondition (CheckBaseAttack);

		BTSequence rotationAttackSequence = new BTSequence (new IBTTask[] { checkRotationAttackAbility, useRotationAttackAbility });
		BTSequence enGuardeSequence = new BTSequence (new IBTTask[] { checkEnGuardeAbility, useEnGuardAbility });
		BTSequence baseAttackSequence = new BTSequence (new IBTTask[] { checkBaseAttack, useBaseAttack });

		BTProbabilisticSelector checkAttackSelector = new BTProbabilisticSelector (new IBTTask[] { rotationAttackSequence, enGuardeSequence, baseAttackSequence }, new int[] {15, 15, 70});

		BTSequence moveNearSequence = new BTSequence (new IBTTask[] { checkDistanceFromTarget, moveNear });
		BTSequence checkFireWeaponAbilitySequence = new BTSequence (new IBTTask[] { checkFireWeaponAbility, useFireAbility });

		BTDecorator moveNearUntilFail = new BTDecoratorUntilFail (moveNearSequence);
		BTDecorator checkFireWeaponAbilityUntilFail = new BTDecoratorUntilFail (checkFireWeaponAbilitySequence);

		BTSequence attackSequence = new BTSequence (new IBTTask[] { checkPlayerLifeInUntilFail, checkTargetIsAlive, moveNearUntilFail, checkFireWeaponAbilityUntilFail, checkAttackSelector });

		BTDecorator attackUntilFail = new BTDecoratorUntilFail (attackSequence);

		BTSequence checkTowerSequence = new BTSequence (new IBTTask[] { checkAroundTower, escapeFromTower});

		BTDecorator checkTowerUntilFail = new BTDecoratorUntilFail (checkTowerSequence);

		BTSequence checkImmortalitySequence = new BTSequence (new IBTTask[] { checkImmortalityAbility, useImmortalityAbility });

		BTDecorator checkImmortalityUntilFail = new BTDecoratorUntilFail (checkImmortalitySequence);

		BTSequence rootSequence = new BTSequence (new IBTTask[] { checkImmortalityUntilFail, checkPlayerLife, checkTarget, hookTarget, checkTowerUntilFail, attackUntilFail});

		BTDecorator rootUntilFail = new BTDecoratorUntilFail (rootSequence);

		behaviourTree = new BehaviorTree(rootUntilFail);

		controller.StartCoroutine(PatrolBT());
	}

	public IEnumerator PatrolBT() {
		// while we have step or player is alive
		while (behaviourTree.Step() && controller.isAlive) {
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}

	// CONDITIONS

	// check my health bar
	private bool CheckPlayerLife() {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			// stop coroutine and restart dt base on the derived tree
			DTPlayerIAAttack dtPlayerIAAttack = new DTPlayerIAAttack(controller);
			dtPlayerIAAttack.StartAttackTree();
			return false;
		}
		return true;
	}

	// check my health bar inside until fail, so not stop tree 
	private bool CheckPlayerLifeInUntilFail() {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			return false;
		}
		return true;
	}

	// check if player can use immortality ability
	private bool CheckImmortalityAbility() {
		// if player have alarm life and can use the immortality ability
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController && controller.level > 4 && Time.time >= controller.boxerImmortalityTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if there are targets
	private bool CheckTarget() {
		DTPlayerIAAttack dtPlayerIAAttack;
		Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider);
		Dictionary<GameObject, float> enemiesTower = controller.EnemyTowers();
		Dictionary<GameObject, float> alliesList = controller.FindAllies();
		if (enemiesList.Count > 0 && enemiesTower.Count > 0) {
			foreach (KeyValuePair<GameObject, float> enemy in enemiesList) {
				// if there is a tower and enemies to attack
				if (enemy.Key != null && enemy.Key.GetComponent<TurretStats>() != null) {
					int count = 0;
					foreach (KeyValuePair<GameObject, float> ally in alliesList) {
						// if there is a tower and allies under
						if (ally.Key != null && enemiesTower.First().Key.GetComponent<TurretStats>().visionRange >= Vector3.Distance(enemiesTower.First().Key.transform.position, ally.Key.transform.position)) {
							count++;
						}
					}
					if (count >= 3) {
						controller.targetList.Clear();
						// enemies are now the target
						controller.targetList.Add(enemiesTower.First().Key, enemiesTower.First().Value);
						return true;
					}
					else {
						// if i can't attack stop coroutine and restart dt base on the derived tree
						dtPlayerIAAttack = new DTPlayerIAAttack(controller);
						dtPlayerIAAttack.StartAttackTree();
						// don't hook target
						return false;
					}
				}
				else {
					// if enemy is not in the range of the tower
					if (Vector3.Distance(enemy.Key.transform.position, enemiesTower.First().Key.transform.position) >= enemiesTower.First().Key.GetComponent<TurretStats>().visionRange) {
						controller.targetList.Clear();
						// enemies are now the target
						controller.targetList = enemiesList;
						return true;
					}
				}
			}
		}
		// attack first enemy
		else if (enemiesList.Count > 0 && enemiesTower.Count == 0) {
			controller.targetList.Clear();
			// enemies are now the target
			controller.targetList = enemiesList;
			return true;
		}
		// stop coroutine and restart dt base on the derived tree
		dtPlayerIAAttack = new DTPlayerIAAttack(controller);
		dtPlayerIAAttack.StartAttackTree();
		return false;
	}

	// check if i can destroy tower if is inside the field of view
	private bool CheckAroundTower() {
		Dictionary<GameObject, float> enemiesInField = controller.fieldOfView.GetEnemiesTarget();
		foreach (KeyValuePair<GameObject, float> enemy in enemiesInField) {
			// if there is a tower
			if (enemy.Key != null && enemy.Key.GetComponent<TurretStats>() != null) {
				Dictionary<GameObject, float> alliesInField = controller.fieldOfView.GetAlliesTarget();
				// there are other target for the tower and I am not in the range of the tower
				if (alliesInField.Count > 2 && enemy.Key.GetComponent<TurretStats>().visionRange < enemy.Value) {
					// attack tower
					return false;
				}
				// there are other target for the tower and I am in the range of the tower
				else if (alliesInField.Count > 2 && enemy.Key.GetComponent<TurretStats>().visionRange > enemy.Value) {
					// if I am under attack escape
					if (controller.isUnderAttack && !CheckPlayerLifeInUntilFail()) {
						controller.isUnderAttack = false;
						currentEnemyTarget = enemy;
						target = enemy.Key;
						return true;
					}
					// stay and fight
					else {
						return false;
					}
				}
				// if I am under tower
				else if (enemy.Key.GetComponent<TurretStats>().visionRange > enemy.Value && !CheckPlayerLifeInUntilFail()) {
					// escape
					currentEnemyTarget = enemy;
					target = enemy.Key;
					return true;
				}
				// stay
				else {
					return false;
				}
			}
		}
		return false;
	}

	// check if target is alive
	private bool CheckTargetIsAlive() {
		// target enemies is alive
		if (target == null) {
			return false;
		}
		return true;
	}

	// check if player is near target
	private bool CheckDistanceFromTarget() {
		// if enemies is near
		if (target != null && target.GetComponent<TurretStats>() == null && Vector3.Distance(currentEnemyTarget.Key.transform.position, controller.transform.position) <= controller.lowRangeCollider) {
			controller.playerAgent.isStopped = true;
			return false;
		}
		// if is a tower
		else if (target != null && target.GetComponent<TurretStats>() != null && Vector3.Distance(currentEnemyTarget.Key.transform.position, controller.transform.position) <= controller.lowRangeCollider + 1f) {
			controller.playerAgent.isStopped = true;
			return false;
		}
		else if (target == null) {
			controller.playerAgent.isStopped = true;
			return false;
		}
		return true;
	}

	// check if player can use fire ability
	private bool CheckFireWeaponAbility() {
		if (controller.level > 3 && Time.time >= controller.boxerFireWeaponTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use rotation attack ability
	private bool CheckRotationAttackAbility() {
		if (controller.level > 1 && Time.time >= controller.boxerRotateAttackTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use en guarde ability
	private bool CheckEnGuardeAbility() {
		if (controller.level > 2 && Time.time >= controller.boxerEnGuardeTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use base attack
	private bool CheckBaseAttack() {
		if (Time.time >= controller.boxerBaseAttackTimer && target != null) {
			return true;
		}
		return false;
	}

	// ACTIONS

	// use the immortality ability
	private bool UseImmortalityAbility() {
		// I can't recive damage
		controller.playerCollider.invincible = true;
		immortalityAbilityTimer = Time.time + controller.stats.A4Cooldown;
		controller.StartCoroutine(CheckImmortalityCoroutine());
		return true;
	}

	// hook target found
	private bool HookTarget() {
		// hook target
		currentEnemyTarget = controller.targetList.First();
		target = currentEnemyTarget.Key;
		return true;
	}

	// move near the target
	private bool MoveNear() {
		if (target != null) {
			// move to player position
			controller.playerAgent.isStopped = false;
			controller.playerAgent.SetDestination(target.transform.position);
		}
		return true;
	}

	// escape from the tower
	private bool EscapeFromTower() {
		// rotate player back
		Quaternion rotation = controller.transform.rotation;
		controller.playerAgent.isStopped = false;
		controller.transform.LookAt(2 * controller.transform.position - currentEnemyTarget.Key.transform.position);
		// escape
		controller.playerAgent.SetDestination(controller.transform.forward * (controller.highRangeCollider / 2f));
		// reset rotation
		controller.transform.rotation = rotation;
		return true;
	}

	// use the fire weapon ability
	private bool UseFireAbility() {
		controller.FireAbility();
		controller.boxerFireWeaponTimer = Time.time + controller.stats.A3Cooldown;
		return true;
	}

	// use the rotation attack ability
	private bool UseRotationAttackAbility() {
		if (target != null) {
			// look enemy and attack
			controller.transform.LookAt(target.transform);
			controller.boxerIAStats.weapon.CmdTotDamage((int)((float)controller.playerStats.attackStrength * controller.boxerIAStats.circularAttackMultiplier));
			controller.boxerIAStats.weapon.Attack(2, 0 , true);
			controller.boxerRotateAttackTimer = Time.time + controller.stats.A1Cooldown;
		}
		return true;
	}

	// use the en guarde ability
	private bool UseEnGuardAbility() {
		if (target != null) {
			// look enemy and attack
			controller.transform.LookAt(target.transform);
			controller.boxerIAStats.weapon.CmdTotDamage((int)((float)controller.playerStats.attackStrength * controller.boxerIAStats.chargeAttackMultiplier));
			controller.boxerIAStats.weapon.Attack(3, 0 , true);
			controller.boxerEnGuardeTimer = Time.time + controller.stats.A2Cooldown;
		}
		return true;
	}

	// use the base attack
	private bool UseBaseAttack() {
		if (target != null) {
			// look enemy and attack
			controller.transform.LookAt(target.transform);
			controller.boxerIAStats.weapon.CmdTotDamage(controller.playerStats.attackStrength);
			controller.boxerIAStats.weapon.Attack(1, 0 , true);
			controller.boxerBaseAttackTimer = Time.time + controller.stats.attackSpeed;
		}
		return true;
	}

	// check if is time to disable immortality
	public IEnumerator CheckImmortalityCoroutine() {
		while (immortalityAbilityTimer + controller.boxerIAStats.invincibleAbilityDelay > Time.time) {
			yield return new WaitForSeconds(1f);
		}
		// I can recive damage
		controller.playerCollider.invincible = false;
	}
}

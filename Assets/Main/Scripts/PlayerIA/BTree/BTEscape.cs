﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BTStructure;

public class BTEscape {

	private BehaviorTree behaviourTree;
	public BTEscape(PlayerIAController controller) {
		this.controller = controller;
	}
	private PlayerIAController controller;
	private DTPlayerIAMovement dtPlayerIAMovement;
	
	public void StartEscapeBTree () {

		dtPlayerIAMovement = new DTPlayerIAMovement(controller);
		controller.StopCoroutine(dtPlayerIAMovement.PatrolDT());

		BTAction useAbility = new BTAction (UseAbility);
		BTAction runAway = new BTAction (RunAway);

		BTCondition checkDistanceFromEnemy = new BTCondition (CheckDistanceFromEnemy);
		BTCondition checkDefensiveAbility = new BTCondition (CheckDefensiveAbility);

		BTSequence checkUseAbilitySequence = new BTSequence (new IBTTask[] { checkDefensiveAbility, useAbility });

		BTDecorator checkAbilityUntilFail = new BTDecoratorUntilFail (checkUseAbilitySequence);

		BTSequence checkDistanceSequence = new BTSequence (new IBTTask[] { checkDistanceFromEnemy, checkAbilityUntilFail, runAway });

		BTDecorator rootUntilFail = new BTDecoratorUntilFail (checkDistanceSequence);

		behaviourTree = new BehaviorTree(rootUntilFail);

		controller.StartCoroutine(PatrolBT());
	}

	public IEnumerator PatrolBT() {
		// while we have step or player is alive
		while (behaviourTree.Step() && controller.isAlive) {
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}

	// CONDITIONS
	
	// until fail check distance from enemy
	private bool CheckDistanceFromEnemy() {
		Dictionary<GameObject, float> enemies = controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider);
		// if there are enemies in escape range
		if (enemies.Count > 0) {
			// if I am not distant from enemy yet
			if (enemies.First().Value < controller.escapeRangeCollider) {
				return true;
			}
		}
		controller.playerAgent.isStopped = true;
		// reset the decision tree
		dtPlayerIAMovement.StartMovementTree();
		return false;
	}

	// check if player have defensive ability and if it can throw it base on the distance from the target (if request)
	private bool CheckDefensiveAbility() {
		// if is Boxer switch to its attack script
		if (controller.playerStats.playerClass == PlayersClass.Melee) {
			// if my level unlock ability and I can use the ability
			if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController && controller.level > 5 && Time.time >= controller.boxerImmortalityTimer) {
				return true;
			}
		}
		// else BunBun switch to its attack script
		else {
			// if my level unlock ability and I can use the ability
			if (controller.level > 3 && Time.time >= controller.bunBunSmokeTimer) {
				return true;
			}
		}
		return false;
	}

	// ACTIONS

	// use defensive ability
	private bool UseAbility() {
		// if is Boxer switch to its attack script
		if (controller.playerStats.playerClass == PlayersClass.Melee) {
			Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(controller.transform, controller.escapeRangeCollider);
			if (enemiesList.Count > 0) {
				// reset timer of countdown ability
				controller.boxerImmortalityTimer = Time.time + controller.playerStats.A4Cooldown;
				// use ability
				controller.playerCollider.invincible = true;
				controller.StartCoroutine(CheckImmortalityCoroutine());
			}
		}
		// else BunBun switch to its attack script
		else {
			Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(controller.transform, controller.escapeRangeCollider);
			if (enemiesList.Count > 0) {
				// if is in our attack distance range
				if (controller.highRangeCollider > enemiesList.First().Value && controller.lowRangeCollider < enemiesList.First().Value) {
					// look the target
					controller.transform.LookAt(enemiesList.First().Key.transform);
					// reset timer of countdown ability
					controller.bunBunSmokeTimer = Time.time + controller.playerStats.A2Cooldown;
                    // attack
                    controller.bunBunIAStats.gun.enemyPosition = enemiesList.First().Key.transform;
                    controller.bunBunIAStats.gun.CmdShootIA(4, controller.bunBunIAStats.smogAbilityDelay);
				}
			}
		}
		return true;
	}

	// escape from enemy
	private bool RunAway() {
		controller.transform.LookAt(controller.startPosition);
		controller.playerAgent.isStopped = false;
		// move agent to position
		controller.playerAgent.SetDestination(controller.startPosition.position);
		return true;
	}

	// check if is time to disable immortality
	public IEnumerator CheckImmortalityCoroutine() {
		while (controller.boxerImmortalityTimer + controller.boxerIAStats.invincibleAbilityDelay > Time.time) {
			yield return new WaitForSeconds(1f);
		}
		// I can recive damage
		controller.playerCollider.invincible = true;
	}
}

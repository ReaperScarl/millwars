﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BTStructure;

public class BTReachTheTarget {

	private BehaviorTree behaviourTree;
	public BTReachTheTarget(PlayerIAController controller) {
		this.controller = controller;
	}
	private PlayerIAController controller;
	private GameObject target;
	// string with the derived DTree
	private string derivedTree;
	public void StartReachTheTargetBTree (string dt, GameObject trgt) {

		derivedTree = dt;
		if (trgt != null) {
			target = trgt;
		}

		BTAction move = new BTAction (Move);

		BTCondition underAttack = new BTCondition (UnderAttack);
		BTCondition checkDistanceFromTarget = new BTCondition (CheckDistanceFromTarget);

		BTSequence checkSequence = new BTSequence (new IBTTask[] { underAttack, checkDistanceFromTarget, move });

		BTDecorator checkUntilFail = new BTDecoratorUntilFail (checkSequence);

		behaviourTree = new BehaviorTree(checkUntilFail);

		controller.StartCoroutine(PatrolBT());
	}

	public IEnumerator PatrolBT() {
		// while we have step or player is alive
		while (behaviourTree.Step() && controller.isAlive) {
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}

	// CONDITIONS
	
	// I'm attack while i'm moving
	private bool UnderAttack() {
		// if we recive an attack
		if (controller.isUnderAttack) {
			// if someone enter in the sphere of escape range
			if (controller.CheckCollision(controller.transform, controller.escapeRangeCollider)) {
				// if there are enemies in escape range
				if (controller.GetEnemiesInRange(controller.transform, controller.escapeRangeCollider).Count > 0) {
					ResetDecision();
					return false;
				}
			}
		}
		return true;
	}

	// check if player have reached the target
	private bool CheckDistanceFromTarget() {
		if (target != null) {
			// for enemy tower
			if (target.GetComponent<TurretStats>() != null && controller.GetEnemiesInRange(target.transform, target.GetComponent<TurretStats>().visionRange).Count > 2) {
				if (Vector3.Distance(target.transform.position, controller.transform.position) >= controller.highRangeCollider) {
					return true;
				}
			}
			// if target is not near
			else if (Vector3.Distance(target.transform.position, controller.transform.position) >= controller.escapeRangeCollider) {
				Dictionary<GameObject, float> enemiesInField = controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider);
				if (enemiesInField.Count > 0) {
					controller.playerAgent.isStopped = true;
					// reset the decision tree
					ResetDecision();
					return false;
				}
				return true;
			}
		}
		controller.playerAgent.isStopped = true;
		// reset the decision tree
		ResetDecision();
		return false;
	}

	// ACTIONS

	// Reach the target
	private bool Move() {
		if (target != null) {
            // if is a ally tower
            //Debug.Log("Transform tag: "+controller.transform.tag+" and target: "+target);
			if (target.GetComponent<TurretStats>().tag == controller.transform.tag) {
				controller.playerAgent.SetDestination(target.transform.position);
			}
			else {
				GameObject direction = controller.GetDestinationToTarget("enemyNexus");
				if (direction != null) {
					float distance = Vector3.Distance(direction.transform.position, controller.transform.position);
					// if is a tower and you are very near its vision range
					if (distance < controller.escapeRangeCollider -1f && direction.GetComponent<TurretStats>() != null && controller.GetEnemiesInRange(target.transform, target.GetComponent<TurretStats>().visionRange).Count == 0) {
                        // stay far from target
                        KeyValuePair<GameObject, float> pair = controller.FindAllyTowers().First();
                        if(pair.Key != null) {
                            controller.playerAgent.SetDestination(pair.Key.transform.position);
                        }
                        else {
                            Debug.Log("first tower is null");
                        }
					}
					else {
						// move to the nearest target 
						controller.playerAgent.SetDestination(direction.transform.position);
					}
				}
				else {
					// move to the nearest target 
					controller.playerAgent.SetDestination(target.transform.position);
				}
			}
		}
		controller.playerAgent.isStopped = false;
		return true;
	}

	// stop coroutine and restart dt base on the derived tree
	private void ResetDecision() {
		if (derivedTree == "movement") {
			// movement tree
			DTPlayerIAMovement dtPlayerIAMovement = new DTPlayerIAMovement(controller);
			dtPlayerIAMovement.StartMovementTree();
		}
		else {
			// attack tree
			DTPlayerIADefend dtPlayerIADefense = new DTPlayerIADefend(controller);
			dtPlayerIADefense.StartDefendTree();
		}
	}
}

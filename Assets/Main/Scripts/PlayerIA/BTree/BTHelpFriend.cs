﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTStructure;

public class BTHelpFriend {

	private BehaviorTree behaviourTree;
	public BTHelpFriend(PlayerIAController controller) {
		this.controller = controller;
	}
	private PlayerIAController controller;
	private DTPlayerIAMovement dtPlayerIAMovement;
	
	public void StartHelpFriendBTree () {

		dtPlayerIAMovement = new DTPlayerIAMovement(controller);

		BTAction moveNear = new BTAction (MoveNear);

		BTCondition underAttack = new BTCondition (UnderAttack);
		BTCondition checkDistanceFromMate = new BTCondition (CheckDistanceFromMate);
		BTCondition checkMate = new BTCondition (CheckMate);

		BTSequence helpSequence = new BTSequence (new IBTTask[] { underAttack, checkMate, checkDistanceFromMate, moveNear });

		BTDecorator rootUntilFail = new BTDecoratorUntilFail (helpSequence);

		behaviourTree = new BehaviorTree(rootUntilFail);

		controller.StartCoroutine(PatrolBT());
	}

	public IEnumerator PatrolBT() {
		// while we have step or player is alive
		while (behaviourTree.Step() && controller.isAlive) {
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}

	// CONDITIONS
	
	// I'm attack while i'm moving
	private bool UnderAttack() {
		// if we recive an attack
		if (controller.isUnderAttack) {
			// if someone enter in the sphere of escape range
			if (controller.CheckCollision(controller.transform, controller.escapeRangeCollider)) {
				// if there are enemies in escape range
				if (controller.GetEnemiesInRange(controller.transform, controller.escapeRangeCollider).Count > 0) {
					return false;
				}
			}
		}
		return true;
	}

	// until fail check distance from mate
	private bool CheckDistanceFromMate() {
		// if there is a player
		KeyValuePair<GameObject, float> ally = controller.FindAllyPlayer();
		if (!ally.Equals(new KeyValuePair<GameObject, float>())) {
			// if ally is not near
			if (ally.Value >= controller.lowRangeCollider) {
				controller.playerAgent.isStopped = true;
				return true;
			}
		}
		// reset the decision tree
		dtPlayerIAMovement.StartMovementTree();
		return false;
	}

	// check if mate is still alive
	private bool CheckMate() {
		// if there is a player
		KeyValuePair<GameObject, float> ally = controller.FindAllyPlayer();
		if (!ally.Equals(new KeyValuePair<GameObject, float>())) {
			// if player is alive
			if (ally.Key != null) {
				return true;
			}
		}
		// reset the decision tree
		dtPlayerIAMovement.StartMovementTree();
		return false;
	}

	// ACTIONS

	// Reach the mate
	private bool MoveNear() {
		// get best destination 
		GameObject destination = controller.GetDestinationToTarget("playerAlly");
		if (destination != null) {
			controller.playerAgent.isStopped = false;
			// move to player position
			controller.playerAgent.SetDestination(destination.transform.position);
			return true;
		}
		// if ally is death or is in the enemies side of the map with two tower
		else {
			controller.playerAgent.isStopped = true;
			// exit from help
			return false;
		}
	}
}

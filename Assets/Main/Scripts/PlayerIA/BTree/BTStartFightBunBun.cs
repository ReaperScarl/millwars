﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BTStructure;

public class BTStartFightBunBun {

	private BehaviorTree behaviourTree;
	public BTStartFightBunBun(PlayerIAController controller) {
		this.controller = controller;
	}
	private PlayerIAController controller;
	// current target enemy
	private KeyValuePair<GameObject, float> currentEnemyTarget = new KeyValuePair<GameObject, float>();
	private GameObject target;
	
	// it uses dt to know from which DT it derives 
	public void StartFightBunBunBTree () {

		BTAction hookTarget = new BTAction (HookTarget);
		BTAction moveNear = new BTAction (MoveNear);
		BTAction escapeFromTower = new BTAction (EscapeFromTower);
		BTAction useTripleBulletAbility = new BTAction (UseTripleBulletAbility);
		BTAction useMineAbility = new BTAction (UseMineAbility);
		BTAction useBombAbility = new BTAction (UseBombAbility);
		BTAction useBaseAttack = new BTAction (UseBaseAttack);

		BTCondition checkPlayerLife = new BTCondition (CheckPlayerLife);
		BTCondition checkTarget = new BTCondition (CheckTarget);
		BTCondition checkPlayerLifeInUntilFail = new BTCondition (CheckPlayerLifeInUntilFail);
		BTCondition checkAroundTower = new BTCondition (CheckAroundTower);
		BTCondition checkTargetIsAlive = new BTCondition (CheckTargetIsAlive);
		BTCondition checkDistanceFromTarget = new BTCondition (CheckDistanceFromTarget);
		BTCondition checkTripleBulletAbility = new BTCondition (CheckTripleBulletAbility);
		BTCondition checkMineAbility = new BTCondition (CheckMineAbility);
		BTCondition checkBombAbility = new BTCondition (CheckBombAbility);
		BTCondition checkBaseAttack = new BTCondition (CheckBaseAttack);

		BTSequence tripleBulletSequence = new BTSequence (new IBTTask[] { checkTripleBulletAbility, useTripleBulletAbility });
		BTSequence mineSequence = new BTSequence (new IBTTask[] { checkMineAbility, useMineAbility });
		BTSequence checkBombSequence = new BTSequence (new IBTTask[] { checkBombAbility, useBombAbility });
		BTSequence baseAttackSequence = new BTSequence (new IBTTask[] { checkBaseAttack, useBaseAttack });

		BTProbabilisticSelector checkAttackSelector = new BTProbabilisticSelector (new IBTTask[] { tripleBulletSequence, mineSequence, checkBombSequence, baseAttackSequence }, new int[] {12, 12, 12, 70});

		BTSequence moveNearSequence = new BTSequence (new IBTTask[] { checkDistanceFromTarget, moveNear });

		BTDecorator moveNearUntilFail = new BTDecoratorUntilFail (moveNearSequence);

		BTSequence attackSequence = new BTSequence (new IBTTask[] { checkPlayerLifeInUntilFail, checkTargetIsAlive, moveNearUntilFail, checkAttackSelector });

		BTDecorator attackUntilFail = new BTDecoratorUntilFail (attackSequence);

		BTSequence checkTowerSequence = new BTSequence (new IBTTask[] { checkAroundTower, escapeFromTower});

		BTDecorator checkTowerUntilFail = new BTDecoratorUntilFail (checkTowerSequence);

		BTSequence rootSequence = new BTSequence (new IBTTask[] { checkPlayerLife, checkTarget, hookTarget, checkTowerUntilFail, attackUntilFail});

		BTDecorator rootUntilFail = new BTDecoratorUntilFail (rootSequence);

		behaviourTree = new BehaviorTree(rootUntilFail);

		controller.StartCoroutine(PatrolBT());
	}

	public IEnumerator PatrolBT() {
		// while we have step or player is alive
		while (behaviourTree.Step() && controller.isAlive) {
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}

	// CONDITIONS
	
	// check my health bar
	private bool CheckPlayerLife() {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			// stop coroutine and restart dt base on the derived tree
			DTPlayerIAAttack dtPlayerIAAttack = new DTPlayerIAAttack(controller);
			dtPlayerIAAttack.StartAttackTree();
			return false;
		}
		return true;
	}

	// check my health bar inside until fail, so not stop tree 
	private bool CheckPlayerLifeInUntilFail() {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			return false;
		}
		return true;
	}

	// check if there are a targets
	private bool CheckTarget() {
		DTPlayerIAAttack dtPlayerIAAttack;
		Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider);
		Dictionary<GameObject, float> enemiesTower = controller.EnemyTowers();
		Dictionary<GameObject, float> alliesList = controller.FindAllies();
		if (enemiesList.Count > 0 && enemiesTower.Count > 0) {
			foreach (KeyValuePair<GameObject, float> enemy in enemiesList) {
				// if there is a tower and enemies to attack
				if (enemy.Key != null && enemy.Key.GetComponent<TurretStats>() != null) {
					int count = 0;
					foreach (KeyValuePair<GameObject, float> ally in alliesList) {
						// if there is a tower and allies under
						if (ally.Key != null && enemiesTower.First().Key.GetComponent<TurretStats>().visionRange >= Vector3.Distance(enemiesTower.First().Key.transform.position, ally.Key.transform.position)) {
							count++;
						}
					}
					if (count >= 3) {
						controller.targetList.Clear();
						// enemies are now the target
						controller.targetList.Add(enemiesTower.First().Key, enemiesTower.First().Value);
						return true;
					}
					else {
						// if i can't attack stop coroutine and restart dt base on the derived tree
						dtPlayerIAAttack = new DTPlayerIAAttack(controller);
						dtPlayerIAAttack.StartAttackTree();
						// don't hook target
						return false;
					}
				}
				else {
					// if enemy is not in the range of the tower
					if (Vector3.Distance(enemy.Key.transform.position, enemiesTower.First().Key.transform.position) >= enemiesTower.First().Key.GetComponent<TurretStats>().visionRange) {
						controller.targetList.Clear();
						// enemies are now the target
						controller.targetList = enemiesList;
						return true;
					}
				}
			}
		}
		// attack first enemy
		else if (enemiesList.Count > 0 && enemiesTower.Count == 0) {
			controller.targetList.Clear();
			// enemies are now the target
			controller.targetList = enemiesList;
			return true;
		}
		// stop coroutine and restart dt base on the derived tree
		dtPlayerIAAttack = new DTPlayerIAAttack(controller);
		dtPlayerIAAttack.StartAttackTree();
		return false;
	}

	// check if i can destroy tower if is inside the field of view
	private bool CheckAroundTower() {
		Dictionary<GameObject, float> enemiesInField = controller.fieldOfView.GetEnemiesTarget();
		foreach (KeyValuePair<GameObject, float> enemy in enemiesInField) {
			// if there is a tower
			if (enemy.Key != null && enemy.Key.GetComponent<TurretStats>() != null) {
				Dictionary<GameObject, float> alliesInField = controller.fieldOfView.GetAlliesTarget();
				// there are other target for the tower and I am not in the range of the tower
				if (alliesInField.Count > 2 && enemy.Key.GetComponent<TurretStats>().visionRange < enemy.Value) {
					// attack tower
					return false;
				}
				// there are other target for the tower and I am in the range of the tower
				else if (alliesInField.Count > 2 && enemy.Key.GetComponent<TurretStats>().visionRange > enemy.Value) {
					// if I am under attack escape
					if (controller.isUnderAttack && !CheckPlayerLifeInUntilFail()) {
						controller.isUnderAttack = false;
						currentEnemyTarget = enemy;
						target = enemy.Key;
						return true;
					}
					// stay and fight
					else {
						return false;
					}
				}
				// if I am under tower
				else if (enemy.Key.GetComponent<TurretStats>().visionRange > enemy.Value && !CheckPlayerLifeInUntilFail()) {
					Debug.Log(enemy.Key.name +  " " + enemy.Value);
					// escape
					currentEnemyTarget = enemy;
					target = enemy.Key;
					return true;
				}
				// stay
				else {
					return false;
				}
			}
		}
		return false;
	}

	// check if target is alive
	private bool CheckTargetIsAlive() {
		// target enemies is alive
		if (target == null) {
			return false;
		}
		return true;
	}

	// check if player is near target
	private bool CheckDistanceFromTarget() {
		// if enemies is near or enemies is death
		if (target == null || target != null && Vector3.Distance(currentEnemyTarget.Key.transform.position, controller.transform.position) <= controller.highRangeCollider - 1f) {
			controller.playerAgent.isStopped = true;
			return false;
		}
		return true;
	}

	// check if player can use triple bullet ability
	private bool CheckTripleBulletAbility() {
		if (controller.level > 1 && Time.time >= controller.bunBunTripleBulletTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use mine ability
	private bool CheckMineAbility() {
		if (controller.level > 2 && Time.time >= controller.bunBunMineTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use bomb ability
	private bool CheckBombAbility() {
		if (controller.level > 4 && Time.time >= controller.bunBunBombTimer && target != null) {
			return true;
		}
		return false;
	}

	// check if player can use base attack
	private bool CheckBaseAttack() {
		if (Time.time >= controller.bunBunBaseAttackTimer && target != null) {
			return true;
		}
		return false;
	}

	// ACTIONS

	// hook target found
	private bool HookTarget() {
		// hook target
		currentEnemyTarget = controller.targetList.First();
		target = controller.targetList.First().Key;
		return true;
	}

	// move near the target
	private bool MoveNear() {
		if (target != null) {
			// move to player position
			controller.playerAgent.isStopped = false;
			Vector3 position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
			controller.playerAgent.SetDestination(position);
		}
		return true;
	}

	// check if target is alive
	private bool EscapeFromTower() {
		// rotate player back
		Quaternion rotation = controller.transform.rotation;
		controller.playerAgent.isStopped = false;
		controller.transform.LookAt(2 * controller.transform.position - currentEnemyTarget.Key.transform.position);
		// escape
		controller.playerAgent.SetDestination(controller.transform.forward * (controller.highRangeCollider / 2f));
		// reset rotation
		controller.transform.rotation = rotation;
		return true;
	}

	// use the triple bullet attack ability
	private bool UseTripleBulletAbility() {
		// look enemy and attack
		if(target != null){
			controller.transform.LookAt(target.transform);
			controller.bunBunIAStats.gun.enemyPosition = target.transform;
			controller.bunBunIAStats.gun.CmdShootIA(2);
			controller.bunBunTripleBulletTimer = Time.time + controller.stats.A1Cooldown;	
		}
		return true;
	}

	// use the mine ability
	private bool UseMineAbility() {
		// look enemy and attack
		if(target != null){
			controller.transform.LookAt(target.transform);
			controller.bunBunIAStats.gun.enemyPosition = target.transform;
			controller.bunBunIAStats.gun.CmdShootIA(3, (float)controller.playerStats.attackStrength * controller.bunBunIAStats.mineAttackMultiplier);
			controller.bunBunMineTimer = Time.time + controller.stats.A2Cooldown;	
		}
		return true;
	}

	// use the immortality ability
	private bool UseBombAbility() {
		// look enemy and attack
		if(target != null){
			controller.transform.LookAt(target.transform);
			controller.bunBunIAStats.gun.enemyPosition = target.transform;
			controller.bunBunIAStats.gun.CmdShootIA(5, (float)controller.playerStats.attackStrength * controller.bunBunIAStats.bombDamageMultiplier);
			controller.bunBunBombTimer = Time.time + controller.stats.A4Cooldown;
		}

		return true;
	}

	// use the base attack
	private bool UseBaseAttack() {
		// look enemy and attack
		if(target != null){
			controller.transform.LookAt(target.transform);
			controller.bunBunIAStats.gun.enemyPosition = target.transform;
			controller.bunBunIAStats.gun.CmdShootIA(1);
			controller.bunBunBaseAttackTimer = Time.time + controller.stats.attackSpeed;	
		}
		return true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;
using UnityEngine.Networking;

//delegate that will start coroutines
public delegate IEnumerator IACoroutine();

public class PlayerIAController : PlayerControllerWithNet {

	// time to react before check the fsm status
	[Range(0, 1)] public float reactionTime;
	// collider to know if target is very near and playerIA have to stop or move back
	public float lowRangeCollider;
	// colldire know if I have allies or enemies near my field of view
	public float highRangeCollider;
	// collider to know if target is too far to stop escape
	public float escapeRangeCollider;
	// layer mask to prevent collision trigger with map
	public LayerMask obstacleMask;
	// under attack reaction time
	public float underAttackTime;
	// life limit before escape
	[Range(1, 10)] public int lifeAlarmController;
	// max distance to reach for help player ally
	public float maxDistanceForHelpPlayerAlly;
	[HideInInspector]
	// your area base position
	public Transform startPosition;

	// healthbar controller
	private float currentHP;

	[HideInInspector]
	public PlayerIAFSM fsm;
	[HideInInspector]
	public ExperienceManager experienceManager;
	[HideInInspector]
	public BoxerIAStats boxerIAStats;
	[HideInInspector]
	public BunBunIAStats bunBunIAStats;
	[HideInInspector]
	public PlayerStats playerStats;
	[HideInInspector]
	public FieldOfViewIA fieldOfView;
	[HideInInspector]
	public NavMeshAgent playerAgent;
	[HideInInspector]
	public DamageReceiver damageReciver;
	[HideInInspector]
	public PlayerColliderDmgRecevier playerCollider;

	// check if i was attack from someone
	[HideInInspector]
	public bool isUnderAttack;
	// target to rescue or escape
	[HideInInspector]
	public Dictionary<GameObject, float> targetList =  new Dictionary<GameObject, float>();
	// level of the player
	[HideInInspector]
	public int level;

	// Timer for ability Bun Bun
	[HideInInspector]
	public float bunBunBaseAttackTimer = 0;
	[HideInInspector]
	public float bunBunTripleBulletTimer = 0;
	[HideInInspector]
	public float bunBunMineTimer = 0;
	[HideInInspector]
	public float bunBunSmokeTimer = 0;
	[HideInInspector]
	public float bunBunBombTimer = 0;
	// Timer for ability Boxer
	[HideInInspector]
	public float boxerBaseAttackTimer = 0;
	[HideInInspector]
	public float boxerRotateAttackTimer = 0;
	[HideInInspector]
	public float boxerEnGuardeTimer = 0;
	[HideInInspector]
	public float boxerFireWeaponTimer = 0;
	[HideInInspector]
	public float boxerImmortalityTimer = 0;

	protected override void Start () {
		base.Start();
		playerStats = GetComponent<PlayerStats>();
		fieldOfView = GetComponent<FieldOfViewIA>();
		experienceManager = GetComponent<ExperienceManager>();
		playerAgent = GetComponent<NavMeshAgent>();
		damageReciver = GetComponent<DamageReceiver>();
		playerCollider = GetComponent<PlayerColliderDmgRecevier>();
		currentHP = playerStats.HP;
		// if is Boxer
		if (playerStats.playerClass == PlayersClass.Melee) {
			boxerIAStats = GetComponent<BoxerIAStats>();
		}
		// else BunBun
		else {
			bunBunIAStats = GetComponent<BunBunIAStats>();
		}
        //only if is server
        if (isServer)
        {
            // obtains the nexus ally position
            startPosition = FindAllyNexus().Key.transform;
            // get the level of the player
            level = experienceManager.GetLevel();
            // add all the movement points to the list
            CreateListOfPoints();
            // start finite state machine only
            fsm = new PlayerIAFSM(this);
            fsm.StartFSM();
            RpcActivatePlayerAgent(true);
        }
	}

    [ClientRpc]
    public void RpcActivatePlayerAgent(bool val)
    {
        playerAgent.enabled = val;
    }
	
	protected override void Update() {
		base.Update();
		if (currentHP > playerStats.HP) {
			isUnderAttack = true;
			currentHP = playerStats.HP;
		}
	}
	
	// create the list of movemement point of IA order by distance
	private Dictionary<GameObject, float> CreateListOfPoints() {
		// get all points
		GameObject[] points = GameObject.FindGameObjectsWithTag("RallyPoint");
		Dictionary<GameObject, float> listOfPoints =  new Dictionary<GameObject, float>();
		if (points.Length > 0) {
			for (int i = 0; i < points.Length; i++) {
				// add point to dictionary <transform, distance>
				listOfPoints.Add(points[i], Vector3.Distance(transform.position, points[i].transform.position));
			}
		}
		return listOfPoints.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// search ally order by distance
	public Dictionary<GameObject, float> FindAllies() {
		// get all allies
		GameObject[] allies = GameObject.FindGameObjectsWithTag(playerStats.tag);
		Dictionary<GameObject, float> listOfAllies =  new Dictionary<GameObject, float>();
		if (allies.Length > 0) {
			for (int i = 0; i < allies.Length; i++) {
				// add ally to dictionary <transform, distance>
				listOfAllies.Add(allies[i], Vector3.Distance(transform.position, allies[i].transform.position));
			}
		}
		return listOfAllies.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// check collision with something in a range and it is not the map layer
	public bool CheckCollision(Transform player, float range) {
		return Physics.CheckSphere(player.position, range, obstacleMask);
	}

	public Dictionary<GameObject, float> GetEnemiesInRange(Transform player, float range) {
		// capture each element that pass inside the radious of view
		Collider[] targetInViewRadious = Physics.OverlapSphere(player.position, range);
		Dictionary<GameObject, float> visibleTargetList = new Dictionary<GameObject, float>();
		for (int i = 0; i < targetInViewRadious.Length; i++) {
			GameObject target = targetInViewRadious[i].transform.gameObject;
			//ignore landmine
			if (target.transform.parent != null && target.transform.parent.tag != null && target.transform.parent.tag == "landmine")
				continue;
			// if is an enemy
			bool condition = target != player && target.transform.parent != null && target.transform.parent.tag != null && target.transform.parent.tag != "Untagged" && player.tag != target.transform.parent.tag;
			if (condition || target != player && target != null && target.tag != null && target.tag != "Untagged" && player.tag != target.tag) {
				//Debug.Log (target.name);
				if (target.transform.parent != null && target.transform.parent.GetComponent<TurretStats>() != null && !visibleTargetList.ContainsKey(target.transform.parent.gameObject)) {
					// add tower
					visibleTargetList.Add(target.transform.parent.gameObject, Vector3.Distance(player.transform.position, target.transform.parent.transform.position));
				}
				else if (target.transform.parent != null && !visibleTargetList.ContainsKey(target.transform.parent.gameObject)) {
					// add enemy
					visibleTargetList.Add(target, Vector3.Distance(player.transform.position, target.transform.position));
				}
				else if(!visibleTargetList.ContainsKey(target.transform.gameObject) && target.name != "Base"){
					//add enemy player
					visibleTargetList.Add(target, Vector3.Distance(player.transform.position, target.transform.position));
				}
			}
		}
		// list is order by ascending distance
		return visibleTargetList.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// Find ally towers in order by distance
	public Dictionary<GameObject, float> FindAllyTowers() {
		Dictionary<GameObject, float> allyTowers = new Dictionary<GameObject, float>();
		// get all the allies
		Dictionary<GameObject, float> allies = FindAllies();
		int count = 0;
		foreach (KeyValuePair<GameObject, float> ally in allies) {
			// if is a tower add
			if (ally.Key.GetComponent<TurretStats>() != null) {
				allyTowers.Add(ally.Key, ally.Value);
				count++;
				// if tower are two
				if (count > 1) {
					break;
				}
			}
		}
		// list is order by ascending distance
		return allyTowers.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// Find ally player in order by distance
	public KeyValuePair<GameObject, float> FindAllyPlayer() {
		KeyValuePair<GameObject, float> allyPlayer = new KeyValuePair<GameObject, float>();
		// get all the allies
		Dictionary<GameObject, float> allies = FindAllies();
		foreach (KeyValuePair<GameObject, float> ally in allies) {
			// if is a tower add
			if (ally.Key.GetComponent<PlayerStats>() != null) {
				allyPlayer = new KeyValuePair<GameObject, float>(ally.Key, ally.Value);
				break;
			}
		}
		// list is order by ascending distance
		return allyPlayer;
	}

	// Find ally nexus in order by distance
	public KeyValuePair<GameObject, float> FindAllyNexus() {
		KeyValuePair<GameObject, float> allyNexus = new KeyValuePair<GameObject, float>();
		// get all the allies
		Dictionary<GameObject, float> allies = FindAllies();
		foreach (KeyValuePair<GameObject, float> ally in allies) {
			// if is a tower add
			if (ally.Key.GetComponent<NexusStats>() != null) {
				allyNexus = new KeyValuePair<GameObject, float>(ally.Key, ally.Value);
				break;
			}
		}
		// list is order by ascending distance
		return allyNexus;
	}

	// reach the target
	public GameObject GetDestinationToTarget(string metchComponent) {
		// if the target is not the nexus
		if (metchComponent == "playerAlly") {
			KeyValuePair<GameObject, float> allyPlayer = FindAllyPlayer();
			// if player is alive
			if (allyPlayer.Equals(new KeyValuePair<GameObject, float>())) {
				Dictionary<GameObject, float> escapePoint = new Dictionary<GameObject, float>();
				Dictionary<GameObject, float> points = CreateListOfPoints();
				// used to know the ally side of the map
				float escapeDistance = Vector3.Distance(startPosition.position, Vector3.zero);
				// if ally player is in the ally side of the map
				if (escapeDistance < Vector3.Distance(startPosition.position, allyPlayer.Key.transform.position)) {
					// return player transform
					return allyPlayer.Key;
				}
				else {
					// get enemies towers
					Dictionary<GameObject, float> enemiesTowers = EnemyTowers();
					// if there is just one tower
					if (enemiesTowers.Count < 1) {
						// used to know tower side on the map
						float targetTowerdistance = Vector3.Distance(enemiesTowers.First().Key.transform.position, Vector3.zero);
						foreach (KeyValuePair<GameObject, float> point in points) {
							// if point is in the enemy side of the map
							if (escapeDistance > Vector3.Distance(startPosition.position, point.Key.transform.position)) {
								// if point is in the side of the map without tower
								if (targetTowerdistance > Vector3.Distance(enemiesTowers.First().Key.transform.position, point.Key.transform.position)) {
									// add point						
									escapePoint.Add(point.Key, point.Value);
								}
							}
						}
						// return the first point for distance
						return escapePoint.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value).First().Key;
					}
				}
			}
		}
		// if the target is nexus I am sure that there is no tower or just one
		else {
			// if there are towers
			if (EnemyTowers().Count > 0) {
				// choose the nearest
				return EnemyTowers().First().Key;
			}
			// no tower
			else {
				// target is the nexus, the path doesn't metter
				return EnemyNexus().Key;
			}
		}
		return EnemyNexus().Key;
	}

	// return enemies towers
	public Dictionary<GameObject, float> EnemyTowers() {
		Dictionary<GameObject, float> enemyTowers = new Dictionary<GameObject, float>();
		// get all enemies
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(EnemiesTag());
		int count = 0;
		// if there are allies
		if (enemies.Length > 0) {
			for (int i = 0; i < enemies.Length; i++) {
				// if is a tower
				if (enemies[i].GetComponent<TurretStats>() != null) {
					// add tower to dictionary <transform, distance>
					enemyTowers.Add(enemies[i], Vector3.Distance(transform.position, enemies[i].transform.position));
					count++;
					// if tower are two
					if (count > 1) {
						break;
					}
				}
			}
		}
		// order enemies towers
		return enemyTowers.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// return enemies towers
	public KeyValuePair<GameObject, float> EnemyNexus() {
		KeyValuePair<GameObject, float> enemyNexus = new KeyValuePair<GameObject, float>();
		// get all enemies
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(EnemiesTag());
		// if there are allies
		if (enemies.Length > 0 ) {
			for (int i = 0; i < enemies.Length; i++) {
				// if is a tower
				if (enemies[i].GetComponent<TurretStats>() != null) {
					// add tower to dictionary <transform, distance>
					enemyNexus = new KeyValuePair<GameObject, float>(enemies[i], Vector3.Distance(transform.position, enemies[i].transform.position));
					break;
				}
			}
		}
		// order enemies towers
		return enemyNexus;
	}

	// spawn fire
	public void FireAbility() {
		GameObject newFire = Instantiate(boxerIAStats.fireWeapon.gameObject, boxerIAStats.weaponObject.position, boxerIAStats.weaponObject.rotation, boxerIAStats.weaponObject);
        Destroy(newFire, boxerIAStats.burnAbilityDelay);
	}

	// get enemies tag
	private string EnemiesTag() {
		if (transform.tag == "red") {
			return "blue";
		}
		else {
			return "red";
		}
	}

	[ClientRpc]
	public override void RpcDestroyPlayer(){
		playerAgent.enabled = false;
		//set player dead
		isAlive = false;
		//disable the gameObject
		gameObject.SetActive(false);
		//put the game object out of the map to not have problems with environment
		transform.position = new Vector3(0f, -20f, 0);
		//prepare the respawn canvas if is the local player
		if (isServer) {
			RespawnCanvasController canv = Instantiate (respawnCanvas);
			canv.SetCountdown (secondsToRespawn, this);
			canv.GetComponent<Canvas> ().enabled = false;
		}
	}

	[ClientRpc]
	public override void RpcRespawnPlayer(){
		//set player alive
		isAlive = true;
		//enable the gameObject
		this.gameObject.SetActive(true);
		stats.HP = stats.maxHealth;
		little_lifebar.SetConversionPToB(stats.maxHealth);
		little_lifebar.ChangeTheBar(-stats.HP);
		if (isServer) {
			//set the spawn position
			SetSpawnPoint();
		}
		playerAgent.enabled = true;
		// restart finite state machine
		fsm = new PlayerIAFSM(this);
		fsm.StartFSM();
		fieldOfView.StartCoroutine("FindTargetsWithDelay");
	}

	//function to upgrade attributes of AI
	public void IAupgradeAbility(int level){
		if (stats.playerClass == PlayersClass.Melee)
			IAupgradeBoxer (level);
		else
			IAupgradeBunBun (level);
	}

	private void IAupgradeBunBun(int level) {
		if (level >= 2)
		{
			stats.A1Cooldown -= 1f;
			stats.maxHealth += 15;
			stats.healthRegeneration += 0.2f;
			stats.defense += 0.2f;
			stats.perforation += 0.2f;
			stats.attackStrength += 2;
			stats.attackSpeed -= 0.2f;
		}
		if (level >= 3)
		{
			stats.A2Cooldown -= 1f;

		}
		if (level >= 4)
		{
			stats.A3Cooldown -= 1f;
			bunBunIAStats.mineAttackMultiplier += 0.2f;
		}
		if (level >= 5)
		{
			stats.A4Cooldown -= 1f;
		}
		//base.CmdlevelUp(level);
	}

	private void IAupgradeBoxer(int level) {
		if(level >= 2)
		{
			stats.A1Cooldown -= 1f;
			stats.maxHealth += 15;
			stats.healthRegeneration += 0.5f;
			stats.defense += 0.2f;
			stats.perforation += 0.2f;
			stats.attackStrength += 1;
			stats.attackSpeed -= 0.1f;
		}
		if(level >= 3)
		{
			stats.A2Cooldown -= 1f;
			boxerIAStats.circularAttackMultiplier += 0.1f;
		}
		if(level >= 4)
		{
			stats.A3Cooldown -= 1f;
			boxerIAStats.stunDelayAbility += 0.5f;
		}
		if(level >= 5)
		{
			stats.A4Cooldown -= 1f;
			boxerIAStats.burnAbilityDelay += 0.5f;
		}
		//base.CmdlevelUp(level);
	}
}

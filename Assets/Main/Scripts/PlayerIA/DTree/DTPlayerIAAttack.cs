﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DTPlayerIAAttack {

	public DTPlayerIAAttack(PlayerIAController controller) {
		this.controller = controller;
	}
	private DecisionTree decisionTree;
	private PlayerIAController controller;
	private IEnumerator coroutine;


	public void StartAttackTree() {

		// Define actions
		DTAction switchToMovement = new DTAction(SwitchToMovement);
		DTAction startFight = new DTAction(StartFight);

		// Define decisions
		DTDecision checkPlayerLife = new DTDecision(CheckPlayerLife);
		DTDecision checkEnemies = new DTDecision(CheckEnemies);


		// Link action with decisions
		checkPlayerLife.AddLink(true, checkEnemies);
		checkPlayerLife.AddLink(false, switchToMovement);
		checkEnemies.AddLink(true, startFight);
		checkEnemies.AddLink(false, switchToMovement);

		// Setup my DecisionTree at the root node
		decisionTree = new DecisionTree(checkPlayerLife);
		// Start patroling
		coroutine = PatrolDT();
		controller.StartCoroutine(coroutine);
	}

	// ACTIONS

	// switch status of FSM to movement
	private object SwitchToMovement(object o) {
		// exit from decision tree 
		controller.StopCoroutine(coroutine);
		// switch fsm to movement state
		controller.fsm.SetStatus("movement");
		return null;
	}

	// attack the target with strategy
	private object StartFight(object o) {
		controller.StopCoroutine(coroutine);
		// if is Boxer switch to its attack script
		if (controller.playerStats.playerClass == PlayersClass.Melee) {
			BTStartFightBoxer startFightBoxer = new BTStartFightBoxer(controller);
			startFightBoxer.StartFightBoxerBTree();
		}
		// else BunBun switch to its attack script
		else {
			BTStartFightBunBun startFightBunBun = new BTStartFightBunBun(controller);
			startFightBunBun.StartFightBunBunBTree();
		}
		return null;
	}

	// DECISION

	// check my health bar
	private object CheckPlayerLife(object o) {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			return false;
		}
		return true;
	}

	// check enemies around me
	private object CheckEnemies(object o) {
		// if someone enter in the sphere of high range
		if (controller.CheckCollision(controller.transform, controller.highRangeCollider)) {
			// if there are enemies in high range
			if (controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider).Count > 0) {
				return true;
			}
		}
		return false;
	}

	// Take decision every interval, run forever
	public IEnumerator PatrolDT() {
		// if we stop the decision or player is dead stop coroutine
		while(controller.isAlive) {
			decisionTree.walk();
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class DTPlayerIAMovement {

	public DTPlayerIAMovement(PlayerIAController controller) {
		this.controller = controller;
	}
	private DecisionTree decisionTree;
	private PlayerIAController controller;
	private IEnumerator coroutine;


	public void StartMovementTree() {

		// Define actions
		DTAction switchToAttack = new DTAction(SwitchToAttack);
		DTAction switchToDefend = new DTAction(SwitchToDefend);
		DTAction escape = new DTAction(Escape);
		DTAction helpFriend = new DTAction(HelpFriend);
		DTAction reachTheTarget = new DTAction(ReachTheTarget);
		// not BTree for this action
		DTAction levelUp = new DTAction(LevelUp);

		// Define decisions
		DTDecision underAttack = new DTDecision(UnderAttack);
		DTDecision checkUnderTower = new DTDecision(CheckUnderTower);
		DTDecision checkPlayerLife = new DTDecision(CheckPlayerLife);
		DTDecision mateInDefense = new DTDecision(MateInDefense);
		DTDecision ourDefenseAreAttacked = new DTDecision(OurDefenseAreAttacked);
		DTDecision checkMateLife = new DTDecision(CheckMateLife);
		DTDecision checkIfFarAway = new DTDecision(CheckIfFarAway);
		DTDecision checkDistanceFromTarget = new DTDecision(CheckDistanceFromTarget);
		DTDecision checkLevelUp = new DTDecision(CheckLevelUp);

		// Link action with decisions
		underAttack.AddLink(true, checkPlayerLife);
		underAttack.AddLink(false, checkLevelUp);
		checkUnderTower.AddLink(true, escape);
		checkUnderTower.AddLink(false, switchToAttack);
		checkLevelUp.AddLink(true, levelUp);
		checkLevelUp.AddLink(false, mateInDefense);
		checkPlayerLife.AddLink(true, escape);
		checkPlayerLife.AddLink(false, checkUnderTower);
		mateInDefense.AddLink(false, ourDefenseAreAttacked);
		mateInDefense.AddLink(true, checkMateLife);
		ourDefenseAreAttacked.AddLink(true, checkDistanceFromTarget);
		ourDefenseAreAttacked.AddLink(false, switchToDefend);
		checkMateLife.AddLink(true, ourDefenseAreAttacked);
		checkMateLife.AddLink(false, checkIfFarAway);
		checkIfFarAway.AddLink(true, ourDefenseAreAttacked);
		checkIfFarAway.AddLink(false, helpFriend);
		checkDistanceFromTarget.AddLink(true, switchToAttack);
		checkDistanceFromTarget.AddLink(false, reachTheTarget);

		// Setup my DecisionTree at the root node
		decisionTree = new DecisionTree(underAttack);

		// Start patroling
		coroutine = PatrolDT();
		controller.StartCoroutine(coroutine);
	}

	// ACTIONS

	// switch status of FSM to attack
	private object SwitchToAttack(object o) {
		// exit from decision tree
		controller.StopCoroutine(coroutine);
		// switch fsm to movement state
		controller.fsm.SetStatus("attack");
		return null;
	}

	// switch status of FSM to defend
	private object SwitchToDefend(object o) {
		// exit from decision tree
		controller.StopCoroutine(coroutine);
		// switch fsm to movement state
		controller.fsm.SetStatus("defend");
		return null;
	}

	// run away from an enemie
	private object Escape(object o) {
		controller.StopCoroutine(coroutine);
		// switch to escape BTree
		BTEscape escapeBT = new BTEscape(controller);
		escapeBT.StartEscapeBTree();
		return null;
	}

	// move to the position of the friend to help him
	private object HelpFriend(object o) {
		controller.StopCoroutine(coroutine);
		// switch to helpFriend BTree
		BTHelpFriend helpFriendBT = new BTHelpFriend(controller);
		helpFriendBT.StartHelpFriendBTree();
		return null;
	}

	// reach the target to defend
	private object ReachTheTarget(object o) {
		controller.StopCoroutine(coroutine);
		// switch to reachTheTarget BTree
		BTReachTheTarget reachTheTargetBT = new BTReachTheTarget(controller);
		reachTheTargetBT.StartReachTheTargetBTree("movement", controller.targetList.First().Key);
		return null;
	}

	// level up one of the two attribute of the player
	private object LevelUp(object o) {
		float rnd = Random.Range(0, 1);
		// choose the left
		if (rnd >= 0.5) {
			controller.experienceManager.upgradeAttributes(true);
		}
		// or right brench
		else {
			controller.experienceManager.upgradeAttributes(false);
		}
		// set new level
		controller.level++;
		return null;
	}

	// DECISION

	// I'm attack while i'm moving
	private object UnderAttack(object o) {
		// if we recive an attack
		if (controller.isUnderAttack) {
			controller.isUnderAttack = false;
			// if someone enter in the sphere of high range
			if (controller.CheckCollision(controller.transform, controller.highRangeCollider)) {
				// if there are enemies in high range
				if (controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider) != null) {
					return true;
				}
			}
		}
		return false;
	}

	// check my health bar
	private object CheckUnderTower(object o) {
		Dictionary<GameObject, float> enemiesTower = controller.EnemyTowers();
		// if player is under tower
		if (enemiesTower.First().Key != null && enemiesTower.First().Key.GetComponent<TurretStats>() != null && enemiesTower.First().Key.GetComponent<TurretStats>().visionRange < enemiesTower.First().Value) {
			return true;
		}
		return false;
	}

	// check my health bar
	private object CheckPlayerLife(object o) {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			return true;
		}
		return false;
	}

	// check if ally is under attack by multiple enemies
	private object MateInDefense(object o) {
		// if there is a player
		KeyValuePair<GameObject, float> ally = controller.FindAllyPlayer();
		// if someone enter in the sphere of escape range
		if (!ally.Equals(new KeyValuePair<GameObject, float>()) && controller.CheckCollision(ally.Key.transform, controller.escapeRangeCollider)) {
			// get enemies in range
			Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(ally.Key.transform, controller.escapeRangeCollider);
			// if there are more than two enemies in escape range
			if (enemiesList.Count > 0 && enemiesList.Count >= 2) {
				return true;
			}
		}
		return false;
	}

	// our defense are under attack 
	private object OurDefenseAreAttacked(object o) {
		controller.targetList.Clear();
		// find nexus
		KeyValuePair<GameObject, float> nexus = controller.FindAllyNexus();
		// if someone enter in the vision of nexus
		if(nexus.Key == null){ //this is a patch. If no nexus found, match is over and I stop the AI
			controller.isAlive = false;
			return false;
		}
		if (controller.CheckCollision(nexus.Key.transform, controller.highRangeCollider)) {
			// if there are enemies in escape range
			if (controller.GetEnemiesInRange(nexus.Key.transform, controller.highRangeCollider).Count > 0) {
				controller.targetList.Add(nexus.Key, nexus.Value);
				// very important reach immediately
				return true;
			}
		}
		// find tower
		Dictionary<GameObject, float> towers = controller.FindAllyTowers();
		foreach (KeyValuePair<GameObject, float> tower in towers) {
			// if someone enter in the sphere of tower vision range
			if (controller.CheckCollision(tower.Key.transform, tower.Key.GetComponent<TurretStats>().visionRange)) {
				// get enemies in range
				Dictionary<GameObject, float> enemiesList = controller.GetEnemiesInRange(tower.Key.transform, tower.Key.GetComponent<TurretStats>().visionRange);
				// if there are at least two enemies in vision range
				if (enemiesList.Count > 1) {
					controller.targetList.Add(tower.Key, tower.Value);
					return true;
				}
			}
		}
		return false;
	}

	// check the health bar of the ally
	private object CheckMateLife(object o) {
		// if there is a player
		KeyValuePair<GameObject, float> ally = controller.FindAllyPlayer();
		if (!ally.Equals(new KeyValuePair<GameObject, float>())) {
			// if player ally have alarm life
			if (ally.Key.GetComponent<PlayerStats>().HP <= ally.Key.GetComponent<PlayerStats>().maxHealth / controller.lifeAlarmController) {
				return true;
			}
		}
		return false;
	}

	// check how much is distant from the player
	private object CheckIfFarAway(object o) {
		// if there is a player
		KeyValuePair<GameObject, float> ally = controller.FindAllyPlayer();
		if (!ally.Equals(new KeyValuePair<GameObject, float>())) {
			// if is not too far
			if (ally.Value <= controller.maxDistanceForHelpPlayerAlly) {
				return true;
			}
		}
		return false;
	}

	// check how much is distant from the target
	private object CheckDistanceFromTarget(object o) {
		// if target is not too far (the first is the nearest for sure)
		if (Vector3.Distance(controller.targetList.First().Key.transform.position, controller.transform.position) <= controller.highRangeCollider) {
			controller.playerAgent.isStopped = true;
			return true;
		}
		return false;
	}

	// check if player can level up
	private object CheckLevelUp(object o) {
		// if we can upgrade level
		if (controller.level != controller.experienceManager.GetLevel()) {
			return true;
		}
		return false;
	}

	// Take decision every interval, run forever
	public IEnumerator PatrolDT() {
		// if we stop the decision or player is dead stop coroutine
		while(controller.isAlive) {
			decisionTree.walk();
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}
}

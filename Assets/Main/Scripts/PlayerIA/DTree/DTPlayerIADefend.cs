﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DTPlayerIADefend {

	public DTPlayerIADefend(PlayerIAController controller) {
		this.controller = controller;
	}
	private DecisionTree decisionTree;
	private PlayerIAController controller;
	private IEnumerator coroutine;


	public void StartDefendTree() {

		// Define actions
		DTAction switchToMovement = new DTAction(SwitchToMovement);
		DTAction startFight = new DTAction(StartFight);
		DTAction reachTheTarget =  new DTAction(ReachTheTarget);

		// Define decisions
		DTDecision checkPlayerLife = new DTDecision(CheckPlayerLife);
		DTDecision checkEnemies = new DTDecision(CheckEnemies);


		// Link action with decisions
		checkPlayerLife.AddLink(true, switchToMovement);
		checkPlayerLife.AddLink(false, checkEnemies);
		checkEnemies.AddLink(true, startFight);
		checkEnemies.AddLink(false, reachTheTarget);

		// Setup my DecisionTree at the root node
		decisionTree = new DecisionTree(checkPlayerLife);

		// Start patroling
		coroutine = PatrolDT();
		controller.StartCoroutine(coroutine);
	}

	// ACTIONS

	// switch status of FSM to attack
	private object SwitchToMovement(object o) {
		// exit from decision tree
		controller.StopCoroutine(coroutine);
		// switch fsm to movement state
		controller.fsm.SetStatus("movement");
		return null;
	}

	// attack the target with strategy
	private object StartFight(object o) {
		controller.StopCoroutine(coroutine);
		// if is Boxer switch to its attack script
		if (controller.playerStats.playerClass == PlayersClass.Melee) {
			BTStartFightBoxer startFightBoxer = new BTStartFightBoxer(controller);
			startFightBoxer.StartFightBoxerBTree();
		}
		// else BunBun switch to its attack script
		else {
			BTStartFightBunBun startFightBunBun = new BTStartFightBunBun(controller);
			startFightBunBun.StartFightBunBunBTree();
		}
		return null;
	}

	// attack the target with strategy
	private object ReachTheTarget(object o) {
		controller.StopCoroutine(coroutine);
		controller.targetList.Clear();
		BTReachTheTarget reachTheTarget;
		Dictionary<GameObject, float> enemyTowers = controller.EnemyTowers();
		// if there are enemy tower
		if (enemyTowers.Count > 0) {
			//if there are more than 1 tower, there aren't free ways to nexus
			if (enemyTowers.Count > 1) {
				Dictionary<GameObject, float> allies = controller.FindAllies();
				foreach (KeyValuePair<GameObject, float> enemyTower in enemyTowers) {
					int count = 0;
					foreach (KeyValuePair<GameObject, float> ally in allies) {
						float distanceFromTower = Vector3.Distance(ally.Key.transform.position, enemyTower.Key.transform.position);
						// check if allies are attacking the tower
						if (enemyTower.Key.GetComponent<TurretStats>().visionRange >= distanceFromTower) {
							count++;
						}
					}
					// be sure to have some allies near the tower
					if (count >= 3) {	//MAGIC NUMBER
						// and attack it
						controller.targetList.Add(enemyTower.Key, enemyTower.Value);
						reachTheTarget = new BTReachTheTarget(controller);
						controller.StopCoroutine(coroutine);
						reachTheTarget.StartReachTheTargetBTree("defend", enemyTower.Key);
						return null;
					}
				}
				// reach the nearest tower
				controller.targetList.Add(enemyTowers.First().Key, enemyTowers.First().Value);
			}
			// there are no towers. attack nexus
			else {
				// attack enemy nexus
				KeyValuePair<GameObject, float> enemyNexus = controller.EnemyNexus();
				controller.targetList.Add(enemyNexus.Key, enemyNexus.Value);
			}
		}
		else {
			// attack enemy nexus
			KeyValuePair<GameObject, float> enemyNexus = controller.EnemyNexus();
			if (enemyNexus.Key != null)
				controller.targetList.Add (enemyNexus.Key, enemyNexus.Value);
			else
				controller.isAlive = false; 	//this is a patch. If no nexus found, match is over and I stop the AI
			return null;
		}
		//reach the target that I choose
		reachTheTarget = new BTReachTheTarget(controller);
		reachTheTarget.StartReachTheTargetBTree("defend", controller.targetList.First().Key);
		return null;
	}

	// DECISION

	// check enemies around me
	private object CheckEnemies(object o) {
		// if someone enter in the sphere of high range
		if (controller.CheckCollision(controller.transform, controller.highRangeCollider)) {
			// if there are enemies in high range
			if (controller.GetEnemiesInRange(controller.transform, controller.highRangeCollider).Count > 0) {
				return true;
			}
		}
		return false;
	}

	// check my health bar
	private object CheckPlayerLife(object o) {
		// if player have alarm life
		if (controller.playerStats.HP <= controller.playerStats.maxHealth / controller.lifeAlarmController) {
			return true;
		}
		return false;
	}

	// Take decision every interval, run forever
	public IEnumerator PatrolDT() {
		// if we stop the decision or player is dead stop coroutine
		while(controller.isAlive) {
			decisionTree.walk();
			yield return new WaitForSeconds(controller.reactionTime);
		}
	}
}

﻿using UnityEngine.Networking;


public class IAExperienceManager : ExperienceManager
{

	public override void Start(){
		if(GetComponent<PlayerIAController>() != null) return;
		player = GetComponent<PlayerControllerWithNet>();
	}

	protected override void LevelUp(){
		if (level < 5 && experience >= thresholds [level - 1]) {
			experience = experience - thresholds [level - 1];
			level++;
			upgradeAbility ();
		}
	}

	public override void upgradeAttributes(bool left_branch) {
		//save the choosen branch
		if (left_branch)
			upgradeTree = upgradeTree.left;
		else
			upgradeTree = upgradeTree.right;
        //upgrade player stats
        CmdUpgradeTree();
	}

	[ClientRpc]
	public override void RpcAddExperience(_Stats.ObjectTypes type, bool half){
		if (!isServer) return;
		int exp = 0;
		switch (type) {
		case _Stats.ObjectTypes.boss:
			exp = bossExp;
			break;
		case _Stats.ObjectTypes.turret:
			exp = towerExp;
			break;
		case _Stats.ObjectTypes.champion:
			exp = championExp;
			break;
		case _Stats.ObjectTypes.minion:
			exp = minionExp;
			break;
		default:
			//not an object type with an experience level
			return;
		}
		if (half) exp /= 2;
		experience += exp;
		LevelUp ();
	}

	protected override void upgradeAbility(){
		GetComponent<PlayerIAController> ().IAupgradeAbility (level);
	}
}
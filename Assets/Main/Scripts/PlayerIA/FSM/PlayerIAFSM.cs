﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerIAFSM {

	public PlayerIAFSM(PlayerIAController controller){
		this.controller = controller;
	}

	// state of the player
	private enum PlayerFSMStates {
		Attack,
		Defend,
		Move
	};
	private PlayerIAController controller;
	private FSM fsmMachine;
	private PlayerFSMStates currentState;
	private PlayerFSMStates previousState;

	private DTPlayerIAMovement DTMovement;
	private DTPlayerIAAttack DTAttack;
	private DTPlayerIADefend DTDefend;

	public void StartFSM() {

		// Define states and link actions when enter/exit/stay
		FSMState movementAction = new FSMState();
		movementAction.enterActions = new FSMAction[] {EnterMoveTree};

		FSMState attackAction = new FSMState();
		attackAction.enterActions = new FSMAction[] {EnterAttackTree};

		FSMState defendAction = new FSMState();
		defendAction.enterActions = new FSMAction[] {EnterDefendTree};

		// Define transitions
		FSMTransition fromMovementToAttack = new FSMTransition (CheckIfAttack);
		FSMTransition fromMovementToDefend = new FSMTransition (CheckIfDefend);
		FSMTransition fromAttackToMovement= new FSMTransition (CheckIfMove);
		FSMTransition fromDefendToAttack = new FSMTransition (CheckIfMove);

		// Link states with transitions
		movementAction.AddTransition (fromMovementToAttack, attackAction);
		movementAction.AddTransition (fromMovementToDefend, defendAction);
		attackAction.AddTransition (fromAttackToMovement, movementAction);
		defendAction.AddTransition (fromDefendToAttack, movementAction);


		// set current state
		currentState = PlayerFSMStates.Move;
		// Setup a FSA at initial state
		fsmMachine = new FSM(movementAction);

		// Start monitoring
		controller.StartCoroutine(PatrolFSM());
	}

	// Periodic update, run forever
	private IEnumerator PatrolFSM() {
		while(controller.isAlive) {
			// if player is alive and change is current state
			if (currentState != previousState) {
				previousState = currentState;
				fsmMachine.Update();
			}
			yield return new WaitForSeconds(1f);
		}
	}
		
	// CONDITIONS

	// check status and return true if is attacking
	private bool CheckIfAttack () {
		return currentState == PlayerFSMStates.Attack;
	}

	// check status and return true if is moving
	private bool CheckIfMove () {
		return currentState == PlayerFSMStates.Move;
	}

	// check status and return true if is defending
	private bool CheckIfDefend () {
		return currentState == PlayerFSMStates.Defend;
	}

	// ACTIONS

	// access to the decision tree of movement
	private void EnterMoveTree() {
		if(DTMovement == null)
			DTMovement = new DTPlayerIAMovement(controller);
		DTMovement.StartMovementTree();
	}

	// access to the decision tree of attack
	private void EnterAttackTree() {
		if(DTAttack == null)
			DTAttack = new DTPlayerIAAttack(controller);
		DTAttack.StartAttackTree();
	}

	// access to the decision tree of defend
	private void EnterDefendTree() {
		if(DTDefend == null)
			DTDefend = new DTPlayerIADefend(controller);
		DTDefend.StartDefendTree();
	}

	// set current FSM player status
	public void SetStatus(string state) {
		if (state == "attack") {
			currentState = PlayerFSMStates.Attack;
		}
		else if (state == "defend") {
			currentState = PlayerFSMStates.Defend;
		}
		else if (state == "movement") {
			currentState = PlayerFSMStates.Move;
		}
	}
}
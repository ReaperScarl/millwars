﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerIAStats : MonoBehaviour {

	// first ability multiplier
	public float circularAttackMultiplier;
	// second ability multiplier
	public float chargeAttackMultiplier;
	// third ability collateral damage
	public float burnDamageMultiplier;
	// time players burn
	public float burnAbilityDelay;
	// delay of stun
	public float stunDelayAbility;
	// burn ability is started or not
	[HideInInspector] public bool burnAbilityActive;
	// passive ability multiplier
	public float movementPowerUp;
	// time invincibile ability is active
	public float invincibleAbilityDelay;
	// reference to weapon of the player IA
	public Transform weaponObject;
	[HideInInspector]
	// reference to the weapon script
	public Weapon weapon;
	// reference to the particles effect
	public ParticleSystem fireWeapon;

	void Start() {
		weapon = GetComponent<Weapon>();
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class FieldOfViewIA : MonoBehaviour {

	public float refreshDelay = 0.2f;
	public float viewRadius = 20f;
	[Range(0, 360)] public float viewAngle;
	// mask of obstacle map
	public LayerMask obstacleMask;
	// number of ray spawned
	public float rayDensity;
	// add a mesh to the view
	public MeshFilter viewMeshFilter;
	// number of iteration to find edge
	public int edgeIteration;
	// even if both ray hits somethings there is a certain distance apart that will consider that two different object
	public float edgeDistanceThreshold;

	private Mesh viewMesh;
	// mantain all the player enemies inside the field of view
	private Dictionary<GameObject, float> visibleEnemiesList = new Dictionary<GameObject, float>();
	// mantain all the player allies inside the field of view
	private Dictionary<GameObject, float> visibleAlliesList = new Dictionary<GameObject, float>();

	private void Start() {
		viewMesh = new Mesh ();
		viewMesh.name = "View Mesh";
		viewMeshFilter.mesh = viewMesh;
		StartCoroutine ("FindTargetsWithDelay");
	}


	private IEnumerator FindTargetsWithDelay() {
		while (true) {
			yield return new WaitForSeconds (refreshDelay);
			FindVisibleTargets ();
		}
	}

	// it only gets called after the controller is update
	private void LateUpdate() {
		// update field of view
		DrawFieldOfView();
	}

	public void FindVisibleTargets() {
		visibleEnemiesList.Clear();
		visibleAlliesList.Clear();
		// capture each element that pass inside the radious of view
		Collider[] targetInViewRadious = Physics.OverlapSphere(transform.position, viewRadius);
		for (int i = 0; i < targetInViewRadious.Length; i++) {
			Transform target = targetInViewRadious[i].transform;
			if (target.parent != null && target.parent.tag != null && target.parent.tag == "landmine")
				continue;
			bool condition = target != transform && target.transform.parent != null && target.transform.parent.tag != null && target.transform.parent.tag != "Untagged" && transform.tag != target.transform.parent.tag;
			if (condition || target != transform && target != null && target.tag != null && target.tag != "Untagged" && transform.tag != target.tag) {
				// calculate direction of target based on player direction
				Vector3 dirToTarget = (target.position - transform.position).normalized;
				// check if target is in our field of view
				if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2) {
					// calculate distance from target
					float distToTarget = Vector3.Distance(transform.position, target.position);
					// check if is not a obstacle
					if (!Physics.Raycast(transform.position, dirToTarget, distToTarget, obstacleMask)) {
                        //this is a pezza: non va messa la base delle torri nei target possibili
                        if(target.name != "Base"){
                            // if is an enemy
                            if ((target.parent != null && transform.tag != target.parent.tag) || transform.tag != target.tag)
                            {
                                // if is a tower
                                if (target.parent != null && target.parent.GetComponent<TurretStats>() != null)
                                {
                                    // add tower to dictionary not just the base
                                    if (!visibleEnemiesList.ContainsKey(target.parent.gameObject))
                                    {
                                        visibleEnemiesList.Add(target.parent.gameObject, distToTarget);
                                    }
                                }
                                else
                                {
                                    // add target to dictionary
                                    visibleEnemiesList.Add(target.gameObject, distToTarget);
                                }
                            }
                            // else is an ally
                            else
                            {
                                if (target.parent.GetComponent<TurretStats>() != null)
                                {
                                    // add tower to dictionary not just the base
                                    if (!visibleAlliesList.ContainsKey(target.parent.gameObject))
                                    {
                                        visibleAlliesList.Add(target.parent.gameObject, distToTarget);
                                    }
                                }
                                else
                                {
                                    // add target to dictionary
                                    visibleAlliesList.Add(target.gameObject, distToTarget);
                                }
                            }
                        }
					}
				}
			}
		}
	}

	// generate the total amount of ray we cast
	private void DrawFieldOfView() {
		// ray we can cast per degree
		int stepCount = Mathf.RoundToInt(viewAngle * rayDensity);
		// how many degree are in each step
		float stepAngleSize = viewAngle / stepCount;
		// contain view point
		List<Vector3> viewPointsList = new List<Vector3>();
		// check if raycast hit or not an obstacle for edge calculation
		ViewCastInfo oldViewCast = new ViewCastInfo();
		for (int i = 0; i <= stepCount; i++) {
			// current chunck angle
			float angle = transform.eulerAngles.y - (viewAngle / 2) + (stepAngleSize * i);
			// create a raycast with usefull info
			ViewCastInfo newViewCast = ViewCast(angle);
			// after the first iteration when oldViewCast is set
			if (i > 0) {
				// if we exceed it return true
				bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
				if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded)) {
					// find the edge
					EdgeInfo edge =  FindEdge(oldViewCast, newViewCast);
					// add both to the viewPointsList
					if (edge.pointA != Vector3.zero) {
						viewPointsList.Add(edge.pointA);
					}
					else if (edge.pointB != Vector3.zero) {
						viewPointsList.Add(edge.pointB);
					}
				}
			}
			viewPointsList.Add(newViewCast.point);
			oldViewCast = newViewCast;
		}
		// number of view point consider origin point
		int vertexCount = viewPointsList.Count + 1;
		// vertices of our triangles of rays
		Vector3[] vertices = new Vector3[vertexCount];
		// array of total triangles
		int[] triangles = new int[(vertexCount - 2) * 3];
		// set the origin vector
		vertices[0] = Vector3.zero;
		for (int i = 0; i < vertexCount -1; i++) {
			vertices[i + 1] = transform.InverseTransformPoint(viewPointsList[i]);
			// set triangles, first is always origin
			if (i < vertexCount - 2) {
				triangles[i * 3] = 0;
				triangles[i * 3 + 1] = i + 1;
				triangles[i * 3 + 2] = i + 2;
			}
		}
		// reset the mesh
		viewMesh.Clear();
		viewMesh.vertices = vertices;
		viewMesh.triangles = triangles;
		viewMesh.RecalculateNormals();
	}


	// use the min and the max view cast to find a edge between obstacle and not obstacle
	private EdgeInfo FindEdge (ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
		float minAngle = minViewCast.angle;
		float maxAngle = maxViewCast.angle;
		Vector3 minPoint = Vector3.zero;
		Vector3 maxPoint = Vector3.zero;
		// each step of the loop cast out array in betwwen the min and the max angle
		// more we do this loop more accurate the edge detection is going to be
		for (int i = 0; i < edgeIteration; i++) {
			// find new angle
			float angle = (minAngle + maxAngle) / 2f;
			ViewCastInfo newViewCast = ViewCast(angle);
			// if we exceed it return true
			bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
			if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded) {
				minAngle = angle;
				minPoint = newViewCast.point;
			}
			else {
				maxAngle = angle;
				maxPoint = newViewCast.point;
			}
		}
		return new EdgeInfo(minPoint, maxPoint);
	}

	private ViewCastInfo ViewCast(float globalAngle) {
		Vector3 direction = DirFromAngle(globalAngle, true);
		RaycastHit hit;
		// if we hit something with a ray stop construction of it
		if (Physics.Raycast(transform.position, direction, out hit, viewRadius, obstacleMask)) {
			return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
		}
		else {
			// continue until the end of view
			return new ViewCastInfo(false, transform.position + (direction * viewRadius), viewRadius, globalAngle);
		}
	}

	// Get a direction from an angle in degree
	private Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
		if (!angleIsGlobal) {
			angleInDegrees += transform.eulerAngles.y;
		}
		// 90 - x = sin(90 -x) || cos(x)
		return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
	}

	// structure of raycast
	private struct ViewCastInfo {
		public bool hit;
		public Vector3 point;
		public float distance;
		public float angle;

		public ViewCastInfo (bool _hit, Vector3 _point, float _distance, float _angle) {
			hit = _hit;
			point = _point;
			distance = _distance;
			angle = _angle;
		}
	}

	// structure for edge raycast
	private struct EdgeInfo {
		public Vector3 pointA;
		public Vector3 pointB;

		public EdgeInfo(Vector3 _pointA, Vector3 _pointB) {
			pointA =_pointA;
			pointB = _pointB;
		}
	}

	// return the enemies in field of view
	public Dictionary<GameObject, float> GetEnemiesTarget() {
		return visibleEnemiesList.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}

	// return the allies in field of view
	public Dictionary<GameObject, float> GetAlliesTarget() {
		return visibleAlliesList.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
	}
}
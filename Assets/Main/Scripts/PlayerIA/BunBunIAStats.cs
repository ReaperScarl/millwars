﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunBunIAStats : MonoBehaviour {

	// second ability multiplier
	public float mineAttackMultiplier;
	// fourth ability collateral damage
	public float bombDamageMultiplier;
	// passive ability multiplier
	public float attackSpeedPowerUp;
	// smog ability ability time delay
	public float smogAbilityDelay;
	[HideInInspector]
	// gun of the player
	public Gun gun;

	void Start() {
		gun = GetComponent<Gun>();
	}
}

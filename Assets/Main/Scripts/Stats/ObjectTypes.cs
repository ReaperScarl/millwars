﻿using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Partial class with the enumerators
/// </summary>
public partial class _Stats : NetworkBehaviour {
   
    public enum ObjectTypes {turret, nexus, champion, minion, crazyhouse, boss };

    public enum Align {red, blue, crazyminion };

    public enum Type {melee, range };
}

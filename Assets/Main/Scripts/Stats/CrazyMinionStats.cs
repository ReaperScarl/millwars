﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Stats of the crazy Minions
/// </summary>
public class CrazyMinionStats : _Stats {

    public float damage;

    public float fireRate;

    public bool isMelee;

    public bool left;

    public bool vsLeftRed;

    public bool isReallyCrazy;

    [Range(1, 10)]
    public float movement; // to make it right after balance

    [Range(0.1f, 10)]
    public float visionRange;

    [Range(0.1f, 10)]
    [Tooltip("Depends if he's a melee or a range")]
    public float fightRange;

    public GameObject[] SpawnPoint = new GameObject[2];

    void Start () {

        if (GetComponentInChildren<ChangeHelmetColor>())
            GetComponentInChildren<ChangeHelmetColor>().ChangeColor();
    }

    // to understand where the target starts
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRange);
        Gizmos.DrawWireSphere(transform.position, fightRange);
    }
    
    public void SetTheTag()
    {
        if (isReallyCrazy)
            tag = "crazyminion";
        else
        {
            if (left)
                tag = (vsLeftRed) ? "blue" : "red";
            else
                tag = (vsLeftRed) ? "red" : "blue";
        }
    }


    /// <summary>
    /// For some reason, if the agent is enabled, you can't move in the correct position, so it is enabled after the movement
    /// </summary>
    public void GoToPosition() 
    {
        int i;
        i = (left) ? 0 : 1;
        gameObject.transform.position = SpawnPoint[i].transform.position;
        gameObject.GetComponent<NavMeshAgent>().enabled = true;
    }

    public void Stunned(float delay) //Routine called by the attack of Boxer
    {
        StartCoroutine(Stun(delay));
    }

    private IEnumerator Stun(float delay)
    {
        GetComponent<CrazyMinionChasingBehaviour>().enabled = false;
        GetComponent<CrazyMinionMovementBehaviour>().enabled = false;
        GetComponent<NavMeshAgent>().isStopped = true;
        //Debug.Log("Stunned");
        yield return new WaitForSeconds(delay);
        GetComponent<CrazyMinionChasingBehaviour>().enabled = true;
        GetComponent<CrazyMinionMovementBehaviour>().enabled = true;
        GetComponent<NavMeshAgent>().isStopped = false;
    }
}

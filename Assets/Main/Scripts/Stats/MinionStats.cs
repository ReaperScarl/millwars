﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;


/// <summary>
///  The class with the stats of the team minions
/// </summary>
public class MinionStats : _Stats {

    public float damage;

    public float fireRate;

    [Tooltip ("Should be red or blue")]
   
    // public string align;
    [SyncVar(hook ="OnChangedAlign")]
    public Align align;

    public bool isMelee;

    [Range(1, 10)]
    public float movement; // to make it right after balance

    [Range(0.1f, 10)]
    public float visionRange;

    [Range(0.1f, 10)]
    [Tooltip("Depends if he's a melee or a range")]
    public float fightRange;

    [Space(2)]
    public bool isCrazy;

    [Space(3)]
    [Header("Arrays")]
    public Material [] minioncolor = new Material[2];

    public GameObject[] SpawnPoint=new GameObject[2];

    void Start()
    {
        //change color from the align - to change after
        // i=0 red, 1 blue
        int i;
        i=(align == Align.red)  ?  0 :  1;   
        this.gameObject.transform.localPosition = SpawnPoint[i].transform.position;
    }

    // to understand where the target starts
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRange);
        Gizmos.DrawWireSphere(transform.position, fightRange);
    }

    /// <summary>
    /// For some reason, if the agent is enabled, you can't move in the correct position, so it enabled after the movement
    /// </summary>
    public void GoToPosition()
    {
        int i;
        i = (align == Align.red) ? 0 : 1;
        gameObject.transform.position = SpawnPoint[i].transform.position;
        gameObject.GetComponent<NavMeshAgent>().enabled = true;
    }

    public void Stunned (float delay)
    {
        StartCoroutine(Stun(delay));
    }

    private IEnumerator Stun(float delay)
    {
        GetComponent<MinionChasingBehaviour>().enabled = false;
        GetComponent<MinionMovementBehaviour>().enabled = false;
        GetComponent<NavMeshAgent>().isStopped = true;
        //Debug.Log("Stunned");
        yield return new WaitForSeconds(delay);
        GetComponent<MinionChasingBehaviour>().enabled = true;
        GetComponent<MinionMovementBehaviour>().enabled = true;
        GetComponent<NavMeshAgent>().isStopped = false;
    }

    void OnChangedAlign(Align newAlign)
    {
        tag = newAlign.ToString();
        if (GetComponentInChildren<ChangeHelmetColor>())
            GetComponentInChildren<ChangeHelmetColor>().ChangeColor();
    }
}

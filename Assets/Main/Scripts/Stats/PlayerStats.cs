﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum PlayersClass {
    Melee,
    Range
}
/// <summary>
/// Class for the player stats
/// </summary>
public class PlayerStats : _Stats {
	[SyncVar]
    public float healthRegeneration;
	[SyncVar]
    public float defense;
	[SyncVar]
    public float movementSpeed;
	[SyncVar]
    public float perforation;
	[SyncVar]
    public int attackStrength;
	[SyncVar]
    public float attackSpeed;

    public float attackRange;
	
    public float muzzleVelocity;
	
    public float helpRange;

    public PlayersClass playerClass;

    [SyncVar (hook ="OnChangedAlignment")]
    public string align;
    [SyncVar]
	public string playerName;
	[SyncVar]
    public float A1Cooldown;
	[SyncVar]
    public float A2Cooldown;
	[SyncVar]
    public float A3Cooldown;
	[SyncVar]
    public float A4Cooldown;

    private int minionModule;

    void Start () {
        this.HP = maxHealth;
        ScoreManager scoreman = GameObject.Find("Map").transform.Find("ScoreManager").GetComponent<ScoreManager>();
        minionModule = scoreman.minionsperpoint;
        scoreman.PlayersName(this.tag, playerName,this.transform);
    }

    public int GetMinionModule()
    {
        return minionModule;
    }
	
    public void OnChangedAlignment(string newAlign) //-> to change at the start
    {
        align = newAlign;
        //Debug.Log("Align: " + align);
        this.tag = (align == "red") ? "red" : "blue";
        transform.Find("Player").GetComponent<MeshRenderer>().material.color = (align == "red") ? Color.red : Color.blue;
    }
}

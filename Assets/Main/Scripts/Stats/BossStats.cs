﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Stats of the bosses
/// </summary>
public class BossStats : _Stats {

    [Range(1,50)]
    public float healthRegen = 15;

    [Range(5, 20)]
    public int damage = 5;

    [Range(1, 5)]
    public float attackRate = 2;

    [Range(1, 10)]
    public float movement = 2;

    [Range(3, 20)]
    public float fightRange = 5;

    public Type bosstype;
}

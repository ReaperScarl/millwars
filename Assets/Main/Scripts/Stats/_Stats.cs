﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// partent of all the stats
/// </summary>
public partial class _Stats : NetworkBehaviour {

    [Header("Attributes")]
    [SyncVar (hook ="OnHPChanged")]
    public float HP;

    [Range(10, 500)]
    [SyncVar]
    public float maxHealth;
    public ObjectTypes objectType;
    public float offSet;

    public void OnHPChanged(float newHP) {
		float diff = HP - newHP; // there are less hp the when it started
		//patch for an unknown bug
        if (newHP>0 && isServer)
		    CmdUpdateLifeBar(diff);
		if (newHP >0 && newHP < maxHealth)
			HP = newHP;
		else
			HP = maxHealth;
    }

	[Command]
	private void CmdUpdateLifeBar(float diff){
       // Debug.Log("Sono in cmd update");
		RpcUpdateLifeBar (diff);
	}

	[ClientRpc]
	private void RpcUpdateLifeBar(float diff){
        //Debug.Log ("Rpc update: " + diff);
        if (GetComponent<DamageReceiver>())
        {
            BarModifierManager bar = GetComponent<DamageReceiver>().GetBar();
            if(bar != null)
                bar.ChangeTheBar(diff);
        }
        
	}
}

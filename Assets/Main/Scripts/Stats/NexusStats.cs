﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Stats for the nexus
/// </summary>
public class NexusStats : _Stats {

    [Tooltip("Should be red or blue")]
    public Align align;
    public bool isCrazy=false;
}

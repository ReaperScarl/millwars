﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class for the tower/silo stats
/// </summary>
public class TurretStats : _Stats {

    public Align align;

    public float damage;

    public float fireRate;

    [Range(0.1f, 50)]
    public float visionRange;
	
    // to understand where the target starts
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, visionRange);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraUpDownNet : MonoBehaviour {

	
	public Transform player;
    public float maxUpRotation = 20f;
	public float maxDownRotation = 20f;
	public float verticalOffset = 0f;

	private float rotX = 0.0f; // rotation around the horizontal/x axis
	private float rotY = 0.0f; // rotation around the up/y axis

	private PlayerControllerWithNet playerController;

    private float distance;
    private float rotationXoffset;

	// Use this for initialization
	void Start () {
		Vector3 rot = transform.localRotation.eulerAngles;
		rotX = rot.x;
		rotY = rot.y;
        //pezza paurosa per lo spawn dei player
        if(transform.parent.GetComponentInParent<PlayerStats>().align == "red")
        {
            rotY = 180;
        }
		rotationXoffset = rotX;
		distance = Vector3.Distance (transform.position, player.transform.position);

		playerController = player.GetComponentInParent<PlayerControllerWithNet> ();
	}

    public void setCameraRotation(float val)
    {
        rotY = val;
    }
	
	// Update is called once per frame
	void Update () {
        if (!playerController.isLocalPlayer)
            return;
		//camera rotation
		float mouseY = -Input.GetAxis("Mouse Y");
		float mouseX = Input.GetAxis("Mouse X");
		rotY += mouseX * playerController.rotationSensitivity * Time.deltaTime;
		rotX += mouseY * playerController.rotationSensitivity * Time.deltaTime;
		rotX = Mathf.Clamp(rotX, rotationXoffset - maxUpRotation, rotationXoffset + maxDownRotation);
		Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
		transform.rotation = localRotation;

		//camera position
		Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
		Vector3 position = localRotation * negDistance + player.position;
		position = new Vector3 (position.x, position.y + verticalOffset, position.z);
		transform.position = position;
	}
}
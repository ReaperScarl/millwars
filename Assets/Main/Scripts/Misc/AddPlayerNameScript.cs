﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Class responsible for the name in matchMenu
/// </summary>
public class AddPlayerNameScript : MonoBehaviour {

    public Text nameText;

	void Start () {
        string playerName = PlayerPrefs.GetString("CurrentName");
        if (nameText)
            nameText.text = playerName;
        else
            GameObject.Find("NameField").GetComponent<InputField>().text=playerName;
	}
	
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// Class responsible for the background music
/// </summary>
public class MusicController : MonoBehaviour {

    // Inserted only the ones for the scene.
    public Transform[] menuMusics;
    public Transform [] matchMusics;
    public Transform[] resultMusics;

    private Object mManager;
    private GameObject music;
    private int index;

    void Start () {
        if (SceneManager.GetActiveScene().name != "Results")
            StartTheMusic(SceneManager.GetActiveScene().name);
        else
            StartCoroutine(WaitABit());

        StartCoroutine(UpdateTheMusic());
    }

   // Every second, it checks if the song is stopped
    public IEnumerator UpdateTheMusic()
    {
        yield return new WaitForSeconds(1);
        while(true)
        {
            if (music = GameObject.FindGameObjectWithTag("menuMusic"))
            {
                if (music != null || menuMusics.Length > 0)
                {
                    if (!music.GetComponent<AudioSource>().isPlaying)
                    {
                        GameObject.Destroy(music);
                        index++;
                        mManager = Instantiate(menuMusics[index % menuMusics.Length], transform.position, Quaternion.identity);
                        mManager.name = menuMusics[index % menuMusics.Length].name;
                        DontDestroyOnLoad(mManager);
                    }
                }
            }

            if (music = GameObject.FindGameObjectWithTag("matchMusic"))
            {
                if (music != null || matchMusics.Length > 0)
                {
                    if (!music.GetComponent<AudioSource>().isPlaying)
                    {
                        GameObject.Destroy(music);
                        index++;
                        mManager = Instantiate(matchMusics[index % matchMusics.Length], transform.position, Quaternion.identity);
                        mManager.name = matchMusics[index % matchMusics.Length].name;
                        DontDestroyOnLoad(mManager);
                    }
                }
            }

            if (music = GameObject.FindGameObjectWithTag("resultMusic"))
            {
                if (music != null || resultMusics.Length > 0)
                {
                    if (!music.GetComponent<AudioSource>().isPlaying)
                    {
                        GameObject.Destroy(music);
                        index++;
                        mManager = Instantiate(resultMusics[index % resultMusics.Length], transform.position, Quaternion.identity);
                        mManager.name = resultMusics[index % resultMusics.Length].name;

                        DontDestroyOnLoad(mManager);
                    }
                }               
            }
            yield return new WaitForSeconds(1);
        }
    }

    void StartTheMusic(string sceneName)
    {
        GameObject oldMusic;

        if ((SceneManager.GetActiveScene()).name == "MainMenu" || (SceneManager.GetActiveScene()).name == "MatchMenu") // menus scenes
        {
            if (!GameObject.FindGameObjectWithTag("menuMusic"))
            {
                index++;
                mManager = Instantiate(menuMusics[index % menuMusics.Length], transform.position, Quaternion.identity);
                mManager.name = menuMusics[index % menuMusics.Length].name;

                DontDestroyOnLoad(mManager);

                //check for wrong types of music
                if (oldMusic = GameObject.FindGameObjectWithTag("resultMusic"))
                    Destroy(oldMusic.gameObject);
                if (oldMusic = GameObject.FindGameObjectWithTag("matchMusic"))
                    Destroy(oldMusic.gameObject);
            }
            return;
        }

        if ((SceneManager.GetActiveScene()).name == "GameWithNet") // Game scene
        {
            if (!GameObject.FindGameObjectWithTag("matchMusic"))
            {
               
                index++;
                mManager = Instantiate(matchMusics[index % matchMusics.Length], transform.position, Quaternion.identity);
                mManager.name = matchMusics[index % matchMusics.Length].name;
                DontDestroyOnLoad(mManager);

                //check for wrong types of music
                if (oldMusic = GameObject.FindGameObjectWithTag("menuMusic"))
                    Destroy(oldMusic.gameObject);
                if (oldMusic = GameObject.FindGameObjectWithTag("resultMusic"))
                    Destroy(oldMusic.gameObject);
            }
            return;
        }
        if ((SceneManager.GetActiveScene()).name == "Results") // Result scene
        {
            if (!GameObject.FindGameObjectWithTag("resultMusic"))
            {
                //Check to do: check for the result. if he won, then 0 otherwise 1
                index = (GameObject.Find("ResultsManager").GetComponent<ResultsManager>().iWon) ? 0 : 1;
                mManager = Instantiate(resultMusics[index % resultMusics.Length], transform.position, Quaternion.identity);
                mManager.name = resultMusics[index % resultMusics.Length].name;
                DontDestroyOnLoad(mManager);

                //check for wrong types of music
                if (oldMusic = GameObject.FindGameObjectWithTag("menuMusic"))
                    Destroy(oldMusic.gameObject);
                if (oldMusic = GameObject.FindGameObjectWithTag("matchMusic"))
                    Destroy(oldMusic.gameObject);
            }
            return;
        }
    }

    IEnumerator WaitABit() //Need some time of delay because iwon is not immediately with the good value
    {
        yield return new WaitForSeconds(0.5f);
        StartTheMusic(SceneManager.GetActiveScene().name);
    }

}

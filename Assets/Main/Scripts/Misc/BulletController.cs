﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Class responsible for the control of the bullets for NPCs
/// </summary>
public class BulletController : NetworkBehaviour {

    public float bulletSpeed = 50f;
    [SyncVar]
    private Transform target;
    private int dmg;
    private DamageProvider dp;
    private Vector3 oldrotation;
    private float timer;

    private void Start()
    {
        oldrotation = transform.rotation.eulerAngles;
    }

    //Method call when attacking from ranged NPCs
    public void Seek (Transform t, float dmg)
    {
        target = t;
        this.dmg = (int)dmg;
        oldrotation = transform.rotation.eulerAngles;
    }
	
	void Update () {

            if (target == null)
            {
                Destroy(gameObject);
                return;
            }

            Vector3 dir = target.position - transform.position;
            float distanceThisFrame = bulletSpeed * Time.deltaTime;

            if (dir.magnitude <= distanceThisFrame)
            {
                HitTarget();
                return;
            }
            if (target)
            {
                transform.LookAt(target);
                Vector3 newrotation = transform.rotation.eulerAngles;
                if (oldrotation != newrotation)
                {
                    this.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
                    oldrotation = newrotation;
                }
            }
	}

    void HitTarget() //If the target is hit
    {
		dp = new DamageProvider();
        if (dp == null)
            return;
        else
        {
            dp.ProvideDamage(target, dmg);
            Destroy(gameObject);
        }
    }
}

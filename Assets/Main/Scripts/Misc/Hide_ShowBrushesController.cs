﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible to hide and show the bushes. Made to optimize the fps. To fix the bad input (depends on where it walks)
/// </summary>
public class Hide_ShowBrushesController : MonoBehaviour { // 

    GameObject redBrush;
    GameObject blueBrush;

    //1: The barrier is Showed; 0: The barrier Is Hide
    private bool [] blueBarrier= new bool [2];
    private bool[] redBarrier = new bool[2];

    PlayerControllerWithNet player;

    void Start() {
        redBrush = GameObject.Find("RedBrushes");
        blueBrush = GameObject.Find("BlueBrushes");
        player = GetComponent<PlayerControllerWithNet>();

        InizializeTheBushes();
    }

    public void UpdateTheBrush(int triggerNumber, bool isRed, bool value)
    {
        if (!player.isLocalPlayer)
            return;

        switch (triggerNumber)
        {
            case 0: //First trigger red or blue
                if (isRed) 
                {

                    redBarrier[triggerNumber] = value;
                    blueBrush.SetActive(redBarrier[triggerNumber]);
                }
                else
                {
                    blueBarrier[triggerNumber] = value;
                    redBrush.SetActive(blueBarrier[triggerNumber]);
                }

                break;
            case 1:  //Second trigger red or blue
                if (isRed) 
                {
                    redBarrier[triggerNumber] = value;
                    redBrush.SetActive(redBarrier[triggerNumber]);
                }
                else
                {
                    blueBarrier[triggerNumber] = value;
                    blueBrush.SetActive(blueBarrier[triggerNumber]);
                }
                break;
        }
    }

    public void InizializeTheBushes() // when the player is spawn, it has to put all this bushes false and active them in the process
    {
        if (!player.isLocalPlayer)
            return;

        if (this.tag == "red")
        {
            blueBarrier[0] = true;
            blueBarrier[1] = true;
            redBarrier[0] = false;
            redBarrier[1] = false;
            redBrush.SetActive(false);
            blueBrush.SetActive(false);
        }
        else
        {
            redBarrier[0] = true;
            redBarrier[1] = true;
            blueBarrier[0] = false;
            blueBarrier[1] = false;
            redBrush.SetActive(false);
            blueBrush.SetActive(false);
        }
    }

    public void FixTheBushesWhileDead()
    {
		if (!player.isLocalPlayer)
			return;
        redBrush.SetActive(true);
        blueBrush.SetActive(true);
    }
}

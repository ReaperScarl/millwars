﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible for the bushes info (in which positions they are)
/// </summary>
public class BrushesInfo : MonoBehaviour {

    public int TriggerNumber;
    public bool isRed;

    public void OnTriggerExit(Collider other)
    {
       

        if (other.GetComponent<Hide_ShowBrushesController>())
        {
            // if it is red triggers (part higher than the center), then if the trigger is behind the player, then the bush has to be up
            if (isRed)
            {
                if (transform.position.z > other.transform.position.z)
                    other.GetComponent<Hide_ShowBrushesController>().UpdateTheBrush(TriggerNumber, isRed, true);
                else
                    other.GetComponent<Hide_ShowBrushesController>().UpdateTheBrush(TriggerNumber, isRed, false);
            }
            else
            {
                if (transform.position.z < other.transform.position.z)
                    other.GetComponent<Hide_ShowBrushesController>().UpdateTheBrush(TriggerNumber, isRed, true);
                else
                    other.GetComponent<Hide_ShowBrushesController>().UpdateTheBrush(TriggerNumber, isRed, false);
            }
        }
    }
}

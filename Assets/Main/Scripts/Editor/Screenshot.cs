﻿using UnityEditor;
using UnityEngine;

public class Screenshot : MonoBehaviour {

    [MenuItem("Screenshot/Take Screenshot")]
    static void TakeScreenshot()
    {
        ScreenCapture.CaptureScreenshot("test.png");
    }
}

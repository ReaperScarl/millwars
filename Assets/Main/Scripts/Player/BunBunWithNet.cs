﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

public class BunBunWithNet : PlayerControllerWithNet {

	// second ability multiplier
	public float mineAttackMultiplier;
	// fourth ability collateral damage
	public float bombDamageMultiplier;
	// passive ability multiplier
	public float attackSpeedPowerUp;
	// smog ability is started or not
	private bool smogAbilityActive;
	// smog ability ability time delay
	public float smogAbilityDelay;
	public bool mineAbilityActive;
	//trajectory for special abilities
	public GameObject trajectory;
	// weapon of the player
	private Gun gun;

	//manager of unlocked abilities
	private AbilityManager abManager;

	// Use this for initialization
	protected override void Start () {
		// father start
		base.Start();
		gun = GetComponentInChildren<Gun>();
		// At start trajectory is hidden
		trajectory.SetActive(false);
		// set passive ability to player
		stats.movementSpeed += attackSpeedPowerUp * stats.movementSpeed;
		smogAbilityActive = false;
		mineAbilityActive = false;
		//get the ability manager
		abManager = playerCanvas;
        if (!isLocalPlayer) {
            //disable gun
            GetComponent<Gun>().enabled = false;
        }
	}

    // Update is called once per frame
    protected override void Update()
    {
        if (!isLocalPlayer) return;
        // father update
        base.Update();
        // basic attack
        if (CrossPlatformInputManager.GetButtonDown("BaseAttack") && !smogAbilityActive && !mineAbilityActive)
        {
            // if time to recal the attack is passed
            if (Time.time > stats.attackSpeed + nextTimeBaseAttack)
            {
                // attack
                gun.CmdShoot(1);
                // reset timer of countdown ability
                nextTimeBaseAttack = Time.time;
            }
        }
        // first ability
        else if (!abManager.isAbilityLocked(1) && CrossPlatformInputManager.GetButtonDown("Ability1"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A1Cooldown + nextTimeFirstAbility)
            {
                // attack
                gun.CmdShoot(2);
                // reset timer of countdown ability
                nextTimeFirstAbility = Time.time;
                //update canvas
                abManager.useAbility(1, stats.A1Cooldown);
            }
        }
        // second ability buttom down active trajectory
        else if (!abManager.isAbilityLocked(2) && CrossPlatformInputManager.GetButtonDown("Ability2") && !mineAbilityActive)
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A2Cooldown + nextTimeSecondAbility)
            {
                mineAbilityActive = true;
                trajectory.SetActive(true);
            }
        }
        // second  or third ability down button deactive ability
        else if ((!abManager.isAbilityLocked(2) && CrossPlatformInputManager.GetButtonDown("Ability2") && mineAbilityActive) || (!abManager.isAbilityLocked(3) && CrossPlatformInputManager.GetButtonDown("Ability3") && smogAbilityActive))
        {
            trajectory.SetActive(false);
            mineAbilityActive = false;
            smogAbilityActive = false;
        }
        // base attack down button start anility two
        else if (CrossPlatformInputManager.GetButtonDown("BaseAttack") && mineAbilityActive && !smogAbilityActive)
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A2Cooldown + nextTimeSecondAbility)
            {
                // attack
                gun.CmdShoot(3, (float)stats.attackStrength * mineAttackMultiplier);
                //update canvas
                abManager.useAbility(2, stats.A2Cooldown);
                mineAbilityActive = false;
                trajectory.SetActive(false);
                // reset timer of countdown ability
                nextTimeSecondAbility = Time.time;
            }
        }
        // third ability down button active trajectory
        else if (!abManager.isAbilityLocked(3) && CrossPlatformInputManager.GetButtonDown("Ability3") && !smogAbilityActive)
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A3Cooldown + nextTimeThirdAbility)
            {
                smogAbilityActive = true;
                trajectory.SetActive(true);
            }
        }
        // base attack down button start ability three
        if (CrossPlatformInputManager.GetButtonDown("BaseAttack") && smogAbilityActive && !mineAbilityActive)
        {
            // attack
            gun.CmdShoot(4, smogAbilityDelay);
            //update canvas
            abManager.useAbility(3, stats.A3Cooldown);
            smogAbilityActive = false;
            trajectory.SetActive(false);
            // reset timer of countdown ability
            nextTimeThirdAbility = Time.time;
        }
        // fourth ability
        else if (!abManager.isAbilityLocked(4) && CrossPlatformInputManager.GetButtonDown("Ability4"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A4Cooldown + nextTimeFourthAbility)
            {
                // attack
                gun.CmdShoot(5, (float)stats.attackStrength * bombDamageMultiplier);
                //update canvas
                abManager.useAbility(4, stats.A4Cooldown);
                // reset timer of countdown ability
                nextTimeFourthAbility = Time.time;
            }
        }
    }

    [Command]
    // the stats are in the script Upstats, where the variables can be modified in the editor
    // In this way the variables are easier to tune
    public void CmdlevelUpB(int level)
    {
        LevelUpStats Upstats = GetComponent<LevelUpStats>();
        Debug.Log("Command lvl Up");
        if (level > 1)
        {
            stats.A1Cooldown -= Upstats.cooldown;
            stats.maxHealth += Upstats.maxHealth;
            stats.healthRegeneration += Upstats.healthRegeneration;
            stats.defense += Upstats.defense;
            stats.perforation += Upstats.perforation;
            stats.attackStrength +=(int) Upstats.attackStrength;
            stats.attackSpeed -= Upstats.attackSpeed;

            if (level > 2)
            {
                stats.A2Cooldown -= Upstats.cooldown;
                if (level > 3)
                {
                    stats.A3Cooldown -= Upstats.cooldown;
                    mineAttackMultiplier += Upstats.mineAttackMultiplier;

                    if (level > 4)
                    {
                        stats.A4Cooldown -= Upstats.cooldown;
                    }
                }
            }
        }
    }
}

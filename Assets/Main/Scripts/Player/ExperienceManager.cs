﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

public class ExperienceManager : NetworkBehaviour {
	//class attributes for player experience stats
    [SyncVar]
	protected int experience = 0;
    [SyncVar]
	protected int level = 1;
	public int[] thresholds = { 30, 60, 120, 240 };

    //class attributes for ability upgrade
	protected AttributesTree upgradeTree;
    private int toUpgrade = 0;
    private GameObject leftUpgrader;
    private GameObject rightUpgrader;
    private bool gui_visible = false;

	//class attributes to know how much experience add
	public int minionExp = 2;
	public int championExp = 20;
	public int towerExp = 20;
	public int bossExp = 40;

	//class attribute to unlock and show new ability
	protected AbilityManager abilityManager;
	//class attribute for the experience bar
	public BarModifierManager expBar;
	//class attribute to show the actual level
	public Text actualLevelText;
	private string levelString = "Level: ";

    //player
    public PlayerControllerWithNet player;

    //initial setup of the class
	void Awake(){
        //get expbar
		expBar = GameObject.Find ("ExpBar").GetComponent<BarModifierManager> ();
		//get the actual level text
		actualLevelText = GameObject.Find("ActualLevel").GetComponent<Text>();
        //retrieve the attributes tree
        if (GetComponent<PlayerStats>().playerClass == PlayersClass.Melee)
            upgradeTree = AttributesManager.getMeleeTree();
        else upgradeTree = AttributesManager.getRangeTree();

	}

	public virtual void Start(){
		//empty expbar if is localPlayer
		if(isLocalPlayer)
			emptyExpBar (1);
		//setup ability manager
		abilityManager = GetComponent<PlayerControllerWithNet>().playerCanvas;
        //get gui upgrade gameobjects
        player = GetComponent<PlayerControllerWithNet>();
        leftUpgrader = player.playerCanvas.GetComponentInChildren<UpgradeGUI>(true).left;
        rightUpgrader = player.playerCanvas.GetComponentInChildren<UpgradeGUI>(true).right;
        leftUpgrader.SetActive(false);
        rightUpgrader.SetActive(false);
		//set the actual level
		updateLevelText();
    }

    //to handle when a player try to upgrade attributes
    private void FixedUpdate() {
        if (!gui_visible) return;
        
        if (CrossPlatformInputManager.GetButtonDown("LeftUpgrade")) {
            //left branch
            upgradeAttributes(true);
        }else if (CrossPlatformInputManager.GetButtonDown("RightUpgrade")) {
            //right branch
            upgradeAttributes(false);
        }
    }



    //function to add experience to the player
    [ClientRpc]
    public virtual void RpcAddExperience(_Stats.ObjectTypes type, bool half){
        if (!isLocalPlayer) return;
		int exp = 0;
		switch (type) {
		case _Stats.ObjectTypes.boss:
			exp = bossExp;
			break;
		case _Stats.ObjectTypes.turret:
			exp = towerExp;
			break;
		case _Stats.ObjectTypes.champion:
			exp = championExp;
			break;
		case _Stats.ObjectTypes.minion:
			exp = minionExp;
			break;
		default:
			//not an object type with an experience level
			return;
		}
        if (half) exp /= 2;
		experience += exp;
		expBar.ChangeTheBar (-exp);
		LevelUp ();
	}

	//function to check if a player made a level up and handle it (only local player)
	protected virtual void LevelUp(){
		if (!isLocalPlayer) return;
		if (level < 5 && experience >= thresholds [level - 1]) {
			experience = experience - thresholds [level - 1];
			level++;
            if(level != 5)
			    emptyExpBar (level);
			upgradeAbility ();
            Debug.Log ("level up to level " + level);
            ShowUpgradeAttributes();
			updateLevelText ();
		}
	}

    //function to show the upgrade data on the canvas
    private void ShowUpgradeAttributes(){
        if (!gui_visible){
            setGuiVisible(true);
            setupGui();
        }
        toUpgrade++;
    }

    //function to unlock the next ability of the player
	protected virtual void upgradeAbility(){
		abilityManager.unlockAbility (level);
        if(isClient)
            player.CmdlevelUp(level);
	}

    //function to empty the expbar when a levelup is made
	protected void emptyExpBar(int level) {
		expBar.SetConversionPToB (thresholds [level - 1]);	
		expBar.ChangeTheBar (thresholds[level - 1]);
		expBar.ChangeTheBar (- experience);
	}

    //function to set the GUI element visible/invisible
    private void setGuiVisible(bool value) {
		leftUpgrader.SetActive (value);
        rightUpgrader.SetActive(value);
        gui_visible = value;
    }

	//function to update the actual level of player
	private void updateLevelText(){
		actualLevelText.text = levelString + level;
	}

    //function to write the descriptions of the attributes upgrade to the canvas
    private void setupGui(){
        //setup left branch
		Text[] texts = leftUpgrader.GetComponentsInChildren<Text>();
        texts[0].text = upgradeTree.left.upgrader.name;
        texts[1].text = upgradeTree.left.upgrader.description;
        //setup right branch
		texts = rightUpgrader.GetComponentsInChildren<Text>();
        texts[0].text = upgradeTree.right.upgrader.name;
        texts[1].text = upgradeTree.right.upgrader.description;
    }

    //function called when the player want to upgrade the attributes
    public virtual void upgradeAttributes(bool left_branch) {
        //save the choosen branch
        if (left_branch)
            upgradeTree = upgradeTree.left;
        else
            upgradeTree = upgradeTree.right;
        //upgrade attributes
        CmdUpgradeTree();
        //decrement the number of level up to do
        toUpgrade--;
        //update GUI
        if (toUpgrade == 0){
            setGuiVisible(false);
        }
        else{
            setupGui();
        }
    }

    [Command]
    public void CmdUpgradeTree()
    {
        //upgrade player stats
        upgradeTree.upgrader.upgrade(GetComponent<PlayerStats>());
    }

    // return the level of the player
    public int GetLevel() {
        return level;
    }
}

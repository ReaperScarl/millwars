﻿using UnityEngine.Networking;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class BoxerJr : PlayerControllerWithNet {
	// weapon object
	public Transform weaponObject;
	// first ability multiplier
	public float circularAttackMultiplier;
	// second ability multiplier
	public float chargeAttackMultiplier;
	// third ability collateral damage
	public float burnDamageMultiplier;
	// time players burn
	public float burnAbilityDelay;
	// delay of stun
	public float stunDelayAbility;
	// burn ability is started or not
	public bool burnAbilityActive;
	// passive ability multiplier
	public float movementPowerUp;
	// time invincibile ability is active
	public float invincibleAbilityDelay;
	// damage provided by player
	[SyncVar(hook ="OnDamageChanged")]
	private int damage;
	// control immortality
	private float immortalityAbilityTimer;

	// weapon of the player
	private Weapon weapon;
	// fire particle
	public ParticleSystem fireToWeapon;

	//manager of unlocked abilities
	private AbilityManager abManager;

	public PlayerColliderDmgRecevier playerCollider;

	private void OnDamageChanged(int newD){
		if (damage != newD && isLocalPlayer) {
			CmdUpdateDamage (newD);
		}
		damage = newD;
	}

	[Command]
	private void CmdUpdateDamage(int newD){
		damage = newD;
	}

	// Use this for initialization
	protected override void Start () {
		// father start
		base.Start();
		damage = stats.attackStrength;
		weapon = GetComponent<Weapon>();
        // set passive ability to player
        stats.movementSpeed += movementPowerUp * stats.movementSpeed;
		//get the ability manager
		abManager = playerCanvas;
	}

    // Update is called once per frame
    protected override void Update()
    {
        if (!isLocalPlayer) return;
        // father update
        base.Update();
        // basic attack
        if (CrossPlatformInputManager.GetButtonDown("BaseAttack"))
        {
            // if time to recal the attack is passed
            if (Time.time > stats.attackSpeed + nextTimeBaseAttack)
            {
                // attack
                //Debug.Log(damage);
                weapon.CmdTotDamage(damage);
                weapon.Attack(1);
                // reset timer of countdown ability
                nextTimeBaseAttack = Time.time;
            }
        }
        // first ability
        else if (!abManager.isAbilityLocked(1) && CrossPlatformInputManager.GetButtonDown("Ability1"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A1Cooldown + nextTimeFirstAbility)
            {
                // reset timer of countdown ability
                nextTimeFirstAbility = Time.time;
                // attack
                weapon.CmdTotDamage((int)((float)damage * circularAttackMultiplier));
                weapon.Attack(2);
                abManager.useAbility(1, stats.A1Cooldown);
            }
        }
        // second ability
        else if (!abManager.isAbilityLocked(1) && CrossPlatformInputManager.GetButtonDown("Ability2"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A2Cooldown + nextTimeSecondAbility)
            {
                // reset timer of countdown ability
                nextTimeSecondAbility = Time.time;
                // attack
                weapon.CmdTotDamage((int)(damage * chargeAttackMultiplier));
                weapon.Attack(3);
				StartCoroutine (BurnTimerPowerUp ());
                abManager.useAbility(2, stats.A2Cooldown);
            }
        }
        // third ability
        else if (!abManager.isAbilityLocked(1) && CrossPlatformInputManager.GetButtonDown("Ability3"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A3Cooldown + nextTimeThirdAbility)
            {
                // reset timer of countdown ability
                nextTimeThirdAbility = Time.time;
                // start burn damage
                burnAbilityActive = true;
                // instantiate particle effect
                CmdMakeItBurn();
                weapon.Attack(4, burnAbilityDelay);
                abManager.useAbility(3, stats.A3Cooldown);
            }
        }
        // fourth ability
        else if (!abManager.isAbilityLocked(1) && CrossPlatformInputManager.GetButtonDown("Ability4"))
        {
            // if time to recal the ability is passed
            if (Time.time > stats.A4Cooldown + nextTimeFourthAbility)
            {
                // reset timer of countdown ability
                nextTimeFourthAbility = Time.time;
                // start invincibility
				CmdStartImmortality();
                abManager.useAbility(4, stats.A4Cooldown);
            }
        }
    }

	[Command]
	private void CmdStartImmortality(){
		StartCoroutine(CheckImmortality());
	}

    // Check if we have to remove the burn damage multiplier
	private IEnumerator BurnTimerPowerUp() {
		// add damage of fire
		damage = (int)((float)damage * burnDamageMultiplier);
		yield return new WaitForSeconds (burnAbilityDelay);
		// remove damage of fire
		damage /= (int)((float)damage / burnDamageMultiplier); // ma ha dato un divided by 0 exception. stare attenti
		burnAbilityActive = false;
	}

    [ClientRpc]
    public void RpcMakeItBurn()
    {
        GameObject fire = Instantiate(fireToWeapon.gameObject, weaponObject.position, fireToWeapon.transform.rotation, weaponObject);
        Destroy(fire, burnAbilityDelay);
    }

    [Command]
    public void CmdMakeItBurn()
    {
        RpcMakeItBurn();
    }

	// check immortality ability
	private IEnumerator CheckImmortality() {
		playerCollider.invincible = true;
		yield return new WaitForSeconds (invincibleAbilityDelay);
		playerCollider.invincible = false;
	}

    [Command]
    // the stats are in the script Upstats, where the variables can be modified in the editor
    // In this way the variables are easier to tune
    public void CmdlevelUpB(int level)
    {
        LevelUpStats Upstats = GetComponent<LevelUpStats>();

        if (level > 1)
        {
            stats.A1Cooldown -= Upstats.cooldown;
            stats.maxHealth += Upstats.maxHealth;
            stats.healthRegeneration += Upstats.healthRegeneration;
            stats.defense += Upstats.defense;
            stats.perforation += Upstats.perforation;
            stats.attackStrength += (int)Upstats.attackStrength;
            stats.attackSpeed -= Upstats.attackSpeed;

            if (level > 2)
            {
                stats.A2Cooldown -= Upstats.cooldown;
                circularAttackMultiplier += Upstats.circularAttackMultiplier;

                if (level > 3)
                {
                    stats.A3Cooldown -= Upstats.cooldown;
                    stunDelayAbility += Upstats.stunDelayAbility;

                    if (level > 4)
                    {
                        stats.A4Cooldown -= Upstats.cooldown;
                        burnAbilityDelay += Upstats.burnAbilityDelay;
                    }
                }
            }
        }
    }
}

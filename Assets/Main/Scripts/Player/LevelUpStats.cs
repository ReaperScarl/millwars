﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpStats : MonoBehaviour {

	public float cooldown = 1f;
    public float maxHealth = 15;
    public float healthRegeneration = 0.2f;
    public float defense = 0.2f;
    public float perforation = 0.2f;
    public float attackStrength = 2;
    public float attackSpeed = 0.2f;

    [Header("Bun bun")]
    public float mineAttackMultiplier = 0.2f;

    [Header("Boxer")]
    public float circularAttackMultiplier = 0.1f;
    public float stunDelayAbility = 0.5f;
    public float burnAbilityDelay = 0.5f;

}

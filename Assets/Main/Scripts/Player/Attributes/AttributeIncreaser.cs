﻿using System;

//delegate that will increment the attributes
public delegate void Increaser(PlayerStats stats);

//class that contains the delegate, his name and description
public class AttributeIncreaser{
	//delegate function
	public Increaser upgrade;
	//name of the upgrade
	public string name;
	//upgrade description
	public string description;

	public AttributeIncreaser(Increaser upgrade, string name, string description){
		this.upgrade = upgrade;
		this.name = name;
		this.description = description;
	}
}
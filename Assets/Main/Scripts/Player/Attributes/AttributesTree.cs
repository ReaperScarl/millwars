﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class for the attributes tree
public class AttributesTree{
	//left son of the tree
	public AttributesTree left;
	//right son of the tree
	public AttributesTree right;
	//property with delegate for upgrade, his name and description
	public AttributeIncreaser upgrader;

	public AttributesTree(AttributeIncreaser upgrader, AttributesTree left, AttributesTree right){
		this.upgrader = upgrader;
		this.left = left;
		this.right = right;
	}
}

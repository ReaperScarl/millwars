﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class with all values and functions to increase attributes of players
public static class UpgradeManager {
	
	//attributes for delta increment for attributes
	public static int maxHealthDelta = 20;
	public static float healthRegenerationDelta = 0.5f;
	public static float defenseDelta = 0.2f;
	public static float movementSpeedDelta = 0.5f;
	public static float perforationDelta = 0.5f;
	public static int attackStrengthDelta = 2;
	public static float attackSpeedDelta = 0.5f;
	public static float abilityCooldownDelta = 1f;


	//methods to decrement macro areas of ability
	public static void incrementConstitution(PlayerStats stats){
		incrementHealth (stats);
		incrementHealthRegeneration (stats);
	}

	public static void incrementArmor(PlayerStats stats){
		incrementDefense (stats);
		incrementMovementSpeed (stats);
	}

	public static void incrementAttack(PlayerStats stats){
		incrementPerforation (stats);
		incrementAttackStrength (stats);
	}

	public static void incrementDexterity(PlayerStats stats){
		incrementAttackSpeed (stats);
		decrementAbilityCooldown (stats);
	}


	//methods to increment specific attributes
	public static void incrementHealth(PlayerStats stats){
		stats.maxHealth += maxHealthDelta;
	}

	public static void incrementHealthRegeneration(PlayerStats stats){
		stats.healthRegeneration += healthRegenerationDelta;
	}

	public static void incrementDefense(PlayerStats stats){
		stats.defense += defenseDelta;
	}

	public static void incrementMovementSpeed(PlayerStats stats){
		stats.movementSpeed += movementSpeedDelta;
	}

	public static void incrementPerforation(PlayerStats stats){
		stats.perforation += perforationDelta;
	}

	public static void incrementAttackStrength(PlayerStats stats){
		stats.attackStrength += attackStrengthDelta;
	}

	public static void incrementAttackSpeed(PlayerStats stats){
		stats.attackSpeed += attackSpeedDelta;
	}

	public static void decrementAbilityCooldown(PlayerStats stats){
		correctCooldown (ref stats.A1Cooldown);
		correctCooldown (ref stats.A2Cooldown);
		correctCooldown (ref stats.A3Cooldown);
		correctCooldown (ref stats.A4Cooldown);
	}
	private static void correctCooldown(ref float value){
		value -= abilityCooldownDelta;
		if (value < 0)
			value = 0;
	}
}

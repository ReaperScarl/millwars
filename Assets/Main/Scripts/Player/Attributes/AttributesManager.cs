﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class that creates and memorize the two attribute upgrades tree
public class AttributesManager{
	//attributes trees
	private static AttributesTree range_tree = null;
	private static AttributesTree melee_tree = null;

	//attributes increaser (macro)
	private static AttributeIncreaser constitution = new AttributeIncreaser(
		UpgradeManager.incrementConstitution,
		"Constitution",
		"Upgrade max health and health regeneration.");

	private static AttributeIncreaser armor = new AttributeIncreaser(
		UpgradeManager.incrementArmor,
		"Armor",
		"Upgrade defense and movement speed.");
	
	private static AttributeIncreaser attack = new AttributeIncreaser(
		UpgradeManager.incrementAttack,
		"Attack",
		"Upgrade perforation and attack strength.");

	private static AttributeIncreaser dexterity = new AttributeIncreaser(
		UpgradeManager.incrementDexterity,
		"Dexterity",
		"Upgrade attack speed and ability cooldown.");

	//attribute increaser (micro)
	private static AttributeIncreaser health = new AttributeIncreaser(
		UpgradeManager.incrementHealth,
		"Health",
		"Upgrade max health.");

	private static AttributeIncreaser healthRegeneration = new AttributeIncreaser(
		UpgradeManager.incrementHealthRegeneration,
		"Health Regeneration",
		"Upgrade health regeneration.");

	private static AttributeIncreaser defense = new AttributeIncreaser(
		UpgradeManager.incrementDefense,
		"Defense",
		"Upgrade defense.");

	private static AttributeIncreaser movementSpeed = new AttributeIncreaser(
		UpgradeManager.incrementMovementSpeed,
		"Movement Speed",
		"Upgrade movement speed.");

	private static AttributeIncreaser perforation = new AttributeIncreaser(
		UpgradeManager.incrementPerforation,
		"Perforation",
		"Upgrade perforation.");

	private static AttributeIncreaser attackStrength = new AttributeIncreaser(
		UpgradeManager.incrementAttackStrength,
		"Attack Strength",
		"Upgrade attack strength.");

	private static AttributeIncreaser attackSpeed = new AttributeIncreaser(
		UpgradeManager.incrementAttackSpeed,
		"Attack Speed",
		"Upgrade attack speed.");

	private static AttributeIncreaser abilityCooldown = new AttributeIncreaser(
		UpgradeManager.decrementAbilityCooldown,
		"Ability Cooldown",
		"Upgrade ability cooldown.");

		
	public static AttributesTree getRangeTree(){
		if (range_tree != null)
			return range_tree;
		//construct the tree
	//left branch
		//5th level
		AttributesTree health_reg = new AttributesTree(healthRegeneration, null, null);
		AttributesTree max_health = new AttributesTree(health, null, null);
		AttributesTree def = new AttributesTree(defense, null, null);
		AttributesTree mov_sp = new AttributesTree(movementSpeed, null, null);

		//4th level
		AttributesTree perf_left = new AttributesTree(perforation, health_reg, max_health);
		AttributesTree perf_right = new AttributesTree(perforation, def, mov_sp);
		AttributesTree att_strength_left = new AttributesTree(attackStrength, health_reg, max_health);
		AttributesTree att_strength_right = new AttributesTree(attackStrength, def, mov_sp);

		//3rd level
		AttributesTree constit_left = new AttributesTree(constitution, perf_left, att_strength_left);
		AttributesTree arm_left = new AttributesTree(armor, perf_right, att_strength_right);

		//2nd level
		AttributesTree att = new AttributesTree(attack, constit_left, arm_left);

	//right branch
		//5th level is already done

		//4th level
		AttributesTree att_speed_left = new AttributesTree(attackSpeed, health_reg, max_health);
		AttributesTree att_speed_right = new AttributesTree(attackSpeed, def, mov_sp);
		AttributesTree ab_cool_left = new AttributesTree(abilityCooldown, health_reg, max_health);
		AttributesTree ab_cool_right = new AttributesTree(abilityCooldown, def, mov_sp);
		
		//3rd level
		AttributesTree constit_right = new AttributesTree(constitution, att_speed_left, ab_cool_left);
		AttributesTree arm_right = new AttributesTree(armor, att_speed_right, ab_cool_right);

		//2nd level
		AttributesTree dext = new AttributesTree(dexterity, constit_right, arm_right);


		//root
		range_tree = new AttributesTree(null, att, dext);
		return range_tree;
	}

	public static AttributesTree getMeleeTree(){
		if(melee_tree != null)
			return melee_tree;
		//construct the tree
	//left branch
		//5th level
		AttributesTree perf = new AttributesTree(perforation, null, null);
		AttributesTree att_strength = new AttributesTree(attackStrength, null, null);
		AttributesTree att_speed = new AttributesTree(attackSpeed, null, null);
		AttributesTree ab_cool = new AttributesTree(abilityCooldown, null, null);

		//4th level
		AttributesTree health_reg_left = new AttributesTree(healthRegeneration, perf, att_strength);
		AttributesTree health_reg_right = new AttributesTree(healthRegeneration, att_speed, ab_cool);
		AttributesTree max_health_left = new AttributesTree(health, perf, att_strength);
		AttributesTree max_health_right = new AttributesTree(health, att_speed, ab_cool);

		//3rd level
		AttributesTree att_left = new AttributesTree(attack, health_reg_left, max_health_left);
		AttributesTree dext_left = new AttributesTree(dexterity, health_reg_right, max_health_right);

		//2nd level
		AttributesTree constit = new AttributesTree(constitution, att_left, dext_left);

	//right branch
		//5th level is already done

		//4th level
		AttributesTree def_left = new AttributesTree(defense, perf, att_strength);
		AttributesTree def_right = new AttributesTree(defense, att_speed, ab_cool);
		AttributesTree mov_sp_left = new AttributesTree(movementSpeed, perf, att_strength);
		AttributesTree mov_sp_right = new AttributesTree(movementSpeed, att_speed, ab_cool);
		
		//3rd level
		AttributesTree att_right = new AttributesTree(attack, def_left, mov_sp_left);
		AttributesTree dext_right = new AttributesTree(dexterity, def_right, mov_sp_right);

		//2nd level
		AttributesTree arm = new AttributesTree(armor, att_right, dext_right);

		//root
		melee_tree = new AttributesTree(null, constit, arm);
		return melee_tree;
	}
}
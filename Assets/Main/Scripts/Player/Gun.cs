﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Gun : NetworkBehaviour {

	// hole where the bullet start to shoot
	public Transform[] projectileSpawn = new Transform[3];
	// the projectile to shoot
	public PlayerBullet projectile;
	// smoke projectile
	public Granade smokeProjectile;
	// landmine
	public Landmine landmine;
	// smoke projectile
	public RenderBallisticPath trajectory;
	// camera for center
	public Camera playerCamera;
	// target aim
	public Transform aim;
	// bomb bullet
	public BombBullet Bomb;
    //used only by IA
    [HideInInspector]
    [SyncVar]
    public Transform enemyPosition;

	// player reference
	private PlayerStats player;
	// bullet reference
	private PlayerBullet newProjectile;
	// smokebomb reference
	private Granade newSmokeProjectile;
	// new landmine reference
	private Landmine newLandmine;
	// new bomb reference
	private BombBullet newBombBullet;

	void Awake() {
		player = GetComponent<PlayerStats>();
	}

	[Command]
	public void CmdShoot(int attackNumber, params float[] delayOrDamage) {

		// set velocity of the bullet
		RaycastHit hit;
		Rigidbody bulletRigidBody;
		switch (attackNumber) {
			// base attack
		case 1:
				// create projectile
			newProjectile = Instantiate (projectile, projectileSpawn [0].transform.position, projectileSpawn [0].transform.rotation);
			bulletRigidBody = newProjectile.transform.GetComponent<Rigidbody>();
			// set the bullet statistics
			if (Physics.Raycast (playerCamera.transform.position, playerCamera.transform.forward * player.attackRange, out hit, Mathf.Infinity)) {
				newProjectile.SpawnBullet(player.attackStrength, player.transform, player.tag, player.attackRange, projectileSpawn[0].position);
				bulletRigidBody.velocity = (hit.point - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity;
			}
			else {
				newProjectile.SpawnBullet(player.attackStrength, player.transform, player.tag, player.attackRange, projectileSpawn[0].position);
				bulletRigidBody.velocity = (aim.position - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity;
			}
			NetworkServer.Spawn (newProjectile.gameObject);
			break;
			// first ability
			case 2:
				for (int i = 0; i < projectileSpawn.Length; i++) {
					newProjectile = Instantiate (projectile, projectileSpawn [i].transform.position, projectileSpawn [i].transform.rotation);
					bulletRigidBody = newProjectile.transform.GetComponent<Rigidbody>();
					if (Physics.Raycast (playerCamera.transform.position, playerCamera.transform.forward * player.attackRange, out hit, Mathf.Infinity)) {
						newProjectile.SpawnBullet(player.attackStrength, player.transform, player.tag, player.attackRange, projectileSpawn[i].position);
					bulletRigidBody.velocity = ((hit.point - projectileSpawn[i].transform.position).normalized + (projectileSpawn[i].rotation * Vector3.forward).normalized + (playerCamera.transform.rotation * Vector3.forward).normalized) * player.muzzleVelocity;
					}
					else {
						newProjectile.SpawnBullet(player.attackStrength, player.transform, player.tag, player.attackRange, projectileSpawn[i].position);
					bulletRigidBody.velocity = ((aim.position - projectileSpawn[i].transform.position).normalized + (projectileSpawn[i].rotation * Vector3.forward).normalized + (playerCamera.transform.rotation * Vector3.forward).normalized) * player.muzzleVelocity;
					}
					NetworkServer.Spawn (newProjectile.gameObject);
				}
				break;

			// second ability
			case 3:
				newLandmine = Instantiate(landmine, trajectory.gameObject.transform.position, trajectory.gameObject.transform.rotation);
				newLandmine.SetLandmine((int)delayOrDamage[0], player.align, player.transform);
				NetworkServer.Spawn (newLandmine.gameObject);
				break;
			// third ability
			case 4:
				newSmokeProjectile = Instantiate(smokeProjectile, trajectory.gameObject.transform.position, trajectory.gameObject.transform.rotation);
				NetworkServer.Spawn (newSmokeProjectile.gameObject);
				break;
			// fourth ability
			case 5:
				newBombBullet = Instantiate (Bomb, projectileSpawn [0].transform.position, projectileSpawn [0].transform.rotation);
				bulletRigidBody = newBombBullet.transform.GetComponent<Rigidbody>();
				// set the bullet statistics
				if (Physics.Raycast (playerCamera.transform.position, playerCamera.transform.forward * player.attackRange, out hit, Mathf.Infinity)) {
					bulletRigidBody.velocity = (hit.point - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity;
					newBombBullet.SetBomb(delayOrDamage[0], player.transform, player.align, player.attackRange, projectileSpawn[0].position);
				}
				else {
					bulletRigidBody.velocity = (aim.position - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity / 10f;
					newBombBullet.SetBomb(delayOrDamage[0], player.transform, player.align, player.attackRange, projectileSpawn[0].position);
				}
				NetworkServer.Spawn (newBombBullet.gameObject);
				break;
			default:
				break;
		}
	}

	[Command]
	public void CmdShootIA(int attackNumber, params float[] delayOrDamage) {

		// set velocity of the bullet
		Rigidbody bulletRigidBody;
		switch (attackNumber) {
			// base attack
			case 1:
				// create projectile
				newProjectile = Instantiate (projectile, projectileSpawn [0].transform.position, projectileSpawn [0].transform.rotation);
				bulletRigidBody = newProjectile.transform.GetComponent<Rigidbody>();
				newProjectile.SpawnBullet(player.attackStrength, player.transform, player.align, player.attackRange, projectileSpawn[0].position);
				bulletRigidBody.velocity = (enemyPosition.position - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity;
				NetworkServer.Spawn (newProjectile.gameObject);
				break;
			// first ability
			case 2:
			// throw projectile in three direction at same time
				for (int i = 0; i < projectileSpawn.Length; i++) {
					newProjectile = Instantiate (projectile, projectileSpawn [i].transform.position, projectileSpawn [i].transform.rotation);
					bulletRigidBody = newProjectile.transform.GetComponent<Rigidbody>();
					newProjectile.SpawnBullet(player.attackStrength, player.transform, player.tag, player.attackRange, projectileSpawn[i].position);
					bulletRigidBody.velocity = ((enemyPosition.position - projectileSpawn[i].transform.position).normalized + (projectileSpawn[i].rotation * Vector3.forward).normalized + (playerCamera.transform.rotation * Vector3.forward).normalized) * player.muzzleVelocity;
					NetworkServer.Spawn (newProjectile.gameObject);
				}
				break;
			// second ability
		case 3:
				newLandmine = Instantiate (landmine, trajectory.gameObject.transform.position, trajectory.gameObject.transform.rotation);
				newLandmine.SetLandmine ((int)delayOrDamage [0], player.align, player.transform);
				NetworkServer.Spawn (newLandmine.gameObject);
				break;
			// third ability
			case 4:
				newSmokeProjectile = Instantiate(smokeProjectile, trajectory.gameObject.transform.position, trajectory.gameObject.transform.rotation);
				NetworkServer.Spawn (newSmokeProjectile.gameObject);
				break;
			// fourth ability
			case 5:
				newBombBullet = Instantiate (Bomb, projectileSpawn [0].transform.position, projectileSpawn [0].transform.rotation);
				bulletRigidBody = newBombBullet.transform.GetComponent<Rigidbody>();
				// set the bullet statistics
				bulletRigidBody.velocity = (enemyPosition.position - projectileSpawn[0].transform.position).normalized * player.muzzleVelocity;
				newBombBullet.SetBomb(delayOrDamage[0], player.transform, player.align, player.attackRange, projectileSpawn[0].position);
				NetworkServer.Spawn (newBombBullet.gameObject);
				break;
			default:
				break;
		}
	}
}

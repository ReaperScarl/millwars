﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Weapon : NetworkBehaviour {

    public Transform weapon;
    public string[] animationNames;
    private Animator attackAnimator;

    private BoxCollider boxCollider;

    // reference to animation clip 
    private AnimationClip[] animationArray;
    private DamageReceiver damageReciver;
    // control if charge ability is active
    public bool chargeAbilityActive;

    // total damage inflict by attack
	[SyncVar(hook ="OnDamageChanged")]
    private int totalDamage;
    // counter of the attack did
	[SyncVar]
    private int attackCounter;

    private bool isStunning = false;

	public override void OnStartLocalPlayer(){
		GetComponent<NetworkAnimator> ().SetParameterAutoSend (0, true);
	}

	public override void PreStartClient(){
		GetComponent<NetworkAnimator> ().SetParameterAutoSend (0, true);
	}

	private void OnDamageChanged(int newD){
		if (totalDamage != newD && isLocalPlayer) {
			CmdUpdateDamage (newD);
		}
		totalDamage = newD;
	}

	[Command]
	private void CmdUpdateDamage(int newD){
		totalDamage = newD;
	}

    void Start() {
        attackAnimator = GetComponent<Animator>();
        boxCollider = weapon.GetComponent<BoxCollider>();
        damageReciver = GetComponent<DamageReceiver>();
		animationArray = new AnimationClip[attackAnimator.runtimeAnimatorController.animationClips.Length -1];
        // set each animation with its ability name
        for (int i = 0; i < animationArray.Length; i++) {
            animationArray[i] = GetAnimationClip(animationNames[i]);
        }
		CmdChangeCollider (false);
    }

	void OnColliderChange(bool newVal){
        //Debug.Log("changed collider: " + newVal);
        //Debug.Log("is client: " + this.isClient);
        //Debug.Log("is server: " + this.isServer);
        boxCollider.enabled = newVal;

	}

	[Command]
	void CmdChangeCollider(bool val){
        
        //Debug.Log("Cmd collider: " + val);
		OnColliderChange(val);
	}

    // it can take optional params 
	public void Attack(int attackNumber, float delay = 0, bool bypass = false) {
        if(!bypass)
            if (!isLocalPlayer){
                return;
            }
		if (attackAnimator != null) {
            if (attackNumber != 4 && attackNumber != 5) {
                if(attackNumber == 2) { 

                    // if is a charge animation we disable the box collider at the end of the animation
                    StartCoroutine(AttackCircularCoroutine(attackNumber));
                }
                else {
                    
                    // if is a charge animation we disable the box collider at the half of the animation
                    StartCoroutine(AttackCoroutine(attackNumber));
                }

                switch (attackNumber) // i can't tell the difference between a 1 and a 3... be carefull for 4 and 5.
                {
                    case 1:
                        isStunning = false;
						CmdAttackIncrement();
                        break;
                    case 3:
                        isStunning = true;
						CmdAttackIncrement();
                        break;

                    default:
                        isStunning = false;
						CmdAttackIncrement();
                        break;
                }
            }
            else {
                StartCoroutine(AttackDelay(attackNumber, delay));
            }
        }
    }

	[Command]
	void CmdAttackIncrement(){
		attackCounter++;
	}

    IEnumerator AttackCoroutine(int attackNumberAnimation) {
  
        CheckChargeAbilityActive(attackNumberAnimation);
        // active box collider
		CmdChangeCollider (true);
        // start aniamtion
        attackAnimator.SetInteger("State", attackNumberAnimation);
        // wait end of animation
        yield return new WaitForSeconds(animationArray[attackNumberAnimation-1].length/2);
        // wait end of animation
        yield return new WaitForSeconds(1f);
		// hide box collider
		CmdChangeCollider (false);
        attackAnimator.SetInteger("State", 0);
        CheckChargeAbilityActive(attackNumberAnimation);
    }

    IEnumerator AttackCircularCoroutine(int attackNumberAnimation) {
  
        CheckChargeAbilityActive(attackNumberAnimation);
        // active box collider
		CmdChangeCollider (true);
        // start aniamtion
        attackAnimator.SetInteger("State", attackNumberAnimation);
        // wait end of animation
        yield return new WaitForSeconds(animationArray[attackNumberAnimation-1].length);
        // hide box collider
		CmdChangeCollider (false);
        attackAnimator.SetInteger("State", 0);
        CheckChargeAbilityActive(attackNumberAnimation);
    }

    IEnumerator AttackDelay(int attackNumber, float delay) {
        // if it is invincible ability you not recive damage
        if (attackNumber == 5) {
            damageReciver.active = false;
            yield return new WaitForSeconds(delay);
            damageReciver.active = true;
        }
        else {
            yield return new WaitForSeconds(delay);
        }
    }

    public AnimationClip GetAnimationClip(string name) {
     if (!attackAnimator) return null; // no animator
     foreach (AnimationClip clip in attackAnimator.runtimeAnimatorController.animationClips) {
         if (clip.name == name) {
             return clip;
         }
     }
     return null;
    }
    
	[Command]
    public void CmdTotDamage(int damage) {
        totalDamage = damage;
    }

    // get the damage of the weapon
    public int DamageWeapon() {
		return totalDamage;
	}

    // attack counter
    public int AttackCounter() {
        return attackCounter;
    }

    // check if charge ability is activate before starting animation
    void CheckChargeAbilityActive(int attackNumber) {
        if (attackNumber == 3 && !chargeAbilityActive) {
            chargeAbilityActive = true;
        }
        else {
            chargeAbilityActive = false;
        }
    }

    public DamageInfo GetDamageInfo()
    {
        PlayerStats stats= GetComponent<PlayerStats>();
        DamageInfo damage;
        if (isStunning)
            damage= new DamageInfo(stats.playerName, AttackCounter(), DamageWeapon(), isStunning);
        else
            damage = new DamageInfo(stats.playerName, AttackCounter(), DamageWeapon());
        return damage;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//To Complete
public class PlayerBullet : NetworkBehaviour {

    private float damage;
    private Transform hitter;
    private string mastertag;
    private float maxDistance;
    private Vector3 spawnPosition;

	void Update () {
        // destroy the object if the distance from the spawn point is far
		if(!base.isServer) return;

        if (Vector3.Distance(spawnPosition, transform.position) > maxDistance -0.5f) {
            Destroy(gameObject);
        }
	}

    //Has to be called When the Bullet is istantiated
    public void SpawnBullet(float _damage, Transform _hitter, string _mastertag, float _maxDistance, Vector3 _spawnPosition) {
        damage = _damage;
        hitter = _hitter;
        mastertag = _mastertag;
        maxDistance = _maxDistance;
        spawnPosition = _spawnPosition;
    }

    public int Damage() {
        return (int)damage;
    }

    public Transform Hitter() {
        return hitter;
    }

    public string GetTag() {
        return mastertag;
    }
}

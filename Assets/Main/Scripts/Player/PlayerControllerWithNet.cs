﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerControllerWithNet : NetworkBehaviour {

    public float rotationSensitivity;
    public float gravity;
    [SyncVar]
    public Transform myCamera;

    public BarModifierManager little_lifebar;

    public Camera mainCamera;
    public RespawnCanvasController respawnCanvas;
    public AbilityManager playerCanvas;
    [HideInInspector]
    public bool isStun = false;

    public GameObject spawner;

    [HideInInspector]
    public  PlayerStats stats;
    private CharacterController controller;
    private Vector3 moveDir = Vector3.zero;

    private RaycastHit hit;
    private Vector3 surfaceNormal;
    private Vector3 forwardRelativeToSurfaceNormal;
    private BarModifierManager healthbar;

    private int currentMinionindex = 0;

    public GameObject leftUpgrader;
    public GameObject rightUpgrader;

    protected float nextTimeBaseAttack;
    protected float nextTimeFirstAbility;
    protected float nextTimeSecondAbility;
    protected float nextTimeThirdAbility;
    protected float nextTimeFourthAbility;

    public int secondsToRespawn = 5;

	// set or get variable to know if player is death or alive
    [HideInInspector]
	public bool isAlive;

	//for score manager
	[SyncVar]
	public bool alreadyDead;

    //index of spawnPoint
    [SyncVar]
    public int spawnIndex;

    private Transform lasthitter;

    protected virtual void Start() {
		isAlive = true;
		stats = GetComponent<PlayerStats> ();
		//set tag of the player
		//TODO: to remove when AI will be ready.
        if(stats.align == "")
		    stats.align = "blue";
        
        gameObject.tag = stats.align;
            
		controller = GetComponent<CharacterController> ();
        //Debug.Log("inside start before align call");
        stats.OnChangedAlignment(stats.align);
        mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
        // cursor is lock at the center of the screen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        // le abilità vanno riviste ovviamente questa è una toppa
        nextTimeBaseAttack = Time.time - 60f;
        nextTimeFirstAbility = Time.time - 60f;
        nextTimeSecondAbility = Time.time - 60f;
        nextTimeThirdAbility = Time.time - 60f;
        nextTimeFourthAbility = Time.time - 60f;
        //coroutine for health regeneration
        InvokeRepeating("CmdRegenerateHealth", 0f, 1f);
        //disabilito la telecamera principale
        mainCamera.enabled = false;
        mainCamera.GetComponent<AudioListener>().enabled = false;

        //setup of local camera
        if (myCamera == null)
            myCamera = GetComponentInChildren<Camera>().transform;

        if (!isLocalPlayer) {
            //disable camera
            myCamera.GetComponent<Camera>().enabled = false;
            myCamera.GetComponent<AudioListener>().enabled = false;
            //enable lifebar
            healthbar = little_lifebar;
            healthbar.transform.parent.parent.gameObject.SetActive(true);
            healthbar.SetConversionPToB(stats.maxHealth);
            GetComponent<DamageReceiver>().hmod = healthbar;
        } else {
            // get script of bar of health and setup it
			healthbar = GameObject.Find("HealthPlayer").transform.GetChild(0).GetComponent<BarModifierManager>();
            if (GetComponentInChildren<DamageReceiver>().healthbar == null)
            {
                GetComponentInChildren<DamageReceiver>().healthbar = healthbar.transform.GetComponent<Image>();
                GetComponentInChildren<DamageReceiver>().SetBar();
                healthbar.transform.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            }
            //create player canvas
            playerCanvas = Instantiate(playerCanvas);
            //initialize player canvas
            playerCanvas.initAbilities();

        }
        //set the correct spawn position
        SetSpawnPoint();
        //Debug.Log("tag: "+tag+"; index: "+spawnIndex+"spawn:"+ transform.position);

    }

    // Update is called more than once per frame
    protected virtual void Update()
    {
        if (!isLocalPlayer)
            return;

        //player movement if not stub by ability
        if (!isStun)
        {
            MovePlayer();
        }
        //player rotation over the map
        CharacterFaceRelativeToSurface();
        //player rotation (camera)
        MoveCamera();
    }

    void MovePlayer() {
        moveDir = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"), 0, CrossPlatformInputManager.GetAxis("Vertical"));
        moveDir = transform.TransformDirection(moveDir);
        moveDir *= stats.movementSpeed;
        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * Time.deltaTime);
    }

    void MoveCamera() {
       transform.Rotate(new Vector3(0, CrossPlatformInputManager.GetAxis("Mouse X"), 0) * Time.deltaTime * rotationSensitivity);
    }

    void CharacterFaceRelativeToSurface() {
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 10)) {
            surfaceNormal = hit.normal;
            forwardRelativeToSurfaceNormal = Vector3.Cross(transform.right, surfaceNormal);
            Quaternion targetRotation = Quaternion.LookRotation(forwardRelativeToSurfaceNormal);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 2);
        }
    }
	[Command]
    void CmdRegenerateHealth() {
        // if I have max health don't do nothing
        if (stats.HP < stats.maxHealth) {
			//Debug.Log ("rigenero vita local player");
            stats.HP += stats.healthRegeneration;
            if (stats.HP > stats.maxHealth) {
                stats.HP = stats.maxHealth;
            }
        }
    }

    public void Die() {
        // if health is under 0 kill player
        if (isAlive && stats.HP <= 0) {
            //destroy the player
            RpcDestroyPlayer();
            GetComponent<Hide_ShowBrushesController>().FixTheBushesWhileDead();
        }
    }

    public void changeCameraState(bool value)
    {
        //enable the main camera
        mainCamera.enabled = !value;
        mainCamera.GetComponent<AudioListener>().enabled = !value;
        //hide bars
        healthbar.GetComponent<Image>().enabled = value;
        GetComponent<ExperienceManager>().expBar.GetComponent<Image>().enabled = value;
    }

    [ClientRpc]
    public virtual void RpcDestroyPlayer() {
        //set player dead
        isAlive = false;
        //disable the gameObject
        gameObject.SetActive(false);
        //put the game object out of the map to not have problems with environment
        transform.position = new Vector3(0f, -20f, 0);
        //prepare the respawn canvas if is the local player
        if (isLocalPlayer) {
            RespawnCanvasController canv = Instantiate(respawnCanvas);
            canv.SetCountdown(secondsToRespawn, this);
            changeCameraState(false);
        }
    }

    [Command]
    public void CmdRespawn()
    {
		alreadyDead = false;
        RpcRespawnPlayer();
    }

    [ClientRpc]
	public virtual void RpcRespawnPlayer(){
		//set player alive
		isAlive = true;
		//enable the gameObject
		this.gameObject.SetActive(true);
        stats.HP = stats.maxHealth;
        if (isLocalPlayer)
        {
            changeCameraState(true);
            //prepare the HP
            healthbar.SetConversionPToB(stats.maxHealth);
            healthbar.ChangeTheBar(-stats.HP);
        }
        else {
            little_lifebar.SetConversionPToB(stats.maxHealth);
            little_lifebar.ChangeTheBar(-stats.HP);
        }
		//set the spawn position
		SetSpawnPoint();
        //show brushes
        transform.GetComponent<Hide_ShowBrushesController>().InizializeTheBushes();
	}

	protected void SetSpawnPoint(){
		//get all the spawnpoints
		Transform[] spawnPoints = new Transform[2];
		if (tag == "blue") {
			//spawns 0 and 1
			spawnPoints[0] = spawner.transform.GetChild(0);
			spawnPoints[1] = spawner.transform.GetChild(1);
		} else {
			//spawns 2 and 3
			spawnPoints[0] = spawner.transform.GetChild(2);
			spawnPoints[1] = spawner.transform.GetChild(3);
		}
		//choose the first spawn
		Transform spawn;
        spawn = spawnPoints[spawnIndex];
        //Debug.Log(spawn);
        //finally update the position
        transform.position = new Vector3(spawn.position.x, spawn.position.y + transform.lossyScale.y / 2, spawn.position.z);
        transform.rotation = spawn.rotation;
        myCamera.GetComponent<CameraUpDownNet>().setCameraRotation(spawn.rotation.y * 180);
	}

    //Player has kill a minion. If has killed minion module, then he gains a point.
    public void MinionKilled()
    {
        currentMinionindex++;
        if (currentMinionindex % stats.GetMinionModule()  == 0)
            GameObject.Find("ScoreManager").GetComponent<ScoreManager>().PointForMinion(this.tag, stats.playerName);
    }

	public IEnumerator Stun(float seconds) {
		isStun = true;
		yield return new WaitForSeconds(seconds);
		isStun = false;
	}


    public void CryForHelp(Transform hitter)
    {
        lasthitter = hitter;
        CmdCryForHelp();
    }

    [Command]
    public void CmdCryForHelp()
    {
        GameObject[] allies = GameObject.FindGameObjectsWithTag(this.tag);
        foreach (GameObject ally in allies)
        {
            float distanceToAlly = Vector3.Distance(transform.position, ally.transform.position);
            if (stats.helpRange >= distanceToAlly)
            {
                Debug.Log("Ally near: " + ally);
                _Stats allystats = ally.transform.GetComponentInChildren<_Stats>();
                if (allystats != null)
                {
                    switch (allystats.objectType) {
                        case _Stats.ObjectTypes.turret:
                            ally.GetComponent <TurretController>().ChangeTarget(lasthitter);
                            break;

                        case _Stats.ObjectTypes.minion:
                            ally.GetComponent<MinionBehaviour>().ChangeTarget(lasthitter);
                            break;
                    }
                }
            }       
        }
    }

    // return if player is death or alive
    public bool PlayerIsAlive() {
        return isAlive;
    }

    [Command]
    public void CmdlevelUp(int level)
    {
        if (isServer)
        {
            if (GetComponent<BunBunWithNet>())
                GetComponent<BunBunWithNet>().CmdlevelUpB(level);
            else
                GetComponent<BoxerJr>().CmdlevelUpB(level);
        }
        healthbar.SetConversionPToB(stats.maxHealth);
        healthbar.BarFilled();
        healthbar.ChangeTheBar(stats.maxHealth - stats.HP);
    }
}
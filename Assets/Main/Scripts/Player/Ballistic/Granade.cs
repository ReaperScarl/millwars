﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

public class Granade : NetworkBehaviour {

	public float velocity;
	public ParticleSystem smogParticle;
	private Rigidbody myRigidbody;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody>();
		myRigidbody.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
	}

	void OnTriggerEnter(Collider other) {
		ParticleSystem obj = Instantiate (smogParticle, transform.position, Quaternion.identity);
		NetworkServer.Spawn (obj.gameObject);
		Destroy(gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Landmine : NetworkBehaviour {

	public float velocity;
	public LayerMask layerMask;
	public ParticleSystem landmineExplosion;
	public float landmineTimeOfExplosion;

	private Rigidbody myRigidbody;
	private List<GameObject> collisionObject = new List<GameObject>();
	private LandmineTrigger trigger;
	private DamageProvider dmgP;
	private int damage;

	[SyncVar]
	private string masterTag;
	private Transform hitter;

	private ParticleSystem particle;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody>();
		trigger = GetComponentInChildren<LandmineTrigger>();
		//dmgP = GetComponent<DamageProvider>();
		dmgP = new DamageProvider();
		myRigidbody.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
	}

	void OnTriggerEnter(Collider other) {
		// collision with terrain 
		if (other.gameObject.layer == Mathf.Log(layerMask.value, 2)) {
			// lock the movement
			myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
			// insert mine in terrain
			transform.position = new Vector3(transform.position.x,transform.position.y + transform.localScale.y, transform.position.z);
			//hide the landmine for enemies
			if(isServer) RpcShowHide();
		}
		// explode on contact with other
		else {
			collisionObject = trigger.GetListOfGameobject();
			//controllo di aver colpito qualcosa che non sia il player
			bool val = collisionObject.Count > 0 && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && masterTag != other.transform.parent.tag;
			//faccio or con il controllo per il player
			PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
			val = val || (collisionObject.Count > 0 && other.transform != transform && otherStats != null && otherStats.align != masterTag);
			if (val) {
				ActiveLandimine(other);
			}
		}
	}

	private void ActiveLandimine(Collider other) {
		// if I collide with more than one enemies
		for (int i = 0; i < collisionObject.Count; i++) {
			if (collisionObject == null)
				continue;
			if (collisionObject [i].transform.GetComponent<DamageReceiver> () != null) {//player
				dmgP.ProvideDamage (collisionObject [i].transform, damage, hitter);
			}
			else if (collisionObject [i].transform.parent.GetComponent<DamageReceiver> () != null) {//minion e roba varia
				dmgP.ProvideDamage (collisionObject [i].transform.parent, damage, hitter);
			} 
		}
		particle = Instantiate (landmineExplosion, transform.position, Quaternion.identity);
		CmdSpawn ();
		// destroy landmine
		NetworkServer.Destroy(gameObject);
	}

	[Command]
	void CmdSpawn(){
		NetworkServer.Spawn (particle.gameObject);
	}

	// set damage provided by landmine
	public void SetLandmine(int _damage, string _masterTag, Transform _hitter) {
		damage = _damage;
		masterTag = _masterTag;
		hitter = _hitter;
	}

	//hide landmine
	[ClientRpc]
	private void RpcShowHide(){
		//set invisible only for opponents team
		if (isClient) {
			PlayerStats []players = FindObjectsOfType<PlayerStats> ();
			foreach(PlayerStats player in players){
				if (player.transform.GetChild (1).GetComponentInChildren<Camera> ().enabled == true) {
					GetComponent<MeshRenderer> ().enabled = player.align == masterTag;
				}
			}
		}
	}

	// set tag of player
	public string GetTag() {
		return masterTag;
	}
}

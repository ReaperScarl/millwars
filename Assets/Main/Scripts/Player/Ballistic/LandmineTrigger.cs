﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandmineTrigger : MonoBehaviour {

	private List<GameObject> collisionObject = new List<GameObject>();
	private string tagPlayer;
	public LayerMask layerMask;
	private Rigidbody myRigidbody;

	void Start() {
		tagPlayer = GetComponentInParent<Landmine>().GetTag();
		myRigidbody = GetComponent<Rigidbody>();
		myRigidbody.AddForce(transform.forward * GetComponentInParent<Landmine>().velocity, ForceMode.VelocityChange);
	}

	void OnTriggerEnter(Collider other) {
		PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
		bool val = !collisionObject.Contains(other.gameObject) && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && tagPlayer != other.transform.parent.tag;
		val = val || (!collisionObject.Contains(other.gameObject) && other.transform != transform && otherStats != null && otherStats.align != tagPlayer);

		if (other.gameObject.layer == Mathf.Log(layerMask.value, 2)) {
			// lock the movement
			myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
			// set trigger
			transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + transform.parent.localScale.y, transform.parent.position.z);
			transform.rotation = Quaternion.identity;
		}
		// if not in list add element that collide with the box
		else if (val) {
			collisionObject.Add(other.gameObject);
		}
	}

	void OnTriggerExit(Collider other) {
		// if not in list add element that collide with the box
		PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
		bool val = collisionObject.Contains(other.gameObject) && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && tagPlayer != other.transform.parent.tag;
		val = val || (collisionObject.Contains(other.gameObject) && other.transform != transform && otherStats != null && otherStats.align != tagPlayer);

		if (val) {
			collisionObject.Remove(other.gameObject);
		}
	}

	public List<GameObject> GetListOfGameobject() {
		return collisionObject;
	}
}

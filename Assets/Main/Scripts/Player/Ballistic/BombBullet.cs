﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBullet : MonoBehaviour {

	public ParticleSystem bombExplosion;
	public float bombTimeOfExplosion;
	public LayerMask layerMask;

	private List<GameObject> collisionObject = new List<GameObject>();
	private BombTrigger trigger;
	private DamageProvider dmgP;
	private int damage;
    private Transform hitter;
    private string masterTag;
    private float maxDistance;
    private Vector3 spawnPosition;

	// Use this for initialization
	void Start () {
		trigger = GetComponentInChildren<BombTrigger>();
		// set bomb triggered
		trigger.SetBombTrigger(masterTag, maxDistance, spawnPosition);
		//dmgP = GetComponent<DamageProvider>();
		dmgP = new DamageProvider();
	}

	void Update() {
        // destroy the object if the distance from the spawn point is far
        if (Vector3.Distance(spawnPosition, transform.position) > maxDistance) {
            Destroy(gameObject);
        }
	}

	void OnTriggerEnter(Collider other) {
		collisionObject = trigger.GetListOfGameobject();
		bool val = collisionObject.Count > 0 && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && masterTag != other.transform.parent.tag;
		//faccio or con il controllo per il player
		PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
		val = val || (collisionObject.Count > 0 && other.transform != transform && otherStats != null && otherStats.align != masterTag);
		if (val) {
			ActiveBomb(other);
		}
		else if (collisionObject.Count == 0 && other.gameObject.layer == Mathf.Log(layerMask.value, 2)) {
			// if hit the map but there is no player in list
			Destroy(Instantiate(bombExplosion, transform.position, Quaternion.identity), bombTimeOfExplosion);
			// destroy bomb
			Destroy(gameObject);
		}
	}

	private void ActiveBomb(Collider other) {
		// if I collide with more than one enemies
		for (int i = 0; i < collisionObject.Count; i++) {
			if (collisionObject [i].transform.GetComponent<DamageReceiver> () != null) {//player
				dmgP.ProvideDamage (collisionObject [i].transform, damage, hitter);
			}
			else if (collisionObject [i].transform.parent.GetComponent<DamageReceiver> () != null) {//minion e roba varia
				dmgP.ProvideDamage (collisionObject [i].transform.parent, damage, hitter);
			} 
		}
		// if hit the map or player explodes
		Destroy(Instantiate(bombExplosion, transform.position, Quaternion.identity), bombTimeOfExplosion);
		// destroy bomb
		Destroy(gameObject);
	}

	//Has to be called When the Bullet is istantiated
    public void SetBomb(float _damage, Transform _hitter, string _mastertag, float _maxDistance, Vector3 _spawnPosition) {
        damage = (int)_damage;
        hitter = _hitter;
        masterTag = _mastertag;
        maxDistance = _maxDistance;
        spawnPosition = _spawnPosition;
    }
}

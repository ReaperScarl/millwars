﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombTrigger : MonoBehaviour {

	private List<GameObject> collisionObject = new List<GameObject>();
	private string tagPlayer;
    private float maxDistance;
    private Vector3 spawnPosition;
    private Vector3 cameraPosition;

	void Update() {
        // destroy the object if the distance from the spawn point is far
        if (Vector3.Distance(spawnPosition, transform.position) > maxDistance -0.5f) {
			Debug.Log(Vector3.Distance(spawnPosition, transform.position));
            Destroy(gameObject);
        }
	}

	void OnTriggerEnter(Collider other) {
		// if not in list add element that collide with the box
		PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
		bool val = !collisionObject.Contains(other.gameObject) && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && tagPlayer != other.transform.parent.tag;
		val = val || (!collisionObject.Contains(other.gameObject) && other.transform != transform && otherStats != null && otherStats.align != tagPlayer);
		//Debug.Log ("onTriggerBomb: other.transform" + other.transform + "and val = " + val);
		if (val) {
			collisionObject.Add(other.gameObject);
		}
	}

	void OnTriggerExit(Collider other) {
		// if not in list add element that collide with the box
		PlayerStats otherStats = other.transform.GetComponent<PlayerStats>();
		bool val = collisionObject.Contains(other.gameObject) && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && tagPlayer != other.transform.parent.tag;
		val = val || (collisionObject.Contains(other.gameObject) && other.transform != transform && otherStats != null && otherStats.align != tagPlayer);

		if (collisionObject.Contains(other.gameObject) && other.transform.parent != null && (other.transform.parent.tag == "blue" || other.transform.parent.tag == "red" || other.transform.parent.tag == "crazyminion"|| other.transform.parent.tag =="boss") && other.transform.parent.tag != tagPlayer) {
			collisionObject.Remove(other.gameObject);
		}
	}

	public List<GameObject> GetListOfGameobject() {
		return collisionObject;
	}

	//Has to be called When the Bullet is istantiated
    public void SetBombTrigger(string _tagPlayer, float _maxDistance, Vector3 _spawnPosition) {
        tagPlayer = _tagPlayer;
        maxDistance = _maxDistance;
        spawnPosition = _spawnPosition;
    }
}

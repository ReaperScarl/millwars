﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderBallisticPath : MonoBehaviour {

	public float initialVelocity;
	public float timeResolution;
	public float maxTime;
	public LayerMask layerMask;
	public GameObject explosionPreDisplay;

	private GameObject explosionPreInstance;
	private LineRenderer lineRenderer;
	private Vector3 hitPoint;

	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 velocityVector = transform.forward * initialVelocity;
		lineRenderer.positionCount = ((int)(maxTime/timeResolution));

		int index = 0;
		Vector3 currentPosition = transform.position;
		for (float i = 0.0f; i < maxTime; i += timeResolution) {
			lineRenderer.SetPosition(index, currentPosition);
			RaycastHit hit;
			if(Physics.Raycast(currentPosition, velocityVector, out hit, velocityVector.magnitude * timeResolution, layerMask)) {
				hitPoint = hit.point;
				lineRenderer.positionCount = index +2;
				lineRenderer.SetPosition(index+1, hitPoint);
				if (explosionPreDisplay != null) { 
					if(explosionPreInstance != null) {
						explosionPreInstance.SetActive(true);
						explosionPreInstance.transform.position = hitPoint;
					}
					else {
						explosionPreInstance = Instantiate(explosionPreDisplay, hitPoint, Quaternion.identity, this.transform);
						explosionPreInstance.SetActive(true);
					}
				}
				break;
			}
			else {
				if (explosionPreInstance != null) {
					explosionPreInstance.SetActive(false);
				}
			}
			currentPosition += velocityVector * timeResolution;
			velocityVector += Physics.gravity * timeResolution;
			index++;
		}
	}

	public Vector3 GetHitPoint() {
		return hitPoint;
	}
}

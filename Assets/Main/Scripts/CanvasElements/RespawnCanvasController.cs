﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RespawnCanvasController : NetworkBehaviour {

	//text where the countdown is displayed
	public Text countdown;
	//variable to know if the countdown is started
	private bool started;
    //variable to knwo if the countdown has finished
    private bool finished;
	//acutal countdown value
	private float countValue;
	//player
	private PlayerControllerWithNet player;


	// Use this for initialization
	void Awake () {
		started = false;
        finished = false;

	}
	
	// Update is called once per frame
	void Update () {
        if (started && ! finished) {
			countValue -= Time.deltaTime;
			if (countValue > 0) {
				countdown.text = ((int)countValue).ToString ();
			}else{
                //end of timer, try to respawn player
                finished = true;
                player.CmdRespawn();
                GetComponent<Canvas>().enabled = false;
                countValue = 3;
			}
        }
        else {
            countValue -= Time.deltaTime;
            if(countValue < 0)
            {
                DestroyImmediate(gameObject);
            }
        }
	}

	//setup the countdown
	public void SetCountdown(int seconds, PlayerControllerWithNet player){
		countValue = seconds;
		this.player = player;
		countdown.text = ((int)countValue).ToString();
		//let the countdown start
		started = true;
	}
}

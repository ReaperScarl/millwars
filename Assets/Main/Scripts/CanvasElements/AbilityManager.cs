﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class AbilityManager : MonoBehaviour {

	public Sprite lockSprite;

	public Sprite[] abilitiesSprite;

	public Image[] abilities;

	public Image[] abilities_fader;

	public Text[] abilities_text;


	public void unlockAbility(int level){
        Debug.Log("cambio sprite");
        Debug.Log(level);
		abilities[level - 2].sprite = abilitiesSprite [level - 2];
	}

	public void lockAbility(int i){
		abilities[i].sprite = lockSprite;
	}

	public void initAbilities(){
		for (int i = 0; i < 4; i++) {
			lockAbility (i);
		}
	}

	public bool isAbilityLocked(int i){
		return abilities [i - 1].sprite == lockSprite;
		//return false;		//for developing
	}


	public void useAbility(int i, float cooldown){
		StartCoroutine (TimerAbility (i, cooldown));
	}

	private IEnumerator TimerAbility(int i, float cooldown){
		i -= 1;
		abilities_fader [i].enabled = true;
		abilities_text [i].enabled = true;
		for (int j = (int)cooldown; j > 0; j--) {
			abilities_text [i].text = (j).ToString();
			yield return new WaitForSeconds (1);
		}
		abilities_fader [i].enabled = false;
		abilities_text [i].enabled = false;
	}

}
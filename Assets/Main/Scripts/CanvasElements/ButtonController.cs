﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Class responsible for the GUI buttons (except the ones in the lobbyplayer)
/// </summary>
public class ButtonController : MonoBehaviour {

    public InputField namefield;
    public RectTransform nameErrorText;

	public void GoToResults()
    {
        SceneManager.LoadScene("Results");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Play()
    {
        if (namefield.text != "") //If the name is
        {
            PlayerPrefs.SetString("CurrentName", namefield.text);
            SceneManager.LoadScene("MatchMenu");
        }
        else
        {
            nameErrorText.gameObject.SetActive(true);
            nameErrorText.GetComponent<DelayedDisappearScript>().Disappear();
        }   
    }

    public void BackFromMatchMenu() //If there is a NetManager, Destroy it and go to Main Menu
    {
        GameObject net = GameObject.Find("LobbyManagerCanvas");
        if (net)
            GameObject.Destroy(net);
        SceneManager.LoadScene("MainMenu");
    }

    public void QuickMatch() // Just temporary
    {
        SceneManager.LoadScene("GameWithNet");
    }

    public void BackFromResults()
    {
        GameObject net = GameObject.Find("LobbyManagerCanvas");
        if(net)
        {

            //Debug.Log("Is Host? : "+ net.GetComponent<LobbyManager>().isHost);
            if (net.GetComponent<LobbyManager>().isHost)
                net.GetComponent<LobbyManager>().StopHost();
            else
                net.GetComponent<LobbyManager>().StopClient();
        }
    }

    // to do this evening if there is time
    public void BackFromCredits()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that manages to make a conversion from hp to "bar point".
/// To put in every heath bar or there will be exceptions.
/// </summary>
public class BarModifierManager : MonoBehaviour {


    private float conversion1HPToHB;

    //Convert the max health in points for the bar
    public void SetConversionPToB(float hp){
        conversion1HPToHB = 1 / hp;
    }

    /// <summary>
    /// Subtract the amount from bar (or add, if it is negative)
    /// </summary>

    public void ChangeTheBar(float i)
    {
       this.transform.GetComponent<Image>().fillAmount -= i * conversion1HPToHB;
    }

    /// <summary>
    /// Refill completely the bar
    /// </summary>
    public void BarFilled()
    {
        this.transform.GetComponent<Image>().fillAmount =1;
    }

}

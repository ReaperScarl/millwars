﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class in results scene responsible for the show of the informations on the scene.
/// </summary>
public class ResultsManager : MonoBehaviour {

    public float timeAfterTheWinLose=0.5f;
    public float timeForEveryPlayerLine = 0.5f;
    //Thing that have to be set off and on when run this scene.
    public RectTransform MatchType;
    public RectTransform WinText;
    public RectTransform Players;
    public GameObject GoBackButton;

    private GameObject NetManager;
    private ScoreInfo scoreInfo;
    public bool iWon=false;

	void Start () {
        NetManager = GameObject.Find("LobbyManagerCanvas");
        scoreInfo = NetManager.GetComponent<ScoreInfo>();
        StartCoroutine(ShowTheResults());
	}

	// Function responsible for the show of the infos. the info will be shown with a delay.
    IEnumerator ShowTheResults()
    {
        //Shows The title
        ShowTheWinLose();
        Time.timeScale = 1; // After the last scene, the timeScale was 0<x<0.1
        yield return new WaitForSeconds(timeAfterTheWinLose);
        //Shows The players
        for ( int i=0; i<4; i++)
        {
            ShowThePlayer(i);
            yield return new WaitForSeconds(timeForEveryPlayerLine);
        }
        GoBackButton.SetActive(true);
    }
    
    //Show the Win/Lose text and the Type of Match
    void ShowTheWinLose()
    {
        int mySide = MyTeam();
        WinText.gameObject.SetActive(true);
        switch (scoreInfo.winner)
        {
            case 0:
                WinText.GetComponent<Text>().text = "It's a draw";
                iWon = false;
                break;
            case 1:
                WinText.GetComponent<Text>().text =(mySide==1)? "You won!": "You lost!";
                iWon = (mySide == 1) ? true : false;
                break;
            case 2:
                WinText.GetComponent<Text>().text = (mySide == 2) ? "You won!" : "You lost!";
                iWon = (mySide == 2) ? true : false;
                break;
        }
        MatchType.gameObject.SetActive(true);
    }
    
    void ShowThePlayer(int index) // Self-explanatory
    {
        //Has different Values and he has to show them all:
        string ind = "" + (index+1);
        Transform player = Players.Find("Player " + ind);
        if (player)
        {
            GameObject PlayerNameObject=Players.Find("Player " + ind).gameObject;
            PlayerNameObject.SetActive(true);
            PlayerNameObject.GetComponent<Text>().text = scoreInfo.playersname[index];
            Text[] playerInfo = PlayerNameObject.GetComponentsInChildren<Text>();
            if (index <= 1)
            {
                int tot = 0;
                for (int i = 1; i < playerInfo.Length-1; i++)
                {
                    switch (i)  //The order is: min, K,D,T,B, N, Tot 
                    {
                        //Minions
                        case 1:
                            tot += scoreInfo.redScores[index, i - 1] * scoreInfo.rewards[i - 1];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, i - 1]);
                            break;
                        //Kills
                        case 2:
                            tot += scoreInfo.redScores[index, i] * scoreInfo.rewards[i];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, i]);
                            break;
                        //Deaths
                        case 3:
                            tot -= scoreInfo.redScores[index, 5] * scoreInfo.rewards[5];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, 5]);
                            break;
                        //Towers
                        case 4:
                            tot += scoreInfo.redScores[index, 1] * scoreInfo.rewards[1];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, 1]);
                            break;
                        //Bosses
                        case 5:
                            tot += scoreInfo.redScores[index, 3] * scoreInfo.rewards[3];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, 3]);
                            break;
                        //Nexus
                        case 6:
                            tot += scoreInfo.redScores[index, 4] * scoreInfo.rewards[4];
                            playerInfo[i].text = "" + (scoreInfo.redScores[index, 4]);
                            break;
                    }
                }
                playerInfo[playerInfo.Length-1].text =""+tot;
            }
            else // when there is no player registered -> it shouldn't happen
            {
                int tot = 0;
                index = index % 2;
                for (int i = 1; i < playerInfo.Length - 1; i++)
                {
                    /*
                    The order in result is: min, K,D,T,B, N, Tot
                    The order in Rewards is : min, T, K, B, N, D
                    */
                    switch (i)
                    {
                        //Minions
                        case 1:
                            tot += scoreInfo.blueScores[index, i - 1] * scoreInfo.rewards[i - 1];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, i - 1]); //1 point x3 minions
                            break;
                        //Kills
                        case 2:
                            tot += scoreInfo.blueScores[index, i] * scoreInfo.rewards[i];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, i]);
                            break;
                        //Deaths
                        case 3:
                            tot -= scoreInfo.blueScores[index, 5] * scoreInfo.rewards[5];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, 5]);
                            break;
                        //Towers
                        case 4:
                            tot += scoreInfo.blueScores[index, 1] * scoreInfo.rewards[1];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, 1]);
                            break;
                        //Bosses
                        case 5:
                            tot += scoreInfo.blueScores[index, 3] * scoreInfo.rewards[3];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, 3]);
                            break;
                        //Nexus
                        case 6:
                            tot += scoreInfo.blueScores[index, 4] * scoreInfo.rewards[4];
                            playerInfo[i].text = "" + (scoreInfo.blueScores[index, 4]);
                            break;
                    }
                }
                playerInfo[playerInfo.Length-1].text = ""+tot;
            }
        }   
    }

    //To know what team i am in
    int MyTeam()
    {
        for (int i=0; i< scoreInfo.playersname.Length; i++)
        {
            if (scoreInfo.playersname[i] == scoreInfo.myName)
            {
                if (i < 2)
                    return 1;
                else
                    return 2;
            }     
        }
        Debug.Log("You shouldn't be here!");
        return 0;
    }
}

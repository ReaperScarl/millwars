﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class made for make desappear a transform (for text, images etc) after a delay decided by the designer.
/// </summary>
public class DelayedDisappearScript : MonoBehaviour {

    public float secToDis=4f;

    /// <summary>
    ///     Called if the transf has to desappear
    /// </summary>
    public void Disappear()
    {
        StartCoroutine(WaitToDiseppear());
    }
	
    IEnumerator WaitToDiseppear()
    {
        yield return new WaitForSeconds(secToDis);
        this.gameObject.SetActive(false);
    }


}

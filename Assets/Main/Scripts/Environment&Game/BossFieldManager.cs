﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFieldManager : MonoBehaviour {

    public Transform boss;
    public int secondsBeforeCheckForPlayers=5;

    private Transform[] players=new Transform[4];
    private int numOfPlayers=0;

    private void Start()
    {
        StartCoroutine(CheckForPlayers());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!boss)
        {
            this.enabled = false;
            return;
        }

        _Stats stats = other.transform.root.GetComponentInChildren<_Stats>();
        if (stats && stats.objectType== _Stats.ObjectTypes.champion)
        {
            PlayerInTheField(other.transform.root);
            numOfPlayers++;
        }     
    }

    private void OnTriggerExit(Collider other)
    {
        if (!boss)
        {
            this.enabled = false;
            return;
        }   

        _Stats stats = other.transform.root.GetComponentInChildren<_Stats>();
        if (stats && stats.objectType == _Stats.ObjectTypes.champion)
        {  
            PlayerOutTheField(other.transform.root);
            numOfPlayers--;
            if (numOfPlayers == 0)
            {
                boss.GetComponent<BossBehaviour>().BackToBase();
            }
        }
    }

    //Check: if there is not a player with that transfom, add it to the boss
    private void PlayerInTheField(Transform t)
    {
        for (int i=0; i<players.Length; i++)
        {
            if (players[i] == t)
            {
                boss.GetComponent<BossBehaviour>().AddPlayer(t, i);
                //Debug.Log("Desappear e Reappear");
                return;
            }
            if (players[i] == null)
            {
                players[i] = t;
                boss.GetComponent<BossBehaviour>().AddPlayer(t, i);
                return;
            }
        }
        Debug.Log("You shouldn't be here");
    }

    //Check: if a player has left the field, it should be removed
    private void PlayerOutTheField(Transform t)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == t)
            {
                players[i] = null;
                boss.GetComponent<BossBehaviour>().RemovePlayer(i);
                return;
            }  
        }
        Debug.Log("You shouldn't be here");
    }

    //Search the index of a champion in players given his transform
    public int IndexOfChampion(Transform t)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == t)
                return i;
        }
        Debug.Log("You shouldn't be here " + t);
        return -1;
    }

    //Every *secondsBefore...* seconds will check if there are players that have died and not correctly removed (because they didn't exit the trigger correctly)
    private IEnumerator CheckForPlayers()
    {
        bool isSomeoneInside = false;
        while (boss)
        {
            if(!boss.GetComponent<BossBehaviour>().isSleeping)
            {
                isSomeoneInside = false;
                if (numOfPlayers != 0)
                {
                    for (int i = 0; i < players.Length; i++)
                    {
                        if (players[i])
                        {
                            if (Vector3.Distance(players[i].position, this.transform.position) < 25)
                                isSomeoneInside = true;
                            else // some player died and left the field without exit the trigger.
                            {
                                PlayerOutTheField(players[i]);
                                numOfPlayers--;
                            }
                        }
                    }
                    if (!isSomeoneInside) //there is no one still alive
                    {
                        numOfPlayers = 0;
                        boss.GetComponent<BossBehaviour>().BackToBase();
                    }
                }
            }
            yield return new WaitForSeconds(secondsBeforeCheckForPlayers);
        }   
    }


    public void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, 35);
    }
}

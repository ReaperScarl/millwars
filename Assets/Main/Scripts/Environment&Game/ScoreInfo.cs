﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible to pass the info from GameWithNet To Results.
/// </summary>
public class ScoreInfo : MonoBehaviour {

    public int winner;

    public string myName;
    public int[,] redScores;
    public int[,] blueScores;
    public string[] playersname;
    public int[] rewards;
    
    public void ScoreUltimated(int win,int [,] redscores, int [,] bluescores, string[] playerName, int [] _rewards)
    {
        winner = win;
        redScores = redscores;
        blueScores = bluescores;
        playersname = playerName;
        rewards = _rewards;
    }
}

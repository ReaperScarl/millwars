﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Class that is responsible for the spawn of the team minions
/// </summary>
public class NexusSpawn : NetworkBehaviour {

    public int numberofminion;
    public int secsforwave;
    public int numberOfLanes;
    public float delayBetweenMandR = 0.2f;
    [Tooltip ("Time of wait before the minions start to spawn")]
    [Range (0,90)]
    public int timeBeforeSpawn=30;
    [Tooltip("Types of minion spawnable")]
    public GameObject[] minions;

    _Stats.Align align;
    private bool isCrazy;
    NexusStats ns;

    void Start () {
        ns = GetComponent<NexusStats>();
        align = ns.align;
        if (isServer)
            StartCoroutine(TimeToWave());
    }

    //Method that calls waves every *secforwave* seconds
    IEnumerator TimeToWave () {
        yield return new WaitForSeconds(timeBeforeSpawn);
        while (this.isActiveAndEnabled)
        {
            CmdSpawnWave(numberofminion,true);
            yield return new WaitForSeconds(delayBetweenMandR); // with this, the melee will go ahead of the ranged
            CmdSpawnWave(numberofminion, false);
            yield return new WaitForSeconds(secsforwave);
        }
	}

    [Command]
    void CmdSpawnWave(int nom, bool melee)
    {
        //number of different minions - in case a custom map will have different lanes ;) useless in the prototype, i know
        int modul= minions.Length/numberOfLanes;

        GameObject[] min = new GameObject[numberOfLanes];
        if (melee)
        {
            for (int i = 0; i < nom / 2; i++) //spawns half the number as melee
            {
                for (int j = 0; j < numberOfLanes; j++)
                    min[j] = GameObject.Instantiate(minions[j * modul]);//0=melee

                for (int j = 0; j < numberOfLanes; j++)
                {
                    min[j].tag = this.tag;
                    min[j].GetComponent<MinionStats>().align = align;
                    min[j].GetComponent<MinionStats>().isCrazy = ns.isCrazy;
                    min[j].GetComponent<MinionStats>().GoToPosition();
                    NetworkServer.Spawn(min[j]);
                }
            }
        }
        else
        {
            for (int i = 0; i < nom / 2; i++) //spawns half the number as ranged
            {
                for (int j = 0; j < numberOfLanes; j++) 
                    min[j] = GameObject.Instantiate(minions[1 + j * modul]); //1=ranged

                for (int j = 0; j < numberOfLanes; j++)
                {
                    min[j].tag = this.tag;
                    min[j].GetComponent<MinionStats>().align = align;
                    min[j].GetComponent<MinionStats>().isCrazy = ns.isCrazy;
                    min[j].GetComponent<MinionStats>().GoToPosition();
                    NetworkServer.Spawn(min[j]);
                }
            }
        }
    }
}

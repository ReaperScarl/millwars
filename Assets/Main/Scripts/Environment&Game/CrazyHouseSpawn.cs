﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Class responsible for the spawn of the crazy minions
/// </summary>
public class CrazyHouseSpawn : NetworkBehaviour {

    [Tooltip("Number of minions of the first Horde")]
    public int numberofminion=6;
    [Tooltip("Increment of minions for the next Horde (the previous + this increment)")]
    public int incrementofMinions = 6;
    public int numberOfLanes=2;
    public bool crazyMinions=false;
    public bool leftRed;
    [Tooltip("Types of minion spawnable")]
    public GameObject[] minions;

    [Command]
    void CmdSpawnWave(int nom) // to change when there are different lanes
    {
        //number of different minions
        int modul = minions.Length / numberOfLanes;

        //It spawns 
        for (int i = 0; i < nom; i++)
        {
            GameObject[] min = new GameObject[numberOfLanes];
   
            for (int j = 0; j < numberOfLanes; j++)
            {
                //0=melee
                min[j] = GameObject.Instantiate(minions[j * modul]);
            }
       
            // need to change the rotation because 1 of the 2 will look around
            for (int j = 0; j < numberOfLanes; j++)
            {
                CrazyMinionStats cms = min[j].GetComponent<CrazyMinionStats>();
                NetworkServer.Spawn(min[j]);
                cms.isReallyCrazy = crazyMinions;
                cms.vsLeftRed = leftRed;
                cms.GoToPosition();
                cms.SetTheTag();
            }
        }
    }

    public void SpawnTheWave() // this will spawn the wave and increment the number of minions for the next crazy spawn
    {
        this.CmdSpawnWave(numberofminion);
        numberofminion += incrementofMinions;
    }


}

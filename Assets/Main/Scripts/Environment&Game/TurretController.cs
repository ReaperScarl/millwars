﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Class that will control the Silo. It uses a FSM
/// </summary>
public class TurretController : NetworkBehaviour {
    
    public Transform partToRotate;
    public float changeTargetVelocity=10f;
    public float FSMReactionTime = 0.5f;
    //If it is not true, it won't attack the crazy minions
    public bool isCrazy;

    public GameObject bulletPrefab;
    public GameObject firePoint;
    public GameObject BaseDestroyed;

    protected bool isRed;

    private string[] factions = { "red", "blue", "crazyminion" };
    private TurretStats ts;
    private string[] targetTag = new string[2];
    [SyncVar]
    private Transform target;
    private float fireTimer;
    private bool isAttacking = false;
    private FSM fsm;

    void Start () {
        isRed = (this.GetComponent<TurretStats>().align == _Stats.Align.red) ? true : false;
        targetTag[0] = (isRed) ? factions[1] : factions[0];
        targetTag[1] = factions[2];
        ts = this.GetComponent<TurretStats>();

        // Defining states and link actions when enter/exit/stay for silo
        FSMState idle = new FSMState();
        idle.enterActions = new FSMAction[] {  };
        FSMState attacking = new FSMState();
        attacking.enterActions = new FSMAction[] { Attack };
        attacking.stayActions = new FSMAction[] { Attack };

        //Defining transitions
        FSMTransition t1 = new FSMTransition(TargetLocked);
        FSMTransition t2 = new FSMTransition(NoTargetLocked);

        //Link states with transitions
        idle.AddTransition(t1, attacking);
        attacking.AddTransition(t2, idle);

        //Setup a FSA at initial state
        fsm = new FSM(idle);

        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        StartCoroutine(SiloIsOn());
    }

	void LateUpdate () { // mandatory here or it would look bad

        if (target != null)
        {
            //Target Lock on
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation,lookRotation, Time.deltaTime*changeTargetVelocity).eulerAngles;
            partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }
    }

    //It finds the target
    void UpdateTarget() 
    {
        if (target != null && Vector3.Distance(target.transform.position, transform.position) < ts.visionRange)
            return;

        GameObject[] enemies = GameObject.FindGameObjectsWithTag(targetTag[0]);
        GameObject[] enemies2 = null;
        GameObject nearestEnemy = null;

        if (isCrazy)
                enemies2 = GameObject.FindGameObjectsWithTag(targetTag[1]);
        float shortestDistance = ts.visionRange + 1; 
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (isCrazy)
        {
            //part 2
            foreach (GameObject enemy in enemies2)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }
        }
        if (nearestEnemy != null && shortestDistance <= ts.visionRange)
            target = nearestEnemy.transform;
        if (nearestEnemy != null && Vector3.Distance(nearestEnemy.transform.position, transform.position) > ts.visionRange)
            target = null;
        if (nearestEnemy == null) //weird but probably necessary
            target = null;
    }

    //This function will update the fsm
    public IEnumerator SiloIsOn()
    {
        while (transform) //while the transfom is not null -> until it is not dead
        {
            fsm.Update();
            yield return new WaitForSeconds(FSMReactionTime);
        }
    }

    private IEnumerator Shoot()
    {
        //Animation
        firePoint.transform.parent.GetComponent<Animation>().Play("Shoot");
        yield return new WaitForSeconds(ts.fireRate);
        isAttacking = false;
    }

    //Method called by the champion when attacked by another champion
    public void ChangeTarget(Transform t)
    {
        if (target==null || target.GetComponent<_Stats>().objectType != _Stats.ObjectTypes.champion)
        {
            if (Vector3.Distance(transform.position,t.position) <= ts.visionRange)
            {
                target = t;
            }
        }
    }

    public void TowerDestroyed()
    {
        if(isServer)
            CmdUpdateTheScore();
        transform.parent.GetComponentInChildren<GameManager>().FirstTowerDestroyed(this.tag);
        GameObject obj=GameObject.Instantiate(BaseDestroyed, this.transform.Find("Base").transform.position, new Quaternion());
        NetworkServer.Spawn(obj);
        Destroy(transform.gameObject);
    }

    [Command]
    private void CmdUpdateTheScore()
    {
        transform.parent.Find("ScoreManager").GetComponent<ScoreManager>().TowerDestroyed(this.tag);
    }

    //Conditions
    bool TargetLocked()
    {
        return target != null;
    }

    bool NoTargetLocked()
    {
        return !TargetLocked();
    }

    //Actions
    void Attack()
    {
        if (!isAttacking)
        {
            if (isServer)
            {
                if (this.transform.gameObject != null)
                {
                    isAttacking = true;
                    GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.transform.position, firePoint.transform.rotation);
                    BulletController bullet = bulletGO.GetComponent<BulletController>();
                    bulletGO.transform.LookAt(target);
                    bulletGO.GetComponent<Rigidbody>().velocity = bulletGO.transform.forward * bullet.bulletSpeed;
                    if (bullet != null)
                        bullet.Seek(target, ts.damage);
                    Destroy(bulletGO, 2.0f);
                    NetworkServer.Spawn(bulletGO);
                    StartCoroutine(Shoot());
                }
            }
            else
            {
                firePoint.transform.parent.GetComponent<Animation>().Play("Shoot");
            }      
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

/// <summary>
/// It's the class used for the count down in the game.
/// </summary>
public class CountDownManager : NetworkBehaviour {

    //Time limit of play. Can change in the custom match
    public int timeOfPlay = 15;
    //Time for the first spawn. Will change in custom match
    public float minuteForTheFirstSpawn = 6;
    //Time for the next spawns after the first. Will change in custom match
    public float timeOfNextSpawn = 3;
    //Time before the waves will spawn after the start of the match
    [Tooltip ("Time in seconds before the start of the minion waves")]
    public float timeBeforeTheWavesSpawns=30;

    public GameObject manager;
    public Text infoMessage;

    bool alreadySpawn;
    [SyncVar (hook= "OnChangedCountdown")]
    private float countdown;
    private float nextspawn;

	void Start () {
        countdown= timeOfPlay * 60;
       
        nextspawn = timeOfPlay - minuteForTheFirstSpawn-1;
        this.GetComponent<Text>().text = "" + (int)countdown / 60 + " : " + countdown % 60;

        if (isClient) // To fix the rect on its original position
            this.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, -100, 0);

        if (isServer)
        {
            StartCoroutine(ChangeText());
            StartCoroutine(CheckIfItsTimeToSpawn());
        }
             
	}

    //It Will change the countdown once for sec and will make the spawn of crazy minions
    IEnumerator ChangeText()
    {
        while (true)
        {
            countdown -= 1;
            int minutes = (int)countdown / 60;

            //End of the game for time out
            if (minutes == 0 && (int)countdown % 60 == 0)
                TimesUp();

            if (!alreadySpawn && (minutes == nextspawn))
            {
                CallHorde();
                alreadySpawn = true;
            }
            else
                if (alreadySpawn && (minutes != nextspawn))
            {
                alreadySpawn = false;
            }
            yield return new WaitForSeconds(1);
        }
    }

    // Sohuld call a method in GameManager to end the game and give the results
    void TimesUp()
    {
        manager.GetComponent<GameManager>().EndGame();
    }

    void CallHorde() { // Should be called only on the server and then instantiate the crazy minions
        manager.GetComponent<GameManager>().WaveOfCrazyMinions();
        RpcHordeComing();
        nextspawn -= timeOfNextSpawn;
    }
    //Callback from the sync var countdown
    void OnChangedCountdown(float newcountdown)
    {
        this.GetComponent<Text>().text = "" + (int)newcountdown/60 + " : " + (int)newcountdown % 60;
    }

    [ClientRpc]
    public void RpcHordeComing()
    {
        infoMessage.text = "The horde is coming!";
        infoMessage.gameObject.SetActive(true);
        infoMessage.GetComponent<DelayedDisappearScript>().Disappear();
    }


    [ClientRpc]
    public void RpcWaveStarted()
    {
        infoMessage.text = "Chickens have spawned!";
        infoMessage.gameObject.SetActive(true);
        infoMessage.GetComponent<DelayedDisappearScript>().Disappear();
    }

    //Dependent. This will let the players know when the first wave of minions will be spawned
    IEnumerator CheckIfItsTimeToSpawn()
    {
        while ( countdown +timeBeforeTheWavesSpawns > (timeOfPlay*60))
        {
            yield return new WaitForSeconds(1);
        }
        RpcWaveStarted();
    }

}

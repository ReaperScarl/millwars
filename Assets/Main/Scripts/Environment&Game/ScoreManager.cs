﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

/// <summary>
/// 
/// Class manager of the rewards.
/// - The relative score is when at least 1 point is taken.
/// - Where the scores informations are put in redscores and blue scores.
/// </summary>
public class ScoreManager : NetworkBehaviour {

    public Text blueScoreText;
    public Text redScoreText;
    public Text InfoText;
    public int minionsperpoint = 3;
    //several types of rewards:
    [Tooltip("Different types of reward: \n 0: Minion killed \n 1: Tower destroyed \n" +
        "2: Player Killed \n 3: Boss Killed \n 4: Nexus Destroyed \n 5: Death penalty")]
    public int[] rewards;

    //They follow the order of the rewards. the last one is the death penalty
    private int[,] redScores=new int [2,6];   
    private int[,] blueScores=new int[2, 6];
    private string[] playersname = new string[4]; //The names of the players: 0-1 red 2-3 blue
    private Transform[] playersTransform = new Transform[4];

    [SyncVar (hook ="UpdateBlueScore")]
    private int blueScore = 0;
    [SyncVar(hook = "UpdateRedScore")]
    private int redScore = 0;

    void Start () {
        redScore = 0;
        blueScore = 0;
        blueScoreText.text = "0";
        redScoreText.text = "0";
    }

    // A tower is destroyed and the point need to be destributed
    public void TowerDestroyed(string tag)
    {
        // Has to give points to both players of the team
        if (tag == "blue")
        {
            redScores[0, 1] += 1;
            RpcUpdatePlayerScore(true, 0, 1, redScores[0, 1]);
            redScores[1, 1] += 1;
            RpcUpdatePlayerScore(true, 1, 1, redScores[1, 1]);
            redScore += rewards[1]*2;
            ChangeScore(redScore, true);
            RpcChangeInfoMatchText(0, true, null, null);
        }
        else
        {
            blueScores[0, 1] += 1;
            RpcUpdatePlayerScore(false, 0, 1, blueScores[0, 1]);
            blueScores[1, 1] += 1;
            RpcUpdatePlayerScore(false, 1, 1, blueScores[1, 1]);
            blueScore += rewards[1]*2;
            ChangeScore(blueScore, false);
            RpcChangeInfoMatchText(0, false, null, null);
        }
    }

    // Method called when the Nexus has been destroyed.
    public void NexusDestroyed(string tag)
    {
        if (tag == "blue")
        {
            redScores[0, 4] += 1;
            RpcUpdatePlayerScore(true, 0, 4, redScores[0, 4]);
            redScores[1, 4] += 1;
            RpcUpdatePlayerScore(true, 1, 4, redScores[1, 4]);
            redScore += rewards[4]*2;
            ChangeScore(redScore, true);
        }
        else
        {
            blueScores[0, 4] += 1;
            RpcUpdatePlayerScore(false, 0, 4, blueScores[0, 4]);
            blueScores[1, 4] += 1;
            RpcUpdatePlayerScore(false, 1, 4, blueScores[1, 4]);
            blueScore += rewards[4]*2;
            ChangeScore(blueScore, false);
        }
    }

    /// <summary>
    /// Called when a boss dies.
    /// </summary>
    /// <param name="tag">team of the last hit.</param> 
    public void BossKilled(string tag)
    {
        if (tag == "red")
        {
            redScores[0, 3] += 1;
            RpcUpdatePlayerScore(true, 0, 3, redScores[0, 3]);
            redScores[1, 3] += 1;
            RpcUpdatePlayerScore(true, 1, 3, redScores[1, 3]);
            redScore += rewards[3] * 2;
            ChangeScore(redScore, true);
            RpcChangeInfoMatchText(1, true, null, null);
        }
        else
        {
            blueScores[0, 3] += 1;
            RpcUpdatePlayerScore(false, 0, 3, blueScores[0, 3]);
            blueScores[1, 3] += 1;
            RpcUpdatePlayerScore(false, 1, 3, blueScores[1, 3]);
            blueScore += rewards[3] * 2;
            ChangeScore(blueScore, false);
            RpcChangeInfoMatchText(1, false, null, null);
        }
    }

    //Called by the player when it has made a minion reward
    public void PointForMinion(string tag,string playername)
    {
        int indexplayer = IndexPlayer(playername);
        if (tag == "red")
        {
            redScores[(int)indexplayer , 0]+=1;
            RpcUpdatePlayerScore(true, indexplayer, 0, redScores[indexplayer, 0]);
            redScore += rewards[0];
            ChangeScore(redScore, true);
        }
        else
        {
            blueScores[indexplayer % 2, 0] += 1;
            RpcUpdatePlayerScore(false, indexplayer % 2, 0, blueScores[indexplayer % 2, 0]);
            blueScore += rewards[0];
            ChangeScore(blueScore, false);
        }
    }
    
    //Called when a champion has killed an enemy champion
    public void ChampionKilled(string tag, string playername, string playerKilled)
    {
        int indexplayer = IndexPlayer(playername);
        if (tag == "red")
        {
            redScores[indexplayer, 2] += 1;
            RpcUpdatePlayerScore(true, indexplayer, 2, redScores[indexplayer, 2]);
            redScore += rewards[2];
            ChangeScore(redScore, true);
            RpcChangeInfoMatchText(2, true, playername, playerKilled);
        }
        else
        {
            blueScores[indexplayer % 2, 2] += 1;
            RpcUpdatePlayerScore(false, indexplayer % 2, 2, blueScores[indexplayer % 2, 2]);
            blueScore += rewards[2];
            ChangeScore(blueScore, false);
            RpcChangeInfoMatchText(2, false, playername, playerKilled);
        }
        
    }

    // Called when a champion has been killed by a enemy champion
    public void ChampionDead(string tag, string playername)
    {
        int indexplayer = IndexPlayer(playername);
        if (tag == "red")
        {
            redScores[(int)indexplayer, 5] += 1;
            RpcUpdatePlayerScore(true, (int)indexplayer, 5, redScores[(int)indexplayer, 5]);
            redScore -= rewards[5];
            ChangeScore(redScore, true);
        }
        else
        {
            blueScores[indexplayer % 2, 5] += 1;
            RpcUpdatePlayerScore(false, indexplayer % 2, 5, blueScores[indexplayer % 2, 5]);
            blueScore -= rewards[5];
            ChangeScore(blueScore, false);
        }
    }

    /// <summary>
    /// Called At the start of the match by every player
    /// </summary>
    /// <param name="tag">the tag of the player caller</param>
    /// <param name="playername">the name of the player caller</param>
    public void PlayersName(string tag, string playername, Transform t)
    {
        if (tag == "red")
        {
            for (int i = 0; i < playersname.Length / 2; i++) //The first 2 are the red team
            {
                if (playersname[i] == playername)
                    return;
                if (playersname[i] == null)
                {
                    playersTransform[i] = t;
                    playersname[i] = playername;
                    return;
                }
            }
        }
        else
        {
            for (int i = 2; i < playersname.Length; i++)   //The second 2 are the blue team
            {
                if (playersname[i] == playername)
                    return;
                if (playersname[i] == null)
                {
                    playersname[i] = playername;
                    return;
                }
                    
            }
        }
        Debug.Log(playername.ToString());

    }

    public int IndexPlayer(string playername) //given a string, return the index of the player
    {
        for (int i = 0; i < playersname.Length; i++)
        {
            if (playersname[i] == playername)
                return i;
        }
        Debug.Log("Shouldn't be here");
        return -1;
    }

    private void ChangeScore(int score, bool isred)
    {
        if (isred)
            redScoreText.text = "" + score;
        else
            blueScoreText.text = "" + score;
    }

    //returns the winner. if it is = 0 , it's a draw.
    public byte Winner()
    {
        int winner;
        if (redScore == blueScore) 
            winner= 0;
        else
        {
            if (redScore > blueScore)
                winner = 1;
            else
                winner = 2;
        }
        GameObject.Find("LobbyManagerCanvas").GetComponent<ScoreInfo>().ScoreUltimated(winner, redScores, blueScores, playersname, rewards);
        if(isServer)
            RpcWinner(winner);
        return (byte)winner;
    }

    private void UpdateRedScore(int newScore)
    {
        redScore = newScore;
        redScoreText.text = "" + newScore;
    }

    private void UpdateBlueScore(int newScore)
    {
        blueScore = newScore;
        blueScoreText.text = "" + newScore;
    }

    [ClientRpc]
    private void RpcUpdatePlayerScore(bool isRed, int playerIndex, int RecordIndex, int value) // every player will update their scores -> better then sync lists, sorry
    {
        if (isRed)
            redScores[playerIndex, RecordIndex] = value;
        else
            blueScores[playerIndex, RecordIndex] = value;    
    }

    [ClientRpc]
    private void RpcWinner(int win) // otherwise it could be not the same
    {
        GameObject.Find("LobbyManagerCanvas").GetComponent<ScoreInfo>().winner = win;
    }

    /// <summary>
    /// This function will change the InfoMatchText
    /// </summary>
    /// <param name="index">The case of the call. 0:Tower, 1: Boss, 2: Champion Killed</param>
    /// <param name="isRed"></param>
    [ClientRpc]
    private void RpcChangeInfoMatchText(int index, bool isRed, string playerKiller, string playerKilled) 
    {
        InfoText.gameObject.SetActive(true);
        switch (index)
        {
            case 0: // Tower
                
                InfoText.text =(isRed)? "Red Team has destroyed a silo": "Blue Team has destroyed a silo";
                break;
            case 1: // Boss
                InfoText.text = (isRed) ? "Red Team has slain a boss" : "Blue Team has slain a boss";
                break;
            case 2:
                InfoText.text = "" + playerKiller + " has slain " + playerKilled;
                break;
        }
        InfoText.GetComponent<DelayedDisappearScript>().Disappear();

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
/// <summary>
/// Class responsible for the game.
/// </summary>
public class GameManager : NetworkBehaviour  {

    //public Text countDown;
    [Tooltip("The objects are The nexuses and the towers. The first 2 are the nexuses and the rest are the towers")]
    public GameObject[] crazyObjectToSet;
    public GameObject crazyHouse;
    public bool reallyCrazyMinions;
    public Text winner;
    public RectTransform goToResults;
    public RectTransform infoMessage;


    private bool bluefirstsilo = false;
    private bool redfirstsilo = false;

    // attribute about which one to call
    private bool firstLeft; //-> for the crazy minion spawn, to make it random
    private ScoreManager scoremanager;

	void Start () {
        firstLeft = (Random.Range(0, 2) == 0) ? true : false;
        scoremanager = transform.parent.GetComponentInChildren<ScoreManager>();

        //Very dependent, i know.
        foreach (GameObject element in crazyObjectToSet)
        {
            _Stats stats =element.GetComponent<_Stats>();
            if (stats.objectType == _Stats.ObjectTypes.nexus)
            {
                element.GetComponent<NexusStats>().isCrazy = reallyCrazyMinions;
                //Close the nexus or deactivate in the prefab
            }
            if (stats.objectType == _Stats.ObjectTypes.turret)
                element.GetComponent<TurretController>().isCrazy = reallyCrazyMinions;   
        }
            crazyHouse.GetComponent<CrazyHouseSpawn>().crazyMinions = reallyCrazyMinions;
    }

    //Will call the spawn
    public void WaveOfCrazyMinions()
    {
        crazyHouse.GetComponent<CrazyHouseSpawn>().leftRed = firstLeft;
        crazyHouse.GetComponent<CrazyHouseSpawn>().SpawnTheWave();
        firstLeft = !firstLeft ;
    }

    //When a tower is destroyed, this method will be called to activate the nexus bar and receiver.
    public void FirstTowerDestroyed(string tag)
    {
        if (tag == "blue")
        {
            if (!bluefirstsilo)
            {
                bluefirstsilo = true;
                RpcOpenTheNexus(tag);
            }
        }
        if (tag == "red")
        {
            if (!redfirstsilo)
            {
                redfirstsilo = true;
                RpcOpenTheNexus(tag);
            }
        }
    }

    // Method for the end of the match. Can called from 2 events: nexus destroyed or time out.
    private void Results(int loser)
    {
        //Fine partita.
        winner.gameObject.SetActive( true);
        int win = scoremanager.Winner();
        GameObject netMan = GameObject.Find("LobbyManagerCanvas");
        ScoreInfo scoreInfo;
        bool isRed=false;
        if (netMan) // it should always enter
        {
            scoreInfo = netMan.GetComponent<ScoreInfo>();
            scoreInfo.winner = win;
            isRed = (scoremanager.IndexPlayer(scoreInfo.myName) < 2) ? true : false;
        }
        else
            Debug.Log("Error! score info missing"); // if it shows up, it's a problem, so leave it on.
     
        switch (win) // To change With the tag of the local player
        {
            case 0:
                winner.text= "It's a draw!";
                break;                    
            case 1:
                winner.text= (isRed)? "Victory!": "Defeat!";
                break;
            case 2:
                winner.text = (!isRed) ? "Victory!" : "Defeat!";
                break;
        }
    }

    [ClientRpc]
    public void RpcEndGame(int loserindex)
    {
        Results(loserindex);
        goToResults.gameObject.SetActive(true);
        //Put the Cursor Available
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        //Stop the time
        Time.timeScale = 0.00001f;
    }

    // Calls to End the game to all the clients
    public void EndGame()
    {
        RpcEndGame(0);
    }

    [ClientRpc]
    private void RpcOpenTheNexus(string tag)
    {
        Transform canvas; 
        if (tag == "red") //Red team
        {
            canvas = crazyObjectToSet[0].transform.Find("Canvas");
            crazyObjectToSet[0].GetComponent<DamageReceiver>().enabled = true;
        }
        else
        {
            canvas = crazyObjectToSet[1].transform.Find("Canvas");
            crazyObjectToSet[1].GetComponent<DamageReceiver>().enabled = true;
        }
        canvas.gameObject.SetActive(true);
    }

    //The nexus is been destroyed. The match is finished.
    public void NexusDestroyed(string nexustag)
    {
        scoremanager.NexusDestroyed(nexustag);
        if(nexustag=="red")
            RpcEndGame(1); // red lost
        else
            RpcEndGame(2); // Blue lost
    }
}


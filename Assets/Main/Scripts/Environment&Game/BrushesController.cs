﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// The Class responsible for the animation of the bushes
/// </summary>
public class BrushesController : MonoBehaviour {

    Animation movement;

    void Start()
    {
        movement = GetComponent<Animation>();
    }

    void OnTriggerEnter(Collider other)
    {
        this.movement.Play("movement");
    }
}

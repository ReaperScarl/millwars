﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible for the animations for minions (range)
/// </summary>
public class MinionRangeAnimationController : _AnimationController {

    //it Has 2 animation to do: the movement/attack of the minion, and the crossbow.
    //the 2 and 3 will be used by the crossbow
    public Transform crossbow;
    private Animation animBow;
    

    private void Start()
    {
        animBow = crossbow.GetComponent<Animation>();
        anim = GetComponent<Animation>();
    }

    public override void Move()
    {
        if (!anim.IsPlaying(animationsName[0]))
        {
            anim.Play(animationsName[0]);
            animBow.Play(animationsName[2]);
        }
    }

    public override void Attack()
    {
        anim.Play(animationsName[1]);
        animBow.Play(animationsName[3]);
    }

    public override bool WakeUp()
    {
        throw new System.NotImplementedException();
    }

}

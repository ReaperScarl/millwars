﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(BossStats))]
///Class used to attack by the boss
public class BossAttackController : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public Transform firePoint;
    private Transform target;

    private BossAnimationController animCont;

    void Start()
    {
        if (this.GetComponentInChildren<BossAnimationController>())
            animCont = this.GetComponentInChildren<BossAnimationController>();
    }

    public void Attack(_Stats.Type bosstype, Transform t)
    {
        switch (bosstype)
        {
            case _Stats.Type.melee:
                target = t;
                RpcMeleeAttack();
                break;
            case _Stats.Type.range:
                RangedAttack(t);
                break;
        }
    }

    // Method for the ranged attack. for the prototype i didn't have an animation for that attack, sorry
    void RangedAttack(Transform t)
    {
        float dmg;
            dmg = GetComponent<BossStats>().damage;

        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        BulletController bullet = bulletGO.GetComponent<BulletController>();
        if (this.transform.gameObject != null)
        {
            bulletGO.transform.LookAt(t);
            bulletGO.GetComponent<Rigidbody>().velocity = bulletGO.transform.forward * bullet.bulletSpeed;

            if (bullet != null)
                bullet.Seek(t, dmg);
            Destroy(bulletGO, 2.0f);
            NetworkServer.Spawn(bulletGO);
        }
        if (bullet != null)
            bullet.Seek(t, dmg);
    }

    [ClientRpc]
    void RpcMeleeAttack()
    {
        StartCoroutine(MeleeAttack(target));
    }

    // On melee there is not a bullet
    IEnumerator MeleeAttack(Transform t)
    {
        float dmg;

        dmg = GetComponent<BossStats>().damage;
		DamageProvider dp = new DamageProvider();
        if (dp != null)
        {
            if (animCont)
                animCont.Attack();
            yield return new WaitForSeconds(animCont.timeAnimBeforeShoot);

            dp.ProvideDamage(t, (int)dmg);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// Class responsible for the attacks for minions
/// </summary>
public class MinionAttackController : NetworkBehaviour {

    public GameObject bulletPrefab;

    private Transform firePoint;
    private bool isMelee;
    private _AnimationController animCont;

	void Start () {
        if (GetComponent<MinionStats>() != null)
            isMelee = GetComponent<MinionStats>().isMelee;
        else
            isMelee = GetComponent<CrazyMinionStats>().isMelee;

        if (this.GetComponentInChildren<_AnimationController>())
        {
            if(isMelee)
                animCont = this.GetComponentInChildren<MinionAnimationController>();
            else
                animCont= this.GetComponentInChildren<MinionRangeAnimationController>();
        }
        firePoint = transform.Find("firePoint");
	}
	
    public void Attack(Transform t)
    {
        if (isMelee)
            StartCoroutine(MeleeAttack(t));
        else
            StartCoroutine(RangedAttack(t));   
    }

    //On Ranged there is a bullet to spawn
    IEnumerator RangedAttack(Transform t)
    {
        if (animCont)
            animCont.Attack();

        float dmg;
        if (GetComponent<MinionStats>() != null)
            dmg = GetComponent<MinionStats>().damage;
        else
            dmg = GetComponent<CrazyMinionStats>().damage;

        //Debug.Log(animCont.timeAnimBeforeShoot);
        yield return new WaitForSeconds(animCont.timeAnimBeforeShoot);
        if(isServer)
            Shoot(t, dmg);
    }


    private void Shoot(Transform t, float dmg)
    {
        if (this.transform.gameObject != null)
        {
            GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
           
            BulletController bullet = bulletGO.GetComponent<BulletController>();
            bulletGO.transform.LookAt(t);

            bulletGO.GetComponent<Rigidbody>().velocity =bulletGO.transform.forward* bullet.bulletSpeed;
      
            if (bullet != null)
                bullet.Seek(t, dmg);
            Destroy(bulletGO, 2.0f);
            NetworkServer.Spawn(bulletGO);
        }
    }

    // On melee there is not a bullet
    IEnumerator MeleeAttack(Transform t)
    {
        float dmg;
        if (GetComponent<MinionStats>() != null)
            dmg = GetComponent<MinionStats>().damage;
        else
            dmg = GetComponent<CrazyMinionStats>().damage;
		DamageProvider dp = new DamageProvider();
        if (dp != null)
        {
            if (animCont)
                animCont.Attack();

            yield return new WaitForSeconds(animCont.timeAnimBeforeShoot);
            if (this.transform.gameObject != null)
                dp.ProvideDamage(t, (int)dmg);
        }
   }
}

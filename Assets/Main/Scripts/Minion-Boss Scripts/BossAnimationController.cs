﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Child of animation controller. does the animations for the bosses
/// </summary>
public class BossAnimationController : _AnimationController
{
    public bool isAwake = false;

    private void Start()
    {
        anim = GetComponent<Animation>();
    }

    public override void Move()
    {
        /*
        if (!anim.IsPlaying(animationsName[0]))
        {
            anim.Play(animationsName[0]);
        }
        */
    }

    public override void Attack()
    {
        anim.Play(animationsName[1]);
    }

    public override void Sleep()
    {
        isAwake = false;
       // anim.Play(animationsName[3]);
    }

    public override bool WakeUp()
    {
        //anim.Play(animationsName[2]);
        StartCoroutine(WaitTheAnimation(2));
        return isAwake;
    }

    private IEnumerator WaitTheAnimation(float seconds)
    {
        if (!isAwake)
        {
            yield return new WaitForSeconds(2);
            isAwake = true;
        }
    }
}

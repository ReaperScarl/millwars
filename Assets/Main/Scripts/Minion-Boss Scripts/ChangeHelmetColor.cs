﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class provides the behaviour to switch the helmet Color
/// </summary>
public class ChangeHelmetColor : MonoBehaviour
{

    [Tooltip("3 colors: red, blue, gray(for crazyminion)")]
    public Material[] helmetColors;

    [Tooltip("3 colors: red, blue, gray(for crazyminion)")]
    public Material[] bodyColors= new Material[3];

    public void ChangeColor()
    {
        Material[] newMaterials = this.GetComponent<SkinnedMeshRenderer>().materials;
        
        if (this.transform.root.tag == "red")
        {
            newMaterials[0] = helmetColors[0];
            newMaterials[2] = bodyColors[0];
        }
            
        if (this.transform.root.tag == "blue")
        {
            newMaterials[0] = helmetColors[1];
            newMaterials[2] = bodyColors[1];
        }
            
        if (this.transform.root.tag == "crazyminion")
        {
            newMaterials[0] = helmetColors[2];
            newMaterials[2] = bodyColors[2];
        }
           
        GetComponent<SkinnedMeshRenderer>().materials = newMaterials;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible for the animations for the minions (melee)
/// </summary>
public class MinionAnimationController : _AnimationController {

    private void Start()
    {
        anim = GetComponent<Animation>();
    }

    public override void Move()
    {
        if (!anim.IsPlaying(animationsName[0]))
        {
            anim.Play(animationsName[0]);
        } 
    }

    public override void Attack()
    {
        anim.Play(animationsName[1]);
    }

    public override bool WakeUp()
    {
        throw new NotImplementedException();
    }
}

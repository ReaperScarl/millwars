﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Abstract class used as father for the animation controllers
/// </summary>
public abstract class _AnimationController : MonoBehaviour
{
    //0: move ; 1 : attack
    [Tooltip("0: move , \n 1: attack , \n 2: WakeUp , \n 3 Sleep")]
    public string[] animationsName;
    [Tooltip("Seconds before the hit is actually made from the animation")]
    public float timeAnimBeforeShoot;

    protected Animation anim;
    /// <summary>
    /// Animation to move
    /// </summary>
    public abstract void Move();
    /// <summary>
    /// Animation to Attack
    /// </summary>
    public abstract void Attack();
    /// <summary>
    /// Animation to Sleep
    /// </summary>
    public virtual void Sleep() { }
    /// <summary>
    /// Animation to Wake up
    /// </summary>
    /// <returns></returns>
    public abstract bool WakeUp();
}
   

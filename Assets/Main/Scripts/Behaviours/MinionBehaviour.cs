﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
/// <summary>
/// Class responsible for the behaviour of the team minions. It uses a FSM.
/// </summary>
public class MinionBehaviour : NetworkBehaviour {

    public float reactionTime = 0.3f;
    public float changeTargetVelocity;
    [SyncVar]
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public NavMeshAgent agent;

    protected MinionStats ms;
    private MinionChasingBehaviour mcb;
    private MinionMovementBehaviour mmb;
    protected float totFightRange;
    private bool isRed;
    private string[] targetTag = new string[2];
    private string[] factions = { "red", "blue", "crazyminion" };

    private FSM fsm;

    void Start()
    {
        //inizialization of the elements
        agent = GetComponent<NavMeshAgent>();
        ms = this.GetComponent<MinionStats>();
        isRed = (ms.align == _Stats.Align.red) ? true : false;
        targetTag[0] = (isRed) ? factions[1] : factions[0];
        targetTag[1] = factions[2];
        totFightRange = ms.fightRange;
        mcb = GetComponent<MinionChasingBehaviour>();
        mmb = GetComponent<MinionMovementBehaviour>();

        // Defining states and link actions when enter/exit/stay for team minions
        FSMState walking = new FSMState();
        walking.enterActions = new FSMAction[] { FollowThePath };
        walking.stayActions = new FSMAction[] { FollowThePath };
        FSMState chasing = new FSMState();
        chasing.enterActions = new FSMAction[] { Chase };
        chasing.stayActions= new FSMAction[] { Chase };
        FSMState attacking = new FSMState();
        attacking.enterActions = new FSMAction[] { Attack };
        attacking.stayActions = new FSMAction[] { Attack };

        //Defining transitions
        FSMTransition t1 = new FSMTransition(AttackCondition);
        FSMTransition t2 = new FSMTransition(SomeoneInsideCondition);
        FSMTransition t3 = new FSMTransition(NoneInsideCondition);

        //Link states with transitions
        walking.AddTransition(t1, attacking);
        walking.AddTransition(t2, chasing);
        chasing.AddTransition(t1, attacking);
        chasing.AddTransition(t3, walking);
        attacking.AddTransition(t2, chasing);
        attacking.AddTransition(t3, walking);

        //Setup a FSA at initial state
        fsm = new FSM(walking);

        StartCoroutine(ChickenIsUp());
        if(isServer)
            StartCoroutine(UpdateTarget());
        if (isClient)
            agent.enabled = true;
    }

    private void LateUpdate() // the rotation if not put in Update/LateUpdate is ugly to see
    {
        if (target!=null) //Target Lock on
        {
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * changeTargetVelocity).eulerAngles;
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }
    }

    //Funtion that updates the fsm
    IEnumerator ChickenIsUp()
    {
        while (transform)
        {
            fsm.Update();
            yield return new WaitForSeconds(reactionTime);
        }
    }

    //This function is responsible to find the target
    IEnumerator UpdateTarget()
    {
        while (this.transform)
        {
            while (target != null && Vector3.Distance(target.transform.position, transform.position) < ms.visionRange)
                yield return new WaitForSeconds(reactionTime);

            GameObject[] enemies = GameObject.FindGameObjectsWithTag(targetTag[0]);
            GameObject[] enemies2 = null;

            if (ms.isCrazy)
                enemies2 = GameObject.FindGameObjectsWithTag(targetTag[1]);

            float shortestDistance = ms.visionRange + 1;
            GameObject nearestEnemy = null;
            foreach (GameObject enemy in enemies)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    // Debug.Log(distanceToEnemy);
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }

            if (ms.isCrazy) // when the crazy minion feature is active
            {
                //part 2
                foreach (GameObject enemy in enemies2)
                {
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                    if (distanceToEnemy < shortestDistance)
                    {
                        // Debug.Log(distanceToEnemy);
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = enemy;
                    }
                }
            }

            if (nearestEnemy != null && shortestDistance <= ms.visionRange)
                target = nearestEnemy.transform;

            if (nearestEnemy != null && Vector3.Distance(nearestEnemy.transform.position, transform.position) > ms.visionRange)
                target = null;
            if (nearestEnemy == null && target != null)
                target = null;
            yield return new WaitForSeconds(reactionTime);
        }
    }

    // The stopping distance of the agent has to change depending of the target and if it a point of the path
    public void ChangeStopDistance()
    {
        if (target==null)
            agent.stoppingDistance = 0.2f;
        else
        {
            if (ms.fightRange == totFightRange)
                totFightRange += target.GetComponent<_Stats>().offSet;         
            if (ms.isMelee)
                agent.stoppingDistance = ms.fightRange - 0.2f;
            else
                agent.stoppingDistance = ms.fightRange - 0.3f;
        }
    }

    //Method called by the champion when attacked by another champion
    public void ChangeTarget(Transform t)
    {
        Debug.Log("I'm changing the target");
        if (target==null || target.GetComponent<_Stats>().objectType != _Stats.ObjectTypes.champion)
            if (Vector3.Distance(transform.position,t.position) <= ms.visionRange)
                target = t;
    }

    //When it is in range to attack, it has to stop moving and attack.
    void ChangeSpeed(bool inRange)
    {
        if (inRange)
        {
            if (agent.speed == 0)
                return;
            else
            {
                agent.speed = 0;
                agent.velocity = Vector3.zero;
            }
        }
        else
        {
            if (agent.speed == ms.movement)
                return;
            else
                agent.speed = ms.movement;
        }
    }

    //-----Conditions
    bool AttackCondition()
    {
        return (target != null && Vector3.Distance(transform.position, target.transform.position) < totFightRange);
    }

    bool SomeoneInsideCondition()
    {
        return target != null;
    }

    bool NoneInsideCondition()
    {
        return !SomeoneInsideCondition();
    }

    //-----Actions
    void FollowThePath()
    {
        ChangeSpeed(false);
        mmb.Walk();
    }

    void Chase()
    {
        ChangeSpeed(false);
        mcb.Chase(agent, target);
        ChangeStopDistance();
    }

    void Attack()
    {
        ChangeSpeed(true);
        mcb.Fight(target);
    }
 
}

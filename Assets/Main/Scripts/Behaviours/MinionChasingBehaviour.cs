﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
/// <summary>
/// Class responsible to chase the targets. Called by the minion behaviour
/// </summary>
public class MinionChasingBehaviour : NetworkBehaviour {

    [HideInInspector]
    public bool alreadyMoving=true;

    private _AnimationController animCont;
    private MinionStats ms;
    private MinionMovementBehaviour pb;
    private bool isAttacking = false;

    void Start()
    {
        ms = this.GetComponent<MinionStats>();
        if (this.GetComponentInChildren<_AnimationController>())
        {
            if (ms.isMelee)
                animCont = this.GetComponentInChildren<MinionAnimationController>();
            else
                animCont = this.GetComponentInChildren<MinionRangeAnimationController>();
        }
    }

    //It will chase the target t with the NavMeshAgent agent. Called by the chase in fsm
    public void Chase(NavMeshAgent agent, Transform t)
    {
        if(agent!=null&& agent.isActiveAndEnabled)
            agent.SetDestination(t.position);
        if (animCont && !alreadyMoving&&!isAttacking)
        {
            animCont.Move();
            alreadyMoving = true;
        }
    }

    //It will attack the target t. Called by the attack in fsm
    public void Fight( Transform t)
    {
        if (!isAttacking)
            StartCoroutine(Shoot(t));    
    }

    private IEnumerator Shoot(Transform t)
    {
        alreadyMoving = false;
        isAttacking = true;
        GetComponent<MinionAttackController>().Attack(t);
        yield return new WaitForSeconds(ms.fireRate);
        isAttacking = false;
    }
}

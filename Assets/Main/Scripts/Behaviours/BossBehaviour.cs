﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

/// <summary>
/// Class that controls the bosses
/// </summary>
public class BossBehaviour : NetworkBehaviour {

    public Transform bossBase;

    public float changeTargetVelocity = 10;
    public float reactionTime = 0.5f;
    protected NavMeshAgent agent;
    protected BossStats bs;

    [HideInInspector]
    public bool isSleeping = true;
    private Transform[] players= new Transform[4];
    [SyncVar]
    private Transform target;
    private float[] aggro = new float[4];
    private BarModifierManager hb;
    private bool nooneInside = true;
    private bool firstAttack = true;
    private bool isAttacking = false;
    private BossAttackController bAttC;
    private BossAnimationController bAnimC;

    private FSM awakeFSM;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        bs = GetComponentInChildren<BossStats>();
        hb = GetComponentInChildren<DamageReceiver>().healthbar.transform.GetComponent<BarModifierManager>();
        hb.SetConversionPToB(bs.maxHealth);
        bAttC = GetComponentInChildren<BossAttackController>();
        bAnimC = GetComponentInChildren<BossAnimationController>();
        agent.speed = bs.movement;
        StartCoroutine(UpdateTheRotation());
    }

    //Boss is awake and starts to chase and attack someone
    public void WakeUp(Transform t) // -> it's called directly from the server
    {
        //Debug.Log("Wake up"+isSleeping+" and first "+firstAttack);
        if (isSleeping || firstAttack)
        {
            agent.speed = bs.movement;
            int index = IndexOfChampion(t);
            aggro[index] = 1;
            isSleeping= false;
            firstAttack = false;
            nooneInside = false;
            bAnimC.WakeUp();

            StartCoroutine(SelectTheTarget(players[index]));
            //Setup a FSM at initial state
            awakeFSM = new FSM(CreateAwakeFSM());
            StartCoroutine(AwakeTheBoss()); 
        }
        else
            MoreAggro(t);
    }

    IEnumerator UpdateTheRotation()
    {
        while (true){

            if (target && Vector3.Distance(transform.position, target.position) < 30)
                LookTheTarget();

            yield return null;
        }
    }

    // This function will do the update when the boss is awaken
    IEnumerator AwakeTheBoss()
    {
        while (!nooneInside && transform)
        {
            awakeFSM.Update();
            yield return new WaitForSeconds(reactionTime);
        }
        //If there is no one inside, the boss will go back to the base
        BackToBase(); 
    }

    // Attack the target if it is in fightRange
    public IEnumerator AttackTheTarget()
    {
        isAttacking = true;
        //Debug.Log("Attacking!");
        bAttC.Attack(bs.bosstype, target);
        yield return new WaitForSeconds(bs.attackRate);
        isAttacking = false;
    }

    //every 1 second will check the aggro. Called when the boss awakes. It ends when it sleeps
    public IEnumerator SelectTheTarget(Transform t)
    {
        if (!bAnimC.isAwake) // wait the wake up animation
            yield return new WaitForSeconds(2f);
        //First target is the one who woke it up
        target = t;
        yield return new WaitForSeconds(1f);
        while (!nooneInside)
        {
            target = players[IndexmaxAggro()];
            ChangeTheTarget();
            yield return new WaitForSeconds(1f);
        }
    }

    //Check if there is a target or if it can go to his base
    public void ChangeTheTarget()
    {
        if (target == null && !nooneInside)
            target = players[IndexmaxAggro()];
        if (nooneInside)
            BackToBase();
    }

    //It goes back to its base (if it's not already sleeping
    public void BackToBase()
    {
        if (!this.isSleeping)
        {
            agent.speed = 3;
            nooneInside = true;
            target = null;
            agent.SetDestination(bossBase.position);
            ChangeStoppingDistance(3);
            StartCoroutine(RegenerateHealth());
            StartCoroutine(LookToTheBase());
        } 
    }

    //function that makes it sleep
    public IEnumerator Sleep()
    {
        while (agent.remainingDistance>agent.stoppingDistance || bs.HP<bs.maxHealth)
        {
            if (agent.remainingDistance < agent.stoppingDistance)
                agent.velocity = new Vector3 (0,0,0) ;
            yield return new WaitForSeconds(1f);
        }
        bAnimC.Sleep();
        isSleeping = true;
    }

    //Adds the player at the same index of the boss field
    public void AddPlayer(Transform t, int index)
    {
        players[index] = t;
        aggro[index] = 0;
    }

    //Removes a player from the list, because it's outside its field
    public void RemovePlayer(int index)
    {
        Transform tmp = players[index];
        aggro[index] = -1;
        players[index] = null;
        if (target == tmp)
        {
            target = null;
            ChangeTheTarget();
        }
    }

    //Adds an amount of aggro base of how many attacks the player gave to the boss
    public void MoreAggro(Transform t)
    {
        int index = IndexOfChampion(t);
        aggro[index] += 0.2f;
    }

    //Returns the index of the player with the max aggro
    private int IndexmaxAggro()
    {
        float max = -1;
        int index = 0;
        for (int i = 0; i < aggro.Length; i++)
            if (aggro[i] > max)
            {
                max = aggro[i];
                index = i;
            }
        return index;
    }

    //Returns the index on the array of the champion with the transform requested
    public int IndexOfChampion(Transform t)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == t)
                return i;
        }
        Debug.Log("You shouldn't be here " + t);

        if (players[players.Length - 1] == null) // it's just to not return a -1
            AddPlayer(t, players.Length - 1);
           

        return players.Length-1;
    }

    //Start to regenerate when there is no one inside the field
    public IEnumerator RegenerateHealth()
    {
        firstAttack = true;
        while (bs.HP < bs.maxHealth)
        {
            yield return new WaitForSeconds(1f);
            bs.HP += bs.healthRegen;
            RpcHeal();
        }
        if (bs.HP > bs.maxHealth)
        {
            bs.HP = bs.maxHealth;
            RpcHeal();
        }
        if (nooneInside)
            StartCoroutine(Sleep());
    }

    //The boss will loot to the target,rotating to stay in front of the target
    private void LookTheTarget()
    {
            //Target Lock on
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * changeTargetVelocity).eulerAngles;
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    // The stopping distance it's necessary due to its size. for some reason, the navmesh won't do my subject as a agent prefab and i have to check myself
    private void ChangeStoppingDistance(float distance)
    {
        if (agent.stoppingDistance != distance)
                agent.stoppingDistance = distance;
    }

    //the boss will go to the base. to do it, it slows down for a time and then it will go again.
    private IEnumerator LookToTheBase()
    {
        agent.speed = 0.1f; ;
        yield return new WaitForSeconds(1);
        agent.speed = 3; // little slower than the chasing movement
    }

    [ClientRpc]
    private void RpcHeal()
    {
        if (bs.HP <= bs.maxHealth)
            hb.ChangeTheBar(-bs.healthRegen);
        else
            hb.BarFilled();
    }

    // this creates the states and gives the first state to the fsm , in theory
    FSMState CreateAwakeFSM()
    {
        //Creating the states
        FSMState chasing = new FSMState();
        chasing.enterActions = new FSMAction[] { Chase };
        chasing.stayActions = new FSMAction[] { Chase };
        FSMState attacking = new FSMState();
        attacking.enterActions = new FSMAction[] { Attack };
        attacking.stayActions = new FSMAction[] { Attack };

        //Defining transitions
        FSMTransition t1 = new FSMTransition(AttackCondition);
        FSMTransition t2 = new FSMTransition(ChaseCondition);

        //Link states with transitions
        chasing.AddTransition(t1, attacking);
        attacking.AddTransition(t2, chasing);
        return chasing;
    }

    // ---- FSM COnditions

    bool ChaseCondition()
    {
        return target && Vector3.Distance(target.transform.position, transform.position) > bs.fightRange;
    }

    bool AttackCondition()
    {
        return target && Vector3.Distance(target.transform.position, transform.position) <= bs.fightRange;
    }

    // ------ Actions

    void Chase() // method called when in chasing
    {
        agent.speed = bs.movement;
        if (target&& Vector3.Distance(transform.position, target.position) < 40)
        {
            if (Vector3.Dot(transform.forward, agent.velocity) < 0)
            {
                //Debug.Log("Ti stai muovendo al contrario");
                agent.velocity = transform.forward;
            }
            agent.SetDestination(target.position);
            ChangeStoppingDistance(6);
            bAnimC.Move();
        }
    }

    void Attack() // method called when in attacking
    {
        agent.speed = 0;
        agent.velocity = new Vector3(0, 0, 0);
        if (!isAttacking)
            StartCoroutine(AttackTheTarget());
    }
}

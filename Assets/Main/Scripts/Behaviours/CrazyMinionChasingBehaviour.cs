﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;

/// <summary>
/// This class will be responsable for the chasing and attack of the target for the crazy minion
/// </summary>
public class CrazyMinionChasingBehaviour : NetworkBehaviour {


    [HideInInspector]
    public bool alreadyMoving = true;
    public float changeTargetVelocity;

    private _AnimationController animCont;
    private CrazyMinionStats cms;
    private bool isAttacking = false;

    void Start()
    {
        cms = GetComponent<CrazyMinionStats>();
        animCont = GetComponentInChildren<MinionAnimationController>();
    }

    //Function called by the fsm to chase the target
    public void Chase(NavMeshAgent agent, Transform t)
    {
        if (agent != null && agent.isActiveAndEnabled)
            agent.SetDestination(t.position);
        if (animCont && !alreadyMoving&&!isAttacking)
        {
            animCont.Move();
            alreadyMoving = true;
        }
    }

    //Function called by the fsm to attack the target
    public void Fight(Transform t)
    {
        if (!isAttacking)
           StartCoroutine( Shoot(t));
    }

    //Attack the target after a cooldown
    private IEnumerator Shoot(Transform t)
    {
        alreadyMoving = false;
        isAttacking = true;
        GetComponent<MinionAttackController>().Attack(t);
        yield return new WaitForSeconds(cms.fireRate);
        isAttacking = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
/// <summary>
///     //Class that moves the crazy minions, used by the fsm.
/// </summary>
public class CrazyMinionMovementBehaviour : NetworkBehaviour {

    public GameObject[] pointstoreach;

    private Transform lastPoint;
    private CrazyMinionBehaviour cb;
    private CrazyMinionChasingBehaviour ccb;
    private _AnimationController animCont;
    private CrazyMinionStats cms;
    private int nextpoint;

    void Start()
    {
        nextpoint = (int)pointstoreach.Length/2;
        cb = GetComponent<CrazyMinionBehaviour>();
        cms = GetComponent<CrazyMinionStats>();
        ccb = GetComponent<CrazyMinionChasingBehaviour>();
        animCont = GetComponentInChildren<MinionAnimationController>();

        lastPoint = pointstoreach[nextpoint].transform;
        if(isServer)
            StartCoroutine(SetMyDestination());
    }

    //Function called by the fsm when there is no target and can follow the target
    public void Walk()
    {
        if (animCont && !ccb.alreadyMoving)
        {
            animCont.Move();
            ccb.alreadyMoving = true;
        }
        if (nextpoint < 0 || nextpoint >= pointstoreach.Length)
            return;
    }

    //The destination will be updated with the next destination on the path
    void GoToTheNextPoint()
    {
        if (nextpoint >= pointstoreach.Length || nextpoint < 0)
            return;
        lastPoint = pointstoreach[nextpoint].transform;
        cb.agent.SetDestination(lastPoint.position);
        if((cms.left&& cms.vsLeftRed)|| (!cms.left && !cms.vsLeftRed))
                nextpoint--;
        else
                nextpoint++;
    }

    //Check to do because after a enemy is killed, it doesn't know the next point to go.
    IEnumerator SetMyDestination()
    {
        while (transform)
        {
            if (cb.target == null)
            {
                if (Vector3.Distance(cb.agent.destination, transform.position) < 2)
                {
                    if (Vector3.Distance(lastPoint.position, cb.agent.destination) < 1) // Unity is an assohole. The points position in the prefab are changed for a +-0.1 in the positions, so it is not the same...
                        GoToTheNextPoint();
                    else
                        cb.agent.SetDestination(lastPoint.position);    
                    cb.ChangeStopDistance();
                }
                else
                {
                    if (Vector3.Distance(lastPoint.position, cb.agent.destination) > 1)
                        cb.agent.SetDestination(lastPoint.position);
                    cb.ChangeStopDistance();
                }
            }
            yield return new WaitForSeconds(0.3f);
        }
    }
}

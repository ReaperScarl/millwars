﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/// <summary>
/// Class that moves the minion. Used by the fsm in minion behaviour
/// </summary>
public class MinionMovementBehaviour : MonoBehaviour {

    public GameObject[] pointstoreach;
    public float reactionTimeForDestination = 0.3f;
    public float reactionTimeForAdjust = 0.6f;

    private Transform lastPoint;
    private bool isRed;
    private MinionBehaviour mb;
    private MinionChasingBehaviour cb;
    private _AnimationController animCont;
    private MinionStats ms;
    private int nextpoint;

    void Start()
    {
        mb = GetComponent<MinionBehaviour>();
        cb = GetComponent<MinionChasingBehaviour>();
        ms = this.GetComponent<MinionStats>();
        isRed= (ms.align == _Stats.Align.red) ? true : false;
        nextpoint = (isRed) ? 1 : pointstoreach.Length-2;
        cb = GetComponent<MinionChasingBehaviour>();

        if (this.GetComponentInChildren<_AnimationController>())
        {
            if (ms.isMelee)
                animCont = this.GetComponentInChildren<MinionAnimationController>();
            else
                animCont = this.GetComponentInChildren<MinionRangeAnimationController>();
        }

        lastPoint = (isRed) ? pointstoreach[0].transform : pointstoreach[pointstoreach.Length-1].transform;

        StartCoroutine(SetMyDestination());
        StartCoroutine(AdjustThePath());
    }

    public void Walk()
    {
        if (animCont && !cb.alreadyMoving)
        {
            animCont.Move();
            cb.alreadyMoving = true;
        }

        if (nextpoint < 0 || nextpoint >= pointstoreach.Length)
            return;   
    }

    IEnumerator SetMyDestination()
    {
        while(transform)
        {
            if (mb.target == null)
            {
                //Debug.Log("minion: " + transform + "agent.stop distance: " + agent.stoppingDistance);
                if (Vector3.Distance(mb.agent.destination, transform.position) < 2)
                {
                    // Debug.Log("minion: "+transform+"agent.stop distance: " + agent.stoppingDistance);
                    if (Vector3.Distance(lastPoint.position, mb.agent.destination) < 1)
                    {
                        GoToTheNextPoint();
                    }
                    else
                    {
                        mb.agent.SetDestination(lastPoint.position);
                        // Debug.Log("last point: " + lastPoint);    
                    }
                    mb.ChangeStopDistance();
                }
                else
                {
                    if (Vector3.Distance(lastPoint.position, mb.agent.destination) > 1)
                    {
                        mb.agent.SetDestination(lastPoint.position);

                        // Debug.Log("minion: " + transform + "agent.stop distance: " + agent.stoppingDistance);
                    }
                    mb.ChangeStopDistance();
                }
            }
            yield return new WaitForSeconds(reactionTimeForDestination);
        }
    }

    //Will put the minion in the direction for the next point in the path
    void GoToTheNextPoint()
    {
        if (nextpoint >= pointstoreach.Length || nextpoint < 0)
            return;
        //Debug.Log("before "+nextpoint);
        lastPoint = pointstoreach[nextpoint].transform;
        mb.agent.SetDestination(lastPoint.position);
        nextpoint = (isRed) ? nextpoint+1 : nextpoint - 1;
       // Debug.Log("after "+nextpoint);
    }

    IEnumerator AdjustThePath() // In theory, it should not let the minion go back in the path before to go forward
    {
        while (this.gameObject)
        {
            if (CheckMyDestination())
            {
                if (isRed) // red ->
                {
                    // If i'm nearer the nexus than my old destination point
                    if (Vector3.Distance(transform.position, pointstoreach[pointstoreach.Length - 1].transform.position) < Vector3.Distance(lastPoint.position, pointstoreach[pointstoreach.Length - 1].transform.position))
                        GoToTheNextPoint();
                }
                else //Blue <-
                {
                    if (Vector3.Distance(transform.position, pointstoreach[0].transform.position) < Vector3.Distance(lastPoint.position, pointstoreach[0].transform.position))
                        GoToTheNextPoint();
                }
                if(mb.target==null)
                    mb.ChangeStopDistance();
            }
            yield return new WaitForSeconds(reactionTimeForAdjust);
        }
    }

    //Condition that checks if the destination and the last point position are the same. -> See the comment about Unity in CrazyMinionMovementBehaviour
    bool CheckMyDestination()
    {
        if(mb.target != null)
            return false;
        if(mb.agent)
            return Vector3.Distance(mb.agent.destination,lastPoint.position)>1;
        return false;
    }
}

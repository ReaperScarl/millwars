﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

/// <summary>
/// Class responsible for the Behaviour of the crazy minions. It uses a FSM.
/// </summary>
public class CrazyMinionBehaviour : NetworkBehaviour {

    public float reactionTime = 0.3f;
    public float reactionTimeForTarget = 0.7f;
    public float changeTargetVelocity=5;

    [SyncVar]
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public NavMeshAgent agent;

    protected bool isCrazy;
    protected CrazyMinionStats cms;
    protected float totFightRange;

    private CrazyMinionChasingBehaviour mcb;
    private CrazyMinionMovementBehaviour mmb;
    private string[] targetTag = new string[2];
    string[] factions = { "red", "blue" };

    private FSM fsm;

    void Start () {
        //inizialization of the elements
        agent = GetComponent<NavMeshAgent>();
        cms = this.GetComponent<CrazyMinionStats>();
        isCrazy = cms.isReallyCrazy;
        totFightRange = cms.fightRange;
       
        mcb = GetComponent<CrazyMinionChasingBehaviour>();
        mmb = GetComponent<CrazyMinionMovementBehaviour>();

        if (isCrazy)
            targetTag = factions;
        else
        {
            if (cms.vsLeftRed)
            {
                targetTag[0] = (cms.left) ? factions[0] : factions[1];
                this.tag = (cms.left) ? factions[1] : factions[0];
            }
            else
            {
                targetTag[0] = (cms.left) ? factions[1] : factions[0];
                this.tag = (cms.left) ? factions[0] : factions[1];
            }
        }
        ChangeStopDistance();

        //Defining the states of the fsm for the crazy minions
        FSMState walking = new FSMState();
        walking.enterActions = new FSMAction[] { FollowThePath };
        walking.stayActions = new FSMAction[] { FollowThePath };
        FSMState chasing = new FSMState();
        chasing.enterActions = new FSMAction[] { Chase };
        chasing.stayActions = new FSMAction[] { Chase };
        FSMState attacking = new FSMState();
        attacking.enterActions = new FSMAction[] { Attack };
        attacking.stayActions = new FSMAction[] { Attack };

        //Defining transitions
        FSMTransition t1 = new FSMTransition(AttackCondition);
        FSMTransition t2 = new FSMTransition(SomeoneInsideCondition);
        FSMTransition t3 = new FSMTransition(NoneInsideCondition);

        //Link states with transitions
        walking.AddTransition(t1, attacking);
        walking.AddTransition(t2, chasing);
        chasing.AddTransition(t1, attacking);
        chasing.AddTransition(t3, walking);
        attacking.AddTransition(t2, chasing);
        attacking.AddTransition(t3, walking);

        //Setup a FSM at initial state
        fsm = new FSM(walking);

        if (isServer)
        {
            StartCoroutine(CrazyChickenIsUp());
            StartCoroutine(UpdateTarget());
        }            
    }

    private void LateUpdate() // the rotation if not put in Update is ugly to see
    {
        if (target != null)
        {
            //Target Lock on
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * changeTargetVelocity).eulerAngles;
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }
    }

    //Updates the fsm after the reaction time
    IEnumerator CrazyChickenIsUp()
    {
        while (transform)
        {
            fsm.Update();
            yield return new WaitForSeconds(reactionTime);
        }
    }

    // Updates the target every *reaction time for target* seconds. It could change its mind after that time
    IEnumerator UpdateTarget() 
    {
        while (transform)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag(targetTag[0]);
            GameObject[] enemies2 = null;
            GameObject nearestEnemy = null;

            if (isCrazy)
                enemies2 = GameObject.FindGameObjectsWithTag(targetTag[1]);
            float shortestDistance = cms.visionRange + 1;
            //part 1
            foreach (GameObject enemy in enemies)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }

            if (isCrazy)
            {
                //part 2
                foreach (GameObject enemy in enemies2)
                {
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                    if (distanceToEnemy < shortestDistance)
                    {
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = enemy;
                    }
                }
            }
            if (nearestEnemy != null && shortestDistance <= cms.visionRange)
                target = nearestEnemy.transform;
            if (nearestEnemy != null && Vector3.Distance(nearestEnemy.transform.position, transform.position) > cms.visionRange)
                target = null;
            if (nearestEnemy == null && target != null)
                target = null;

            yield return new WaitForSeconds(reactionTimeForTarget);
        }
    }

    //Change the Stopping distance of the agent. For point of the map and target the stopping distance has to be different.
    public void ChangeStopDistance()
    {
        if (target == null)
            agent.stoppingDistance = 0.2f;
        else
        {
            if (cms.fightRange == totFightRange)
                totFightRange += target.GetComponent<_Stats>().offSet;

            if (cms.isMelee)
            {
                agent.stoppingDistance = cms.fightRange - 0.2f;
            }
        }
    }

    //When the minion is in range, it has to stop moving and start attacking
    void ChangeSpeed(bool inRange)
    {
        if (inRange)
        {
            if (agent.speed == 0)
                return;
            else
            {
                agent.speed = 0;
                agent.velocity = Vector3.zero;
            }
        }
        else
        {
            if (agent.speed == cms.movement)
                return;
            else
                agent.speed = cms.movement;
        }
    }

    //Method called by the champion when attacked by another champion
    public void ChangeTarget(Transform t)
    {
        if (target.GetComponent<_Stats>().objectType == _Stats.ObjectTypes.champion)
            if ((transform.position - t.position).magnitude < cms.visionRange)
                target = t;
    }

    //-----Conditions
    bool AttackCondition()
    {
        return (target != null && Vector3.Distance(transform.position, target.transform.position) < totFightRange);
    }

    bool SomeoneInsideCondition()
    {
        return target != null;
    }

    bool NoneInsideCondition()
    {
        return !SomeoneInsideCondition();
    }

    //-----Actions
    void FollowThePath()
    {
        if (isServer)
        {
            ChangeSpeed(false);
            mmb.Walk();
        }
        
    }

    void Chase()
    {
        if (isServer)
        {
            ChangeSpeed(false);
            mcb.Chase(agent, target);
            ChangeStopDistance();
          }
    }

    void Attack()
    {
        ChangeSpeed(true);
        mcb.Fight(target);
    }
}
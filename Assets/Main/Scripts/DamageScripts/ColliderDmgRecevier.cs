﻿using UnityEngine;

/// <summary>
/// This class has to be given to all the object with colliders that have the receiver component on their object.
/// Ex. Tower has the recevier only in the base, but can be hit in all its mesh.
/// </summary>
public class ColliderDmgRecevier : MonoBehaviour {

    private DamageReceiver recevier;
    private DamageInfo [] meleehitters= new DamageInfo[4]; // In case of 4 melee champ

    void Start () {
        //The real receiver of the collider
        if(transform.parent)
            recevier = this.transform.parent.GetComponentInChildren<DamageReceiver>();
        //the receiver is in the object
        if (recevier == null)
            recevier = this.transform.GetComponent<DamageReceiver>();      
    }
    
    private void OnTriggerEnter(Collider other)
    {
		PlayerBullet pb = null;
		Weapon w = null;
		if(other.transform.parent == null)
        	pb= other.transform.GetComponent<PlayerBullet>();
        else
			w= other.transform.parent.GetComponentInParent<Weapon>();

        //The collider has not recevier, so should be ground or environment
        if (!recevier)
        { 
            if (pb) //if it's a bullet
                Destroy(other.gameObject);
            return;
        }
        if (recevier.transform.tag == "blue" || recevier.transform.tag == "red" || recevier.transform.tag == "crazyminion"|| recevier.transform.tag=="boss")
        {
            if (pb) //range
            {
                if (pb.GetTag() == recevier.transform.tag) // ally: goes on
                    return;
                else
                {
					DamageProvider dmgP = new DamageProvider();
                    if (dmgP != null) // should be always, but whatever
                    {
                        // We want to give the dmg to the receiver.
                        dmgP.ProvideDamage(recevier.transform, pb.Damage(), pb.Hitter());
                        Destroy(other.gameObject);
                    }
                }
            }
            if(w) // Melee
            {
                Transform hitter = other.transform.root;
				if (hitter.tag == recevier.transform.tag)
                    return;
                else 
                {
					DamageProvider dmgP = new DamageProvider();
                    DamageInfo hitinfo = w.GetDamageInfo();

                    int playerID = IndexOfChampion(hitinfo.playerName);
                    if (playerID==-1) //champ not in the array
                    {
                        AddMeleeHitter(hitinfo);
                        if (dmgP != null)
                        {
                            // We want to give the dmg to the receiver.
                            dmgP.ProvideDamage(recevier.transform, hitinfo.damage, hitter);
                            if (hitinfo.isStunning)
                            {
                                switch (recevier.transform.GetComponent<_Stats>().objectType)
                                {
                                    case _Stats.ObjectTypes.minion:
                                        if (recevier.transform.GetComponent<MinionStats>())
                                            recevier.transform.GetComponent<MinionStats>().Stunned(hitinfo.stunTime);
                                        else
                                            recevier.transform.GetComponent<CrazyMinionStats>().Stunned(hitinfo.stunTime);
                                        break;

                                    case _Stats.ObjectTypes.champion:
                                        recevier.transform.GetComponent<PlayerControllerWithNet>().Stun(hitinfo.stunTime);
                                        break;
                                }
                            }
                        }
                    }
                    else // in the array. check the counter
                    {
                        if(hitinfo.counter>meleehitters[playerID].counter)
                        {
                            if (dmgP != null)
                            {
                                // We want to give the dmg to the receiver.
                                dmgP.ProvideDamage(recevier.transform, hitinfo.damage, hitter);
                                if (hitinfo.isStunning)
                                {
                                    switch (recevier.transform.GetComponent<_Stats>().objectType)
                                    {
                                        case _Stats.ObjectTypes.minion:
                                            if (recevier.transform.GetComponent<MinionStats>())
                                                recevier.transform.GetComponent<MinionStats>().Stunned(hitinfo.stunTime);
                                            else
                                                recevier.transform.GetComponent<CrazyMinionStats>().Stunned(hitinfo.stunTime);
                                            break;

                                        case _Stats.ObjectTypes.champion:
                                            recevier.transform.GetComponent<PlayerControllerWithNet>().Stun(hitinfo.stunTime);
                                            break;
                                    }
                                }
                                meleehitters[playerID] = hitinfo;
                            }
                        }
                    }    
                }
            }    
        }
    }

    //Add the hit info in the array
    private void AddMeleeHitter(DamageInfo hit)
    {
        for (int i = 0; i < meleehitters.Length; i++)
        {
            if (meleehitters[i]== null)
                meleehitters[i]=hit;
        }
    }

    //returns the index in the array given the name
    private int IndexOfChampion(string player)
    {
        for (int i = 0; i < meleehitters.Length; i++)
        {
            if (meleehitters[i]!=null && meleehitters[i].playerName == player)
                return i;
        }
        return -1; // this time, -1 is a normal value when the player is still not in there
    }
}

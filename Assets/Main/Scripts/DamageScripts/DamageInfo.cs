﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Object where the information about a hit will be inserted
/// </summary>
public class DamageInfo  {

    public string playerName;
    public int counter;
    public int damage;
    public bool isStunning;
    public float stunTime;


    public DamageInfo(string player, int count, int dmg, bool stun = false, float stunTime = 0)
    {
        playerName = player;
        counter = count;
        damage = dmg;
        isStunning = stun;
        this.stunTime = stunTime;
    }

	public int GetCounter(){
		return counter;
	}
}

﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Class responsible to provide the damage to the receiver
/// </summary>
public class DamageProvider {

	public void ProvideDamage(Transform target, int dmg,Transform hitter= null) {

        if (!target)
            return;

		DamageReceiver dr = target.GetComponent<DamageReceiver> ();
		PlayerColliderDmgRecevier collider = dr.GetComponent<PlayerColliderDmgRecevier> ();
		if (dr != null) {
			if (!hitter) {
				if (collider != null && collider.invincible)
					return;
				dr.ReceiveDamage (dmg);
			}
			else{
				if (collider != null && collider.invincible)
					return;
                dr.ReceiveDamage(dmg, hitter); // if there is a hitter, then it's a champion
			}
        }
	}
}

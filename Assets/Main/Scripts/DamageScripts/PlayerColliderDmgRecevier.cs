﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// This class has to be given to all the object with colliders in that have the recevier non in their object
/// Ex. Tower has the recevier only in the base, but can be hit in all his mesh.
/// </summary>
public class PlayerColliderDmgRecevier : NetworkBehaviour {

    private DamageReceiver recevier;
    private DamageInfo [] meleehitters= new DamageInfo[4];
	private Collider other;
	[SyncVar(hook ="OnChange")]
    [HideInInspector]
	public bool invincible = false;

    // Use this for initialization
    void Start () {
        //The real recevier of the collider
        if(transform.parent)
            recevier = this.transform.parent.GetComponentInChildren<DamageReceiver>();
        //the dmgrecevier is in the father object
        if (recevier == null)
            recevier = this.transform.GetComponent<DamageReceiver>();      
    }

	private void OnChange(bool newVal){
		if (isServer && invincible != newVal)
			RpcChange(newVal);
	}

	[ClientRpc]
	void RpcChange(bool newVal){
		invincible = newVal;
	}
    
    private void OnTriggerEnter(Collider other)
    {
		if (!isServer || invincible)
			return;
		
		PlayerBullet pb = null;
		Weapon w = null;
		if(other.transform.parent == null)
        	pb= other.transform.GetComponent<PlayerBullet>();
        else
			w= other.transform.parent.GetComponentInParent<Weapon>();

        //The collider has not recevier, so should be ground or environment
        if (!recevier)
        { 
            if (pb) //if it's a bullet
                Destroy(other.gameObject);
            return;
        }
       
        if (recevier.transform.tag == "blue" || recevier.transform.tag == "red" || recevier.transform.tag == "crazyminion"|| recevier.transform.tag=="boss")
        {
            if (pb) //range
            {
                //Debug.Log("There is recevier");
                if (pb.GetTag() == recevier.transform.tag)
                { // ally: goes on
                   // Debug.Log("There is ally");
                    return;
                }
                else
                {
                 //   Debug.Log("There is foe");
                    // the bullet has a damage provider 
                    //DamageProvider dmgP = other.transform.GetComponent<DamageProvider>();
					DamageProvider dmgP = new DamageProvider();
                   // Debug.Log("There is dmg: "+dmgP);
                    if (dmgP != null)
                    {
                        // We want to give the dmg to the recevier.
                        dmgP.ProvideDamage(recevier.transform, pb.Damage(), pb.Hitter());
                        Destroy(other.gameObject);
                    }
                }
            }
            if(w) // Melee
            {
                //Has hit an ally
                Transform hitter = other.transform.root;
                //Debug.Log("hitter: " + hitter);
				if (hitter.tag == recevier.transform.tag)
                    return;
                else 
                {
                    //DamageProvider dmgP = hitter.parent.GetComponent<DamageProvider>();
					DamageProvider dmgP = new DamageProvider();
                    DamageInfo hitinfo = w.GetDamageInfo();

                    int playerID = IndexOfChampion(hitinfo.playerName);
                    if (playerID==-1) //non è ancora nell'array
                    {
                        AddMeleeHitter(hitinfo);

                        if (dmgP != null)
                        {
                            // We want to give the dmg to the recevier.
                            dmgP.ProvideDamage(recevier.transform, hitinfo.damage, hitter);
                            if (hitinfo.isStunning)
                            {
                                switch (recevier.transform.GetComponent<_Stats>().objectType)
                                {
                                    case _Stats.ObjectTypes.minion:
                                        if (recevier.transform.GetComponent<MinionStats>())
                                            recevier.transform.GetComponent<MinionStats>().Stunned(hitinfo.stunTime);
                                        else
                                            recevier.transform.GetComponent<CrazyMinionStats>().Stunned(hitinfo.stunTime);
                                        break;

                                    case _Stats.ObjectTypes.champion:
                                        recevier.transform.GetComponent<PlayerControllerWithNet>().Stun(hitinfo.stunTime);
                                        break;
                                }
                            }
                        }
                    }
                    else // già nell'array. controlla il counter
                    {
                        if(hitinfo.counter>meleehitters[playerID].counter)
                        {
                            //Debug.Log("hit info counter : "+ hitinfo.counter + ", melee counter: "+ meleehitters[playerID].counter+" and dmg " +hitinfo.damage);
                            if (dmgP != null)
                            {
                                // We want to give the dmg to the recevier.
                                dmgP.ProvideDamage(recevier.transform, hitinfo.damage, hitter);
                                if (hitinfo.isStunning)
                                {
                                    //Debug.Log("To be stunned");
                                    switch (recevier.transform.GetComponent<_Stats>().objectType)
                                    {
                                        case _Stats.ObjectTypes.minion:
                                            if (recevier.transform.GetComponent<MinionStats>())
                                                recevier.transform.GetComponent<MinionStats>().Stunned(hitinfo.stunTime);
                                            else
                                                recevier.transform.GetComponent<CrazyMinionStats>().Stunned(hitinfo.stunTime);
                                            break;

                                        case _Stats.ObjectTypes.champion:
                                            recevier.transform.GetComponent<PlayerControllerWithNet>().Stun(hitinfo.stunTime);
                                            break;
                                    }
                                }
                                meleehitters[playerID] = hitinfo;
                            }
                            // prendi i danni
                        }
                    }    
                }
            }    
        }
    }

    private void AddMeleeHitter(DamageInfo hit)
    {
        for (int i = 0; i < meleehitters.Length; i++)
        {
            if (meleehitters[i]== null)
                meleehitters[i]=hit;
        }
        //Debug.Log("Added");
    }

    private int IndexOfChampion(string player)
    {
        for (int i = 0; i < meleehitters.Length; i++)
        {
            if (meleehitters[i]!=null && meleehitters[i].playerName == player)
                return i;
        }
        return -1;
    }
}

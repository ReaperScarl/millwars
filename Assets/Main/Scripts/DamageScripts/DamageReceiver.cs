﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
/// <summary>
/// Class responsible to take the damage
/// </summary>
public class DamageReceiver : NetworkBehaviour {

    public bool active = true;
    private _Stats ms;
    public Image healthbar;
    public BarModifierManager hmod;

    void Start()
    {
        ms = GetComponent<_Stats>();
        if (transform.name!="Player") // it's still working?
            SetBar();

    }

    //Says to the ones using the receiver to inizialize their bars.
    public void SetBar(){
        if (healthbar)
        {
            hmod = healthbar.GetComponent<BarModifierManager>();
            if (ms == null)
                return;
            if (hmod)
                hmod.SetConversionPToB(ms.maxHealth);
            else
            {
                hmod = healthbar.GetComponentInChildren<BarModifierManager>();
                hmod.SetConversionPToB(ms.maxHealth);
            }
        }
    }


   /// <summary>
   /// It's the Method that deals with the damages. Check that if the is the player using it, it has to have the hitter
   /// </summary>
   /// <param name="i"> it's the dmg</param>
   /// <param name="hitter">if it is not null, then the hitter is a champion</param>
    public void ReceiveDamage(int i, Transform hitter = null)
    {
        if (!isServer)
            return;

        switch (ms.objectType)
        {
            case _Stats.ObjectTypes.champion :
                if (transform.gameObject == null)
                    return;
                PlayerStats ps = GetComponent<PlayerStats>();
                if (active)
                {
                    if (hitter) //the player is attacking another player
                    {
                        //Calls help if it is attacked by an enemy champ
                        GetComponent<PlayerControllerWithNet>().CryForHelp(hitter);
                        //champ are the only ones with the following pattern: dmg = Att -(dif - perf)
                        float def_perf = (ps.defense - hitter.GetComponent<PlayerStats>().perforation);
                        if (def_perf > 0)
                        {
                            if( i - def_perf > 0) // the dmg is more than the defense remaining
                                ps.HP -= i - def_perf;
                        }  
                        else
                            ps.HP -= i;
                    }
                    else
                       // if just ignore the defense
                            ps.HP -= i;
                }   
                break;

            case _Stats.ObjectTypes.boss:
                ms.HP -= i;
                this.GetComponentInParent<BossBehaviour>().WakeUp(hitter);
                break;

            default:
                if (this.enabled)
                    ms.HP -= i;
                else
                    return;
                break;
        }

        if (ms.HP <= 0) //Give experience too
        {
            ScoreManager sm = GameObject.Find("ScoreManager").transform.GetComponent<ScoreManager>();
            switch (ms.objectType)
            {
                case _Stats.ObjectTypes.turret:
                    //Called the destroy inside
                    this.GetComponent<TurretController>().TowerDestroyed();
                    break;

                case _Stats.ObjectTypes.nexus:
                    transform.parent.GetComponentInChildren<GameManager>().NexusDestroyed(this.tag);
                    Destroy(transform.gameObject);
                    break;

				case _Stats.ObjectTypes.champion:
					PlayerControllerWithNet player = GetComponent<PlayerControllerWithNet> ();
					//kill the player only one time
					if (!player.alreadyDead) {
						player.alreadyDead = true;
                        if(hitter) // Team loses points only if the player has been killed by another player
						    sm.ChampionDead (this.tag, this.GetComponent<PlayerStats> ().playerName);
						player.Die ();
					}
					if (hitter)
						sm.ChampionKilled (hitter.tag, hitter.GetComponent<PlayerStats> ().playerName, GetComponent<PlayerStats>().playerName);
					break;

                case _Stats.ObjectTypes.boss:  
                    sm.BossKilled(hitter.tag);
                    Destroy(transform.gameObject);
					break;

                case _Stats.ObjectTypes.minion:
                    if(hitter)
                        hitter.GetComponent<PlayerControllerWithNet>().MinionKilled();
                    GiveExperience();
                    Destroy(transform.gameObject);
					break;
            }
			//add experience
            if(hitter)
			    hitter.GetComponent<ExperienceManager> ().RpcAddExperience(ms.objectType, false);
        }
    }

    public BarModifierManager GetBar()
    {
        return hmod;
    }

    public void GiveExperience() // gives the experience to the players even if they didn't attack
    {
       Collider[] colliders= Physics.OverlapSphere(transform.position, 8);
        foreach(Collider person in colliders)
        {
            if (person!= null&&person.transform.GetComponent<_Stats>()!=null && person.transform.GetComponent<_Stats>().objectType==_Stats.ObjectTypes.champion)
                person.transform.GetComponent<ExperienceManager>().RpcAddExperience(_Stats.ObjectTypes.minion, true);
        }
    }
}
